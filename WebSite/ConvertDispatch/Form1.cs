﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using JamZoo.Project.WebSite.Models;
using JamZoo.Project.WebSite.Service;

namespace ConvertDispatch
{    
    public partial class Form1 : Form
    {
        private bool isDo = false;
        public Form1()
        {
            InitializeComponent();
        }

        private void BtnStart_Click(object sender, EventArgs e)
        {
            listBox1.Items.Insert(0, string.Format("[{0}開始檢查", DateTime.Now.ToString("yyMMdd HH:mm:ss")));
            isDo = true;
            tmr.Start();
            tmr.Interval = 100;
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            listBox1.Items.Insert(0, string.Format("[{0}停止檢查", DateTime.Now.ToString("yyMMdd HH:mm:ss")));
            isDo = false;
            tmr.Stop();
            tmr.Interval = 100;
        }

        private void DoDispatch()
        {
            string errStr = string.Empty;
            
            
            DocumentService docSer = new DocumentService();
            DocumentConvertService docConvSer = new DocumentConvertService();
            

            DocumentConvertListModel docConvLtMod = new DocumentConvertListModel();


            docConvLtMod = docConvSer.GetList(docConvLtMod);

            if(docConvLtMod.Data.Count == 0)
            {
                tmr.Interval = 60000;
                listBox1.Items.Insert(0, string.Format("[{0}無資料", DateTime.Now.ToString("yyMMdd HH:mm:ss")));
                return;
            }
            else
            {
                if(docConvLtMod.Data.Count > 2)
                    tmr.Interval = 30000;
            }

            int Cnt = 1;
            foreach(var conJob in docConvLtMod.Data)
            {                
                if (Cnt == 3)
                    break;
                
                if (conJob.ConvertStatus > 1)
                    continue;

                if(conJob.ConvertStatus == 1)
                {
                    Cnt++;
                    continue;
                }
                
                DocumentVersionModel DocVerObj = docSer.GetDocVer(conJob.VersionId);

                string strDomain = DocVerObj.FileUrl.Replace("https://","").Split('/')[1];

                string exePath = string.Format("{0}DoJob\\DocCnverterApplication.exe", AppDomain.CurrentDomain.BaseDirectory); 
                string fijiCmdText = string.Format("{0} {1}", strDomain, conJob.Id);
                JobExe bgw = null;

                DocumentConvertModel docConvObj = docConvSer.Get(conJob.Id);

                docConvObj.ConvertStartTime = DateTime.Now;
                docConvObj.ConvertStatus = 1;
                docConvObj.ModifyTime = DateTime.Now; ;
                docConvSer.Update(docConvObj, out errStr);              

                if(Cnt== 1)
                {
                    if(string.IsNullOrEmpty(textBox1.Text))
                        bgw = new JobExe(exePath, fijiCmdText, conJob.Id.ToString(), textBox1, listBox1);
                    else
                        bgw = new JobExe(exePath, fijiCmdText, conJob.Id.ToString(), textBox2, listBox1);
                }
                else
                {
                    if (string.IsNullOrEmpty(textBox2.Text))
                        bgw = new JobExe(exePath, fijiCmdText, conJob.Id.ToString(), textBox2, listBox1);
                    else
                        bgw = new JobExe(exePath, fijiCmdText, conJob.Id.ToString(), textBox1, listBox1);
                }                
                bgw.WorkerReportsProgress = true;                
                bgw.RunWorkerAsync();

                Cnt++;
            }
        }

        private void tmr_Tick(object sender, EventArgs e)
        {
            if (isDo)
            {
                if (listBox1.Items.Count > 1000)
                    listBox1.Items.RemoveAt(999);

                listBox1.Items.Insert(0, string.Format("[{0}執行檢查", DateTime.Now.ToString("yyMMdd HH:mm:ss")));
                DoDispatch();
            }                
        }                        
    }

    public class JobExe : BackgroundWorker
    {
        public JobExe(string FileName, string Arg, string ConverId, TextBox ControltxtBox, ListBox LtBox)
        {
            fileName = FileName;
            arg = Arg;
            converId = ConverId;
            controltxtBox = ControltxtBox;
            ltBox = LtBox;

            this.DoWork += new DoWorkEventHandler(backgroundWorker_DoWork);
            this.ProgressChanged += new ProgressChangedEventHandler(backgroundWorker_ProgressChanged);
        }

        private TextBox controltxtBox { get; set; }
        private ListBox ltBox { get; set; }
        private string converId { get; set; }
        private string fileName { get; set; }
        private string arg { get; set; }

        private DateTime StartDt { get; set; }
        private DateTime EndDt { get; set; }

        void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            ExeCmd(fileName, arg);
        }

        void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            // The progress percentage is a property of e
            controltxtBox.Text = e.UserState.ToString();      
            if(e.ProgressPercentage == 100)
            {
                ltBox.Items.Insert(0, string.Format("[{0} -> {1}] 轉換完成[{2}]", StartDt.ToString("yyMMdd HH:mm:ss"), EndDt.ToString("yyMMdd HH:mm:ss"), converId));
            }
        }

        private void ExeCmd(string fileName, string Arg)
        {
            StartDt = DateTime.Now;
            this.ReportProgress(0, converId);
            System.Diagnostics.ProcessStartInfo info = new System.Diagnostics.ProcessStartInfo(fileName, Arg);
            System.Diagnostics.Process p = new System.Diagnostics.Process();
            p.StartInfo = info;
            p.Start();
            p.WaitForExit();
            this.ReportProgress(100, "");
            EndDt = DateTime.Now;
        }
    }
}
