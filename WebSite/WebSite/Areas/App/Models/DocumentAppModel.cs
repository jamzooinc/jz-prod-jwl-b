﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace JamZoo.Project.WebSite.Areas.App.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class DocumentAppModel
    {
        [Required(ErrorMessage = "必填")]
        [Display(Name = "ID")]
        public Guid Id { get; set; }

        [Display(Name = "文件類型Id")]
        public Guid DocumentTypeId { get; set; }

        [Display(Name = "文件類型")]
        public string DocumentTypeName { get; set; }

        [Display(Name = "自訂分類文字1")]
        public string CategoryText1 { get; set; }

        [Display(Name = "自訂分類文字2")]
        public string CategoryText2 { get; set; } 
        
        [Display(Name = "自訂分類文字3")]
        public string CategoryText3 { get; set; }

        /// <summary>
        /// 文件狀態(CheckIn= 1, Release= 2)，具審核權限者會同時得到CheckIn&Release兩份Document
        /// </summary>
        public int State { get; set; }
        /// <summary>
        /// 版號
        /// </summary>
        public string Version { get; set; }
        /// <summary>
        /// 訂閱狀態(未訂閱= 0, 已訂閱= 1, 已訂閱且自動下載= 2)
        /// </summary>
        public int Subscribe { get; set; }
        /// <summary>
        /// 顯示名稱
        /// </summary>
        public string DisplayName { get; set; }
        /// <summary>
        /// 顯示版號
        /// </summary>
        public string DisplayVersion { get; set; }
        /// <summary>
        /// 修改時間
        /// </summary>
        public DateTime ModifyTimestamp { get; set; }
        /// <summary>
        /// 修改帳號
        /// </summary>
        public string ModifyAccountName { get; set; }
        /// <summary>
        /// 到期日
        /// </summary>
        public DateTime ExpireTimestamp { get; set; }
        /// <summary>
        /// 封面縮圖
        /// </summary>
        public string CoverThumbnailUrl { get; set; }
        /// <summary>
        /// 檔案名稱
        /// </summary>
        public string FileName { get; set; }
        /// <summary>
        /// 檔案大小
        /// </summary>
        public long FileSize { get; set; }
        /// <summary>
        /// 檔案URL
        /// </summary>
        public string FileUrl { get; set; }
        /// <summary>
        /// 文件是否已轉換為頁面圖(否=0,是=1)
        /// </summary>
        public bool IsPageImage { get; set; }
        /// <summary>
        /// 文件權限(ReadOnly=0,Edit=1,Approve=2)
        /// </summary>
        public int Permission { get; set; }
        /// <summary>
        /// 是否為公開文件(否=0,是=1)
        /// </summary>
        public bool Public { get; set; }
    }
}