﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace JamZoo.Project.WebSite.Areas.App.Models
{
    /// <summary>
    /// 帳號
    /// </summary>
    public class FolderAppModel
    {
        [Required(ErrorMessage = "必填")]
        [Display(Name = "ID")]
        public Guid Id { get; set; }

        [StringLength(50)]
        [Required(ErrorMessage = "必填")]
        [Display(Name = "顯示名稱")]
        public string Name { get; set; }

        [StringLength(50)]
        [Required(ErrorMessage = "必填")]
        [Display(Name = "檔案類型")]
        public string TypeName { get; set; }   

        [StringLength(200)]        
        [Display(Name = "縮圖URL")]
        public string ThumbnailUrl { get; set; }

        [Display(Name = "文件List")]
        public IEnumerable<DocumentAppModel> Documents { get; set; }

    }
    
}