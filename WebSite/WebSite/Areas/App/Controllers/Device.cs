﻿using JamZoo.Project.WebSite.Filter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

using JamOA.sdk;
using JamZoo.Project.WebSite.Areas.App.Models;
using JamZoo.Project.WebSite.Areas.Service;
using JamZoo.Project.WebSite.Models;
using JamZoo.Project.WebSite.Service;

namespace JamZoo.Project.WebSite.Areas.App.Controllers
{
    public class DeviceController : BaseAppController
    {
       
        #region =================== 驗證 ===================

        #region ========= [001]帳號驗證 =========        
        [HttpPost]
        public ActionResult Auth(string User, string Password)
        {
            string strDomain = string.Empty;
            string strErrMsg = string.Empty;

            try
            {
                #region User
                if (string.IsNullOrEmpty(User))
                {
                    strErrMsg += "參數未輸入[User]";
                }

                if (string.IsNullOrEmpty(Password))
                {
                    strErrMsg += "參數未輸入[Password]";
                }

                if (!string.IsNullOrEmpty(strErrMsg))
                {                    
                    throw new Exception(strErrMsg);
                }
                #endregion

                #region 處理空字串
                User = User.Trim();
                Password = Password.Trim();
                #endregion

                #region Get Data
                if (!JamOAClient.UserAuth("jamzoo.service", User, Password))
                    throw new Exception(string.Format("{0}[{1}", "驗證失敗", JamOAClient.Message));

                var GroupObj = JamOAClient.MeOrgans(User);
                GroupObj = GroupObj.Where(p => p.ParentName.ToUpper() == "JAMZOO.SERVICE");

                if (GroupObj.Count() == 0)
                    throw new Exception("找不到Domain");

                strDomain = GroupObj.FirstOrDefault().Id;
                #endregion
            }
            catch (Exception ex)
            {
                Code = Code != "200" ? Code : "408";
                Msg = ex.Message;                
            }

            return Json(new
            {
                Code = Code != null ? Code : "500",
                Msg = Msg != null ? Msg : "",
                Domain = strDomain,
                AuthKey = "12345"
            }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region ========= [002]變更帳號設定 =========
        [HttpPost]
        public ActionResult ChangeAccountConfig(string User, string NewPassword, int EnableNotify = 1)
        {
            string strDomain = string.Empty;
            string strErrMsg = string.Empty;

            try
            {
                #region User
                if (string.IsNullOrEmpty(User))
                {
                    strErrMsg += "參數未輸入[User]";
                }

                if (string.IsNullOrEmpty(NewPassword))
                {
                    strErrMsg += "參數未輸入[NewPassword]";
                }

                if (!string.IsNullOrEmpty(strErrMsg))
                {
                    throw new Exception(strErrMsg);
                }
                #endregion

                #region 處理空字串
                User = User.Trim();
                NewPassword = NewPassword.Trim();
                #endregion

                #region Get Data
                var oldUsrDataObj = JamOAClient.MeInfo(User);

                if (oldUsrDataObj == null)
                    throw new Exception("找不到User");

                if (!JamOAClient.UpdateAccount(oldUsrDataObj.Id, NewPassword, oldUsrDataObj.Email, oldUsrDataObj.DisplayName, oldUsrDataObj.OfficalName, oldUsrDataObj.Title, oldUsrDataObj.TimeZone, oldUsrDataObj.Profile, EnableNotify == 1))
                    throw new Exception(JamOAClient.Message);                                
                #endregion
            }
            catch (Exception ex)
            {
                Code = Code != "200" ? Code : "408";
                Msg = ex.Message;
            }

            return Json(new
            {
                Code = Code != null ? Code : "500",
                Msg = Msg != null ? Msg : "",                
            }, JsonRequestBehavior.AllowGet);
        }
        #endregion        

        #endregion

        #region ========= [101]取得書櫃清單 =========
        [ApiFilter]
        public ActionResult GetBookcases(string Domain, string User)
        {
            IEnumerable<BookcaseModel> bookcaseModels = new List<BookcaseModel>();
            var bookcaseService = new BookcaseService();
            var permissionAppService = new PermissionAppService();

            try
            {
                #region Get Data
                bookcaseModels = bookcaseService.GetList(Domain).Where(b => permissionAppService.PermissionBookcase(User, b.Id));
                #endregion
            }
            catch (Exception ex)
            {
                Code = Code != "200" ? Code : "408";
                Msg = ex.Message;
            }

            return Json(new
            {
                Code = Code != null ? Code : "500",
                Msg = Msg != null ? Msg : "",
                Data = bookcaseModels.Select(p => new
                {
                    p.Id,
                    p.Name,
                    p.ThumbnailUrl,
                    CreateTimestamp = p.CreateTime,
                    p.FileMaxSize,
                    ApproveEnabled = p.IsApproveEnabled,
                    VersionControlEnabled = p.IsVersionControlEnabled,
                    p.IsDocumentExpireDelete
                })
            }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region ========= [102]取得文件類型清單 =========
        [ApiFilter]
        [HttpPost]
        public ActionResult GetDocumentTypes(string Domain)
        {
            DocumentTypeListModel docTypeModLtMod = new DocumentTypeListModel();
            DocumentTypeService docTypeSer = new DocumentTypeService();            

            string strErrMsg = string.Empty;

            try
            {                
                #region Get Data
                docTypeModLtMod = docTypeSer.GetList(Domain, docTypeModLtMod);
                #endregion
            }
            catch (Exception ex)
            {
                Code = Code != "200" ? Code : "408";
                Msg = ex.Message;                
            }

            return Json(new
            {
                Code = Code != null ? Code : "500",
                Msg = Msg != null ? Msg : "",
                Data = (docTypeModLtMod.Data != null) ? docTypeModLtMod.Data.Select(p => new
                {
                    Id = p.Id == null ? "" : p.Id.ToString(),
                    Name = p.Name == null ? "" : p.Name,
                    SubFileName = p.SubFileName == null ? "" : p.SubFileName,
                    FilterKeyword = p.FilterKeyword == null ? "" : p.FilterKeyword,                    

                }) : null,
            }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region ========= [104]取得文件頁面圖 =========
        [ApiFilter]
        [HttpPost]
        public ActionResult GetPageImages(string documentId)
        {
            PageImageService imgSer = new PageImageService();
            PageImageListModel imgLtMod = new PageImageListModel();

            string strErrMsg = string.Empty;

            try
            {
                #region User
                if (string.IsNullOrEmpty(documentId))
                {
                    strErrMsg += "參數未輸入[documentId]";
                }               

                if (!string.IsNullOrEmpty(strErrMsg))
                {
                    throw new Exception(strErrMsg);
                }
                #endregion

                #region 處理空字串
                documentId = documentId.Trim();
                #endregion

                #region Get Data
                imgLtMod = imgSer.GetLastPublicDocImageList(new Guid(documentId), imgLtMod);
                #endregion
            }
            catch (Exception ex)
            {
                Code = Code != "200" ? Code : "408";
                Msg = ex.Message;                
            }

            return Json(new
            {
                Code = Code != null ? Code : "500",
                Msg = Msg != null ? Msg : "",
                Data = (imgLtMod.Data != null) ? imgLtMod.Data.Select(p => new
                {
                    Order = p.Seq == null ? 0 : p.Seq,
                    ImageUrl = string.IsNullOrEmpty(p.ImageUrl) ? "" : p.ImageUrl,
                }) : null,
            }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region ========= [103]取得書櫃內文件夾及文件 =========
        [ApiFilter]
        public ActionResult GetBookcaseContents(string Domain, Guid bookcaseId, string User, long lastUpdateTime = 0)
        {
            IEnumerable<FolderAppModel> folderModels = new List<FolderAppModel>();
            var bookcaseAppService = new BookcaseAppService();
            var folderAppService = new FolderAppService();
            var permissionAppService = new PermissionAppService();

            try
            {
                #region Get Data
                folderModels = bookcaseAppService.GetFolders(Domain, User, bookcaseId).Where(f => permissionAppService.PermissionFolder(User, f.Id)).ToList();
                foreach (var folderModel in folderModels)
                {
                    folderModel.Documents = folderAppService.GetDocuments(Domain, User, folderModel.Id);
                }
                #endregion
            }
            catch (Exception ex)
            {
                Code = Code != "200" ? Code : "408";
                Msg = ex.Message;
            }

            return Json(new
            {
                Code = Code != null ? Code : "500",
                Msg = Msg != null ? Msg : "",
                Data = folderModels
            }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region ========= [106]審核通過 =========
        [ApiFilter]
        [HttpPost]
        public ActionResult Approve(string User, string documentId, string Domain)
        {
            DocumentService docSer = new DocumentService();
            DocumentListModel docLtMod = new DocumentListModel();

            string strErrMsg = string.Empty;

            try
            {
                #region 
                if (string.IsNullOrEmpty(User))
                {
                    strErrMsg += "參數未輸入[User]";
                }
                if (string.IsNullOrEmpty(documentId))
                {
                    strErrMsg += "參數未輸入[documentId]";
                }

                if (!string.IsNullOrEmpty(strErrMsg))
                {
                    throw new Exception(strErrMsg);
                }
                #endregion

                #region 處理空字串
                User = User.Trim();
                documentId = documentId.Trim();
                #endregion

                #region Get Data
                docLtMod = docSer.GetList(Domain, docLtMod, null, null, true);
                var DocObj = docLtMod.Data.Where(p => p.Id == new Guid(documentId)).FirstOrDefault();
                if (DocObj == null)
                    throw new Exception("找不到文件");

                if(!docSer.ChangeStatus(DocObj.Id, Enums.DocVerState.Release, User, out strErrMsg))
                    throw new Exception(strErrMsg);
                #endregion
            }
            catch (Exception ex)
            {
                Code = Code != "200" ? Code : "408";
                Msg = ex.Message;
            }

            return Json(new
            {
                Code = Code != null ? Code : "500",
                Msg = Msg != null ? Msg : "",
            }, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}