﻿using System;
using System.Web.Mvc;

using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Table;
using Microsoft.WindowsAzure;
using System.IO;
using System.Net;
using System.Text;
using JamZoo.Project.WebSite.Library;
using JamZoo.Project.WebSite.Library.Json;

namespace JamZoo.Project.WebSite.Areas.App.Controllers
{
    /// <summary>
    /// 基底Controller (Fanny)
    /// </summary>
    public class BaseAppController : Controller
    {       
        public string Code { get; set; }
        public string Msg { get; set; }

        public BaseAppController()
        {
            Code = "200";
            Msg = string.Empty;
        }

        protected override JsonResult Json(object data, string contentType,
            Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            if (behavior == JsonRequestBehavior.DenyGet
                && string.Equals(System.Web.HttpContext.Current.Request.HttpMethod, "GET",
                    StringComparison.OrdinalIgnoreCase))
                //Call JsonResult to throw the same exception as JsonResult
                return new JsonResult();
            return new JsonNetResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding
            };
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;

            var webException = filterContext.Exception as WebException;
            if (webException != null && webException.Response != null)
            {
                string result;
                using (Stream data = webException.Response.GetResponseStream())
                {
                    using (var reader = new StreamReader(data))
                    {
                        result = reader.ReadToEnd();
                    }
                }
                filterContext.Result = Json(
                    new
                    {
                        Code = ((HttpWebResponse)webException.Response).StatusCode,
#if DEBUG

                        Msg = result
#else
                    Msg = "伺服器錯誤！"
#endif
                    }, JsonRequestBehavior.AllowGet);
            }
            else
                filterContext.Result = Json(
                    new
                    {
                        Code = 500,
#if DEBUG

                        Msg = filterContext.Exception.Message
#else
                    Msg = "伺服器錯誤！"
#endif
                    }, JsonRequestBehavior.AllowGet);
        }
    }
}
