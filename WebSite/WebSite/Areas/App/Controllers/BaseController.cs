﻿using System.Web.Mvc;

using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Table;
using Microsoft.WindowsAzure;
using System.IO;

using JamZoo.Project.WebSite.Library;

namespace JamZoo.Project.WebSite.Areas.App.Controllers
{
    /// <summary>
    /// 基底Controller (Fanny)
    /// </summary>
    public class BaseController : Controller
    {       
        public string Code { get; set; }
        public string Msg { get; set; }

        public BaseController()
        {
            Code = "200";
            Msg = string.Empty;
        }
    }
}
