﻿using System;
using System.Linq;
using JamOA.sdk;
using JamZoo.Project.WebSite.Enums;
using JamZoo.Project.WebSite.Models;
using JamZoo.Project.WebSite.Service;

namespace JamZoo.Project.WebSite.Areas.Service
{
    public class PermissionAppService : BaseService
    {

        public PermissionModel NewInstance()
        {
            var newOne = new PermissionModel();
            return newOne;
        }

        public bool PermissionBookcase(string userId, Guid bookcaseId)
        {
            var organIds = JamOAClient.MeOrgans(userId).Select(org => org.Id);
            // Bookcase Permission
            var permissions = basedb.JWL_Permission.Where(
                p => (p.BookcaseId == bookcaseId) && (p.AccountId == userId || organIds.Contains(p.OrganId)))
                .Distinct()
                .ToList();

            // Folder Permission
            permissions.AddRange((from p in basedb.JWL_Permission
                                 join f in basedb.JWL_Folder on p.FolderId equals f.Id
                                 where (f.BookcaseId == bookcaseId) && (p.AccountId == userId || organIds.Contains(p.OrganId))
                                 select p).Distinct());

            // Document Permission
            permissions.AddRange((from p in basedb.JWL_Permission
                                 join d in basedb.JWL_Document on p.DocumentId equals d.Id
                                 where (d.BookcaseId == bookcaseId) && (p.AccountId == userId || organIds.Contains(p.OrganId))
                                 select p).Distinct());
            return permissions.Any();
        }

        public bool PermissionFolder(string userId, Guid folderId)
        {
            var organIds = JamOAClient.MeOrgans(userId).Select(org => org.Id);
            // Folder Permission
            var permissions = basedb.JWL_Permission.Where(
                p => (p.FolderId == folderId) && (p.AccountId == userId || organIds.Contains(p.OrganId))).Distinct().ToList();

            // Bookcase Permission
            permissions.AddRange((from p in basedb.JWL_Permission
                                  join f in basedb.JWL_Folder on p.BookcaseId equals f.BookcaseId
                                  where f.Id == folderId && (p.AccountId == userId || organIds.Contains(p.OrganId))
                                  select p).Distinct());

            // Document Permission
            permissions.AddRange((from p in basedb.JWL_Permission
                                 join d in basedb.JWL_Document on p.DocumentId equals d.Id
                                 join fd in basedb.JWL_FolderDocument on d.Id equals fd.DocumentId
                                 where (fd.FolderId == folderId) && (p.AccountId == userId || organIds.Contains(p.OrganId))
                                 select p).Distinct());
            return permissions.Any();
        }

        public ContentRoleType? GetDocumentContentRoleType(string userId, Guid documentId)
        {
            var organIds = JamOAClient.MeOrgans(userId).Select(org => org.Id);
            // Document Permission
            var permissions = basedb.JWL_Permission.Where(
                p => (p.DocumentId == documentId) && (p.AccountId == userId || organIds.Contains(p.OrganId))).Distinct().ToList();
            if (!permissions.Any())
            {
                // Folder Permission
                var folderPermissions = (from p in basedb.JWL_Permission
                                         join fd in basedb.JWL_FolderDocument on p.FolderId equals fd.FolderId
                                         join d in basedb.JWL_Document on fd.DocumentId equals d.Id
                                         where d.Id == documentId && (p.AccountId == userId || organIds.Contains(p.OrganId))
                                         select p).Distinct();
                permissions.AddRange(folderPermissions);

                if (!permissions.Any())
                {
                    // Bookcase Permission
                    permissions.AddRange((from p in basedb.JWL_Permission
                                         join d in basedb.JWL_Document on p.BookcaseId equals d.BookcaseId
                                         where p.AccountId == userId || organIds.Contains(p.OrganId)
                                         select p).Distinct());
                }
            }
            ContentRoleType? maxContentRoleType = null;
            foreach (var bookcasePermission in permissions)
            {
                var contentRoleType = (ContentRoleType)Enum.Parse(typeof(ContentRoleType), bookcasePermission.RoleId);
                if (maxContentRoleType == null || contentRoleType > maxContentRoleType)
                    maxContentRoleType = contentRoleType;
            }
            return maxContentRoleType;
        }

    }

}