﻿using System;
using System.Collections.Generic;
using System.Linq;
using JamZoo.Project.WebSite.Areas.App.Models;
using JamZoo.Project.WebSite.DbContext;
using JamZoo.Project.WebSite.Library;
using JamZoo.Project.WebSite.Models;
using JamZoo.Project.WebSite.Service;

namespace JamZoo.Project.WebSite.Areas.Service
{
    public class BookcaseAppService : BaseService
    {
        public IEnumerable<FolderAppModel> GetFolders(string domain, string userId, Guid bookcaseId)
        {
            var folders = basedb.JWL_Folder.Where(f => f.Domain == domain && f.BookcaseId == bookcaseId).OrderBy(p => p.Name);
            var rztObj = folders.Inject<JWL_Folder, FolderAppModel>();
            return rztObj;
        }

    }
}
