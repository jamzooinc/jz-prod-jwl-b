﻿using System;
using System.Collections.Generic;
using System.Linq;
using JamOA.sdk;
using JamZoo.Project.WebSite.Areas.App.Models;
using JamZoo.Project.WebSite.Enums;
using JamZoo.Project.WebSite.Service;

namespace JamZoo.Project.WebSite.Areas.Service
{
    public class FolderAppService : BaseService
    {
        public IEnumerable<DocumentAppModel> GetDocuments(string domain, string userId, Guid folderId)
        {
            var permissionAppService = new PermissionAppService();
            var documentAppModels = new Dictionary<Guid,DocumentAppModel>();
            var currentTime = DateTime.Now;

            var o_query = from d in basedb.JWL_Document
                          join v in basedb.JWL_DocumentVersion on d.Id equals v.DocumentId
                          join fd in basedb.JWL_FolderDocument on d.Id equals fd.DocumentId
                          join dt in basedb.JWL_DocumentType on d.DocumentTypeId equals dt.Id
                          where d.Domain == domain && fd.FolderId == folderId && v.State > 0 && (v.Expire == null || v.Expire > currentTime)
                          select new { d, v, dt };

            o_query = o_query.Distinct().OrderBy(p => p.v.DisplayName);
            foreach (var oEntity in o_query)
            {
                var documentAppModel = new DocumentAppModel
                {
                    Id = oEntity.d.Id,
                    DisplayName = oEntity.v.DisplayName,
                    CategoryText1 =
                        oEntity.d.CategoryItemId1 == null
                            ? ""
                            : basedb.JWL_CategoryItem.First(i => i.Id == oEntity.d.CategoryItemId1).Name,
                    CategoryText2 =
                        oEntity.d.CategoryItemId2 == null
                            ? ""
                            : basedb.JWL_CategoryItem.First(i => i.Id == oEntity.d.CategoryItemId2).Name,
                    CategoryText3 =
                        oEntity.d.CategoryItemId3 == null
                            ? ""
                            : basedb.JWL_CategoryItem.First(i => i.Id == oEntity.d.CategoryItemId3).Name,
                    DocumentTypeId = oEntity.d.DocumentTypeId,
                    DocumentTypeName = oEntity.dt.Name,
                    State = oEntity.v.State,
                    Version = oEntity.v.Version.ToString(),
                    DisplayVersion = oEntity.v.DisplayVersion,
                    ModifyTimestamp = oEntity.v.ModifyTime,
                    ModifyAccountName = JamOAClient.MeInfo(oEntity.v.ModifyAccountId).DisplayName,
                    ExpireTimestamp = oEntity.v.Expire ?? DateTime.MinValue,
                    CoverThumbnailUrl = oEntity.v.CoverThumbnailUrl,
                    FileName = oEntity.v.FileName,
                    FileSize = oEntity.v.FileSize ?? 0,
                    FileUrl = oEntity.v.FileUrl,
                    IsPageImage = basedb.JWL_DocumentType.First(t => t.Id==oEntity.d.DocumentTypeId).ConvertPageImage,
                    Permission = (int?) permissionAppService.GetDocumentContentRoleType(userId, oEntity.d.Id) ?? -1,
                    Public = oEntity.v.IsPublic
                };
                var subscribe = basedb.JWL_Subscribe.FirstOrDefault(s => s.DocumentId == oEntity.d.Id);
                documentAppModel.Subscribe = subscribe == null ? 0 : (subscribe.IsAutoLoad ? 2 : 1);
                if (documentAppModel.State == 2 ||
                    (documentAppModel.Permission == (int) ContentRoleType.Approve && documentAppModel.State == 1))
                {
                    if (documentAppModels.ContainsKey(oEntity.d.Id))
                    {
                        var currentDocumentAppModel = documentAppModels[oEntity.d.Id];
                        var currentVersion = decimal.Parse(currentDocumentAppModel.Version);
                        if (oEntity.v.Version > currentVersion)
                            documentAppModels[oEntity.d.Id] = documentAppModel;
                    }
                    else
                        documentAppModels.Add(oEntity.d.Id, documentAppModel);
                }
            }

            return new List<DocumentAppModel>(documentAppModels.Values.Where(f => f.Permission >= 0));
        }

    }
}
