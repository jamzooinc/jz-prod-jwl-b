﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace JamZoo.Project.WebSite.Library.Json
{
    public class NullResolver : DefaultContractResolver
    {
        protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
        {
            return type.GetProperties()
                    .Select(p => {
                        var jp = base.CreateProperty(p, memberSerialization);
                        jp.ValueProvider = new NullValueProvider(p);
                        return jp;
                    }).ToList();
        }
    }

    public class NullValueProvider : IValueProvider
    {
        private readonly PropertyInfo _memberInfo;
        public NullValueProvider(PropertyInfo memberInfo)
        {
            _memberInfo = memberInfo;
        }

        public object GetValue(object target)
        {
            var result = _memberInfo.GetValue(target, null);
            if (result == null)
            {
                var propertyType = _memberInfo.PropertyType;
                if (propertyType == typeof(string))
                    result = String.Empty;
                else if (propertyType.GetInterfaces().Contains(typeof(IEnumerable)))
                    result = new int[0];
                else if (propertyType.IsGenericType && propertyType.GetGenericTypeDefinition() == typeof(Nullable<>) && propertyType.IsValueType)
                    result = Activator.CreateInstance(Nullable.GetUnderlyingType(propertyType));
            }
            return result;
        }

        public void SetValue(object target, object value)
        {
            _memberInfo.SetValue(target, value, null);
        }
    }
}
