﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace JamZoo.Project.WebSite.Library.Json
{
    public class TimestampConverter : DateTimeConverterBase
    {
        /// <summary>
        /// Writes the JSON representation of the object.
        /// </summary>
        /// <param name="writer">The <see cref="T:Newtonsoft.Json.JsonWriter"/> to write to.</param><param name="value">The value.</param><param name="serializer">The calling serializer.</param>
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if ((DateTime)value == DateTime.MinValue)
            {
                writer.WriteValue(0);
                return;
            }
            if (value is DateTime)
            {
                var time = ((DateTime)value);

                if (time == DateTime.MinValue)
                {
                    writer.WriteValue(0);
                }

                var epoc = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
                var delta = time.ToUniversalTime() - epoc;

                if (delta.TotalSeconds < 0)
                    throw new ArgumentOutOfRangeException("DateTime Error!");

                writer.WriteValue((long)delta.TotalSeconds);
            }
        }

        /// <summary>
        ///   Reads the JSON representation of the object.
        /// </summary>
        /// <param name = "reader">The <see cref = "JsonReader" /> to read from.</param>
        /// <param name = "objectType">Type of the object.</param>
        /// <param name = "existingValue">The existing value of object being read.</param>
        /// <param name = "serializer">The calling serializer.</param>
        /// <returns>The object value.</returns>
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
                                        JsonSerializer serializer)
        {
            if (reader.TokenType != JsonToken.Integer)
                throw new Exception("Wrong Token Type");

            var ticks = (long)reader.Value;
            var ret = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            return ret.AddSeconds(ticks);
        }
    }
}
