﻿using System.Collections.Generic;
using System.Data.Objects.DataClasses;
using System.Linq;
using EmitMapper;
using EmitMapper.MappingConfiguration;

namespace JamZoo.Project.WebSite.Library
{
    public static class DataTransferExtension
    {
        /// <summary>
        /// 將來源集合中的物件所有屬性值填入目的物件中，傳回一新目的物件集合
        /// </summary>
        /// <typeparam name="TSource">來源物件類別</typeparam>
        /// <typeparam name="TTarget">目的物件類別</typeparam>
        /// <param name="sources">來源物件集合</param>
        /// <param name="ignoreNames">略過填入目的物件屬性名列表</param>
        /// <returns></returns>
        public static IEnumerable<TTarget> Inject<TSource, TTarget>(this IEnumerable<TSource> sources,
            params string[] ignoreNames)
            where TTarget : new()
        {
            var mapConfig = new DefaultMapConfig().ShallowMap().IgnoreMembers<TSource, TTarget>(ignoreNames);
            var mapper = ObjectMapperManager.DefaultInstance.GetMapper<TSource, TTarget>(mapConfig);
            return sources.Select(mapper.Map);
        }
        /// <summary>
        /// 將來源物件所有屬性值填入目的物件中，傳回一新目的物件
        /// </summary>
        /// <typeparam name="TSource">來源物件類別</typeparam>
        /// <typeparam name="TTarget">目的物件類別</typeparam>
        /// <param name="model">來源物件</param>
        /// <param name="ignoreNames">略過填入目的物件屬性名列表</param>
        /// <returns></returns>
        public static TTarget Inject<TSource, TTarget>(this TSource model,
            params string[] ignoreNames)
            where TTarget : new()
        {
            var target = new TTarget();
            model.Inject(target, ignoreNames);
            return target;
        }

        /// <summary>
        /// 將來源物件所有屬性值填入指定目的物件中
        /// </summary>
        /// <typeparam name="TSource">來源物件類別</typeparam>
        /// <typeparam name="TTarget">目的物件類別</typeparam>
        /// <param name="source">來源物件</param>
        /// <param name="target">目的物件</param>
        /// <param name="ignoreNames">略過填入目的物件屬性名列表</param>
        public static void Inject<TSource, TTarget>(this TSource source, TTarget target,
            params string[] ignoreNames)
        {
            var model = target as EntityObject;
            if (model != null && ignoreNames.Length == 0)
            {
                ignoreNames = model.EntityKey.EntityKeyValues.Select(v => v.Key).ToArray();
            }
            var mapConfig = new DefaultMapConfig();
            if (ignoreNames.Length > 0)
                mapConfig.IgnoreMembers<TSource, TTarget>(ignoreNames);
            var mapper = ObjectMapperManager.DefaultInstance.GetMapper<TSource, TTarget>(mapConfig);
            mapper.Map(source, target);
        }
    }
}