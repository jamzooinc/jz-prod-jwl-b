﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.IO;
using System.Drawing.Imaging;

using GhostscriptSharp;
using GhostscriptSharp.Settings;

using OFFICECORE = Microsoft.Office.Core;
//using POWERPOINT = Microsoft.Office.Interop.PowerPoint;
//using WORD = Microsoft.Office.Interop.Word;

namespace JamZoo.Project.WebSite.Library.DocConvert
{
    public class DocConverter
    {
        public void PdfToImage(string filePath, string FolderPath, int PageIdx = 0)
        {
            if (!File.Exists(filePath))
                throw new Exception("File not exists");

            if (!Directory.Exists(FolderPath))
                throw new Exception("Folder not exists");

            FileInfo fi = new FileInfo(filePath);

            if (PageIdx <= 0)
            {
                int PageCnt = 0;

                #region 取得頁數
                if (filePath != null)
                {
                    try
                    {
                        using (var stream = new StreamReader(File.OpenRead(filePath)))
                        {
                            var regex = new Regex(@"/Type\s*/Page[^s]");
                            var matches = regex.Matches(stream.ReadToEnd());

                            PageCnt = matches.Count;
                        }
                    }
                    catch (Exception e)
                    {
                        PageCnt = 0;
                    }
                }
                #endregion

                for (int cnt = 1; cnt <= PageCnt; cnt++)
                {
                    string FileName = string.Format("{0}\\{1}_Page{2}.jpg", FolderPath, fi.Name.Split('.')[0], cnt);
                    
                    GhostscriptWrapper.GeneratePageThumbs(filePath, FileName, cnt, cnt, 130, 130);
                }
            }
            else
            {
                string FileName = string.Format("{0}\\{1}_Page{2}.jpg", FolderPath, fi.Name.Split('.')[0], PageIdx);

                GhostscriptWrapper.GeneratePageThumbs(filePath, FileName, PageIdx, PageIdx, 130, 130);
            }


        }

        public static int GetNumPages(string path)
        {
            if (path != null)
            {
                try
                {
                    using (var stream = new StreamReader(File.OpenRead(path)))
                    {
                        var regex = new Regex(@"/Type\s*/Page[^s]");
                        var matches = regex.Matches(stream.ReadToEnd());

                        return matches.Count;
                    }
                }
                catch (Exception e)
                {
                    return 0;
                }
            }
            return 0;
        }

        public void PptToImage(string filePath, string FolderPath)
        {
            if (!File.Exists(filePath))
                throw new Exception("File not exists");

            if (!Directory.Exists(FolderPath))
                throw new Exception("Folder not exists");

            //POWERPOINT.Application App = new Microsoft.Office.Interop.PowerPoint.Application();

            //POWERPOINT.Presentation pres = App.Presentations.Open(filePath, OFFICECORE.MsoTriState.msoTrue, OFFICECORE.MsoTriState.msoFalse, OFFICECORE.MsoTriState.msoFalse);
            
            //pres.SaveAs(FolderPath, POWERPOINT.PpSaveAsFileType.ppSaveAsJPG, OFFICECORE.MsoTriState.msoFalse);

            //pres.Close();
            GC.Collect();
        }

        public void PptToImageByPageIdx(string filePath, string FolderPath, int PageIdx, string FileName)
        {
            if (!File.Exists(filePath))
                throw new Exception("File not exists");

            if (!Directory.Exists(FolderPath))
                throw new Exception("Folder not exists");

            //var app = new Microsoft.Office.Interop.PowerPoint.Application();

            //var ppt = app.Presentations.Open(filePath, Microsoft.Office.Core.MsoTriState.msoFalse, Microsoft.Office.Core.MsoTriState.msoFalse, Microsoft.Office.Core.MsoTriState.msoFalse);

            //var fileName = Path.GetFileNameWithoutExtension(filePath);

            //if (PageIdx <= 0)
            //    throw new Exception("PageIdx cant under 1");

            //if (PageIdx - 1 > ppt.Slides.Count)
            //    throw new Exception("Page not exists");

            //ppt.Slides[PageIdx].Export(FolderPath + string.Format("\\{0}.png", FileName), "jpg", 1280, 720);

            //ppt.Close();
            //app.Quit();
            GC.Collect();
        }

        public void DocToImage(string filePath, string FolderPath)
        {
            //var app = new Microsoft.Office.Interop.Word.Application();

            //FileInfo fi = new FileInfo(filePath);

            //if (!fi.Exists)
            //    throw new Exception("File not exists");

            //app.Visible = true;

            //var doc = app.Documents.Open(filePath);

            //doc.ShowGrammaticalErrors = false;
            //doc.ShowRevisions = false;
            //doc.ShowSpellingErrors = false;

            //if (!Directory.Exists(FolderPath))
            //{
            //    Directory.CreateDirectory(FolderPath);
            //}

            //foreach (Microsoft.Office.Interop.Word.Window window in doc.Windows)
            //{
            //    int cnvIdx = 1;
            //    int breakCnt = 0;
            //    foreach (Microsoft.Office.Interop.Word.Pane pane in window.Panes)
            //    {
            //        while (cnvIdx <= pane.Pages.Count)
            //        {
            //            if (breakCnt >= 3)
            //            {
            //                cnvIdx = pane.Pages.Count;
            //                break;
            //            }

            //            var page = pane.Pages[cnvIdx];
            //            var bits = page.EnhMetaFileBits;

            //            try
            //            {
            //                var targetTmp = string.Format("{0}\\{1}_tmpPage{2}.{3}", FolderPath, fi.Name.Split('.')[0], cnvIdx, "png");
            //                var target = string.Format("{0}\\{1}_Page{2}.{3}", FolderPath, fi.Name.Split('.')[0], cnvIdx, "jpg");

            //                using (var ms = new MemoryStream((byte[])(bits)))
            //                {
            //                    var image = System.Drawing.Image.FromStream(ms);
            //                    image.Save(targetTmp, ImageFormat.Png);

            //                    Bitmap bmp = new Bitmap(image);
            //                    Graphics graf = Graphics.FromImage(bmp);
            //                    graf.Clear(Color.White);
            //                    graf.DrawImage(image, 0, 0, image.Width, image.Height);
            //                    bmp.Save(target, ImageFormat.Jpeg);

            //                    File.Delete(targetTmp);
            //                }
            //                cnvIdx++;
            //                breakCnt = 0;
            //            }
            //            catch (System.Exception ex)
            //            {
            //                breakCnt++;
            //            }
            //        }
            //    }
            //}
            //doc.Close(Type.Missing, Type.Missing, Type.Missing);
            //app.Quit(Type.Missing, Type.Missing, Type.Missing);
            GC.Collect();
        }
    }
}