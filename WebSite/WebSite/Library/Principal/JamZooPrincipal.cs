﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Security;
using System.Web.Mvc;


namespace JamZoo.Project.WebSite.Library.Principal
{
    /// <summary>
    /// 網站登入者的基本資料
    /// </summary>
    public class WebSiteUser
    {
        public string Id { get; set; }
        public string AccountType { get; set; }
        public string Domain { get; set; }
        public string Account { get; set; }
        public string Name { get; set; }
        public string[] Roles { get; set; }

        public HttpCookie reSet()
        {
            bool isCookiePersistent = false;
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            string jsonAccountModel = serializer.Serialize(this);

            FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(1,
                      this.Id, DateTime.Now, DateTime.Now.AddMinutes(60), isCookiePersistent, jsonAccountModel);

            string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
            HttpCookie authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);

            if (true == isCookiePersistent)
                authCookie.Expires = authTicket.Expiration;

            return authCookie;
        }
    }

    /// <summary>
    /// http://primaryobjects.com/CMS/Article147.aspx
    /// </summary>
    public class JamZooPrincipal : IPrincipal
    {
        public IIdentity Identity
        {
            get;
            private set;
        }

        public JamZooPrincipal(IIdentity identity)
        {

            Identity = identity;
        }

        public WebSiteUser User { get; set; }

        public bool IsInRole(string role)
        {
            if (User != null)
            {
                if (User.Roles != null)
                {
                    if (User.Roles.Length > 0)
                    {
                        return User.Roles.Contains(role);
                    }
                }
            }
            return false;
        }
    }
}