﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Providers;
using JamZoo.Project.WebSite.Models;
using JamZoo.Project.WebSite.Service;

using JamOA.sdk;

namespace JamZoo.Project.WebSite.Library.Security
{
    public class BasicRoleProvider : DefaultRoleProvider
    {
        // Emp SN
        public override string[] GetRolesForUser(string userid)
        {
            var roleLtObj = JamOAClient.MeRoles(userid);

            return (roleLtObj == null) ? null : roleLtObj.Select(p => p.Id .ToString()).ToArray<string>();                                        
        }
    }
}