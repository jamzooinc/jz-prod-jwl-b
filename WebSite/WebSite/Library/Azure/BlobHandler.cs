﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Table;
using Microsoft.WindowsAzure;
using System.IO;

namespace JamZoo.Project.WebSite.Library.Azure
{
    public class BlobHandler
    {
        // Retrieve storage account from connection string.
        CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
        CloudConfigurationManager.GetSetting("StorageConnectionString"));

        private string DirecoryUrl;

        /// <summary>
        /// Receives the users Id for where the pictures are and creates 
        /// a blob storage with that name if it does not exist.
        /// </summary>
        /// <param name="imageDirecoryUrl"></param>
        public BlobHandler(string DirecoryUrl)
        {            
            this.DirecoryUrl = DirecoryUrl;
            // Create the blob client.
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            // Retrieve a reference to a container. 
            CloudBlobContainer container = blobClient.GetContainerReference(DirecoryUrl);

            // Create the container if it doesn't already exist.
            container.CreateIfNotExists();

            //Make available to everyone
            container.SetPermissions(
                new BlobContainerPermissions
                {
                    PublicAccess = BlobContainerPublicAccessType.Blob
                });
        }

        public void Upload(IEnumerable<HttpPostedFileBase> file)
        {
            // Create the blob client.
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            // Retrieve a reference to a container. 
            CloudBlobContainer container = blobClient.GetContainerReference(DirecoryUrl);

            if (file != null)
            {
                foreach (var f in file)
                {
                    if (f != null)
                    {
                        CloudBlockBlob blockBlob = container.GetBlockBlobReference(f.FileName);
                        blockBlob.UploadFromStream(f.InputStream);
                    }
                }
            }
        }

        public void Upload(HttpPostedFileBase file, string FileName)
        {
            // Create the blob client.
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            // Retrieve a reference to a container. 
            CloudBlobContainer container = blobClient.GetContainerReference(DirecoryUrl);

            if (file != null)
            {
                CloudBlockBlob blockBlob = container.GetBlockBlobReference(FileName);
                blockBlob.UploadFromStream(file.InputStream);
            }
        }

        public void Upload(string filePath, string FileName)
        {
            if(!File.Exists(filePath))
                throw new Exception(string.Format("File not found[{0}]", filePath));

            FileStream fsFile = new FileStream(filePath, FileMode.Open);

            try
            {
                // Create the blob client.
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                // Retrieve a reference to a container. 
                CloudBlobContainer container = blobClient.GetContainerReference(DirecoryUrl);

                CloudBlockBlob blockBlob = container.GetBlockBlobReference(FileName);
                blockBlob.UploadFromStream(fsFile);
            }
            catch(Exception ex)
            {                
                throw new Exception(ex.InnerException == null ? ex.Message : ex.InnerException.ToString());
            }
            finally
            {
                fsFile.Close();
            }
        }

        internal void DeleteFile(string name)
        {
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer container = blobClient.GetContainerReference(DirecoryUrl);
            CloudBlockBlob blob = container.GetBlockBlobReference(name);
            blob.DeleteIfExists();
        }


        public List<string> GetFiles()
        {
            // Create the blob client. 
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            // Retrieve reference to a previously created container.
            CloudBlobContainer container = blobClient.GetContainerReference(DirecoryUrl);

            List<string> blobs = new List<string>();

            // Loop over blobs within the container and output the URI to each of them
            foreach (var blobItem in container.ListBlobs())
                blobs.Add(blobItem.Uri.ToString());

            return blobs;
        }
    }
}