﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JamZoo.Project.WebSite.Controllers
{
    [Authorize]
    public class BaseController : Controller
    {
        public static string WLBlob = System.Web.Configuration.WebConfigurationManager.AppSettings["WLBlob"];   
    }
}
