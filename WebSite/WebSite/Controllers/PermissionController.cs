﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JamOA.sdk;
using JamZoo.Project.WebSite.Enums;
using JamZoo.Project.WebSite.Library.Principal;
using JamZoo.Project.WebSite.Models;
using JamZoo.Project.WebSite.Service;

namespace JamZoo.Project.WebSite.Controllers
{
    [Authorize]
    public class PermissionController : BaseController
    {
        PermissionService Service = new PermissionService();

        public ActionResult List(PermissionListModel criteria, string ErrMsg, Guid? folderId = null, Guid? documentId = null)
        {
            PermissionListModel model = null;
            if (folderId == null && documentId == null)
            {
                if (this.ControllerContext.HttpContext.Request.Cookies.AllKeys.Contains("bookcaseMod"))
                {
                    // PermissionContentType.Bookcase
                    System.Web.Script.Serialization.JavaScriptSerializer serializer =
                        new System.Web.Script.Serialization.JavaScriptSerializer();
                    HttpCookie bookCookie = Request.Cookies.Get("bookcaseMod");
                    var contentId = serializer.Deserialize<BookcaseModel>(bookCookie.Value).Id;
                    model = Service.GetList(UserManager.User.Domain, PermissionContentType.Bookcase, contentId, criteria);
                    model.BookcaseId = contentId;
                }
            }
            else
            {
                if (folderId.HasValue)
                {
                    // PermissionContentType.Folder
                    model = Service.GetList(UserManager.User.Domain, PermissionContentType.Folder, folderId.Value, criteria);
                }
                else
                {
                    // PermissionContentType.Document
                    model = Service.GetList(UserManager.User.Domain, PermissionContentType.Document, documentId.Value, criteria);
                }
            }

            return View(model);
        }

        public ActionResult GetOrgans(string id = null)
        {
            if (string.IsNullOrEmpty(id))
                id = UserManager.User.Domain;
            var ous = JamOAClient.GetChildrenOrgans(id);
            var items = ous.Select(ou => new
            {
                id = ou.Id,
                text = ou.DisplayName,
                child = ou.HasChilden ? 1 : 0
            });
            return Json(new
            {
                id = id == UserManager.User.Domain ? "0" : id,
                item = items
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetOrganAccounts(string organId)
        {
            var accounts = JamOAClient.OrganAccounts(organId).Select(a => new
            {
                id = a.Id,
                a.DisplayName,
                a.Title,
                a.Email
            });
            return Json(accounts, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetGroups()
        {
            var groups = JamOAClient.OrganGroups(UserManager.User.Domain).Select(a => new
            {
                id = a.Id,
                a.DisplayName,
            });
            return Json(groups, JsonRequestBehavior.AllowGet);
        }

        #region CRUD

        public ActionResult Add(Guid? id, int page = 1)
        {
            GetRoleList();

            var permissionModel = new PermissionModel();
            permissionModel.page = page;
            permissionModel.Mode = EditPageMode.Create;
            permissionModel.BookcaseId = id;
            return View(permissionModel);
        }

        private void GetRoleList()
        {
            var roleList = PermissionService.Roles.Select(r => new SelectListItem
            {
                Value = r.Id,
                Text = r.DisplayName
            });
            ViewData["Roles"] = roleList;
        }

        [HttpPost]
        public ActionResult Create(PermissionModel model, Guid? folderId = null, Guid? documentId = null)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    string ErrMsgs = string.Empty;

                    if (Service.Create(model, out ErrMsgs))
                    {
                        return RedirectToAction("List", new { page = model.page, folderId = model.FolderId, documentId = model.DocumentId });
                    }

                    ModelState.AddModelError("message", ErrMsgs);

                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("message", ex.Message);
                }
            }

            return View("Add", model);
        }


        public ActionResult Edit(Guid id, int page = 1)
        {
            GetRoleList();
            var permissionModel = Service.Get(id);
            permissionModel.Mode = EditPageMode.Update;
            permissionModel.page = page;
            return View("Add", permissionModel);
        }

        [HttpPost]
        public ActionResult Update(PermissionModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    string ErrMsgs = string.Empty;

                    if (Service.Update(model, out ErrMsgs))
                    {
                        return RedirectToAction("List", new { page = model.page, folderId = model.FolderId, documentId = model.DocumentId });
                    }
                    else
                    {
                        ModelState.AddModelError("message", "修改失敗:" + ErrMsgs);
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("message", ex.Message);
                }
            }

            model.Mode = EditPageMode.Update;
            return View("Add", model);
        }

        public ActionResult Delete(Guid id, Guid? folderId = null, Guid? documentId = null, int page = 1)
        {
            try
            {
                string errorMsg = string.Empty;

                if (Service.Delete(id, out errorMsg))
                {
                    TempData["delete"] = true;
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("message", ex.Message);
            }
            return RedirectToAction("List", new { page, folderId, documentId });
        }

        #endregion
    }
}
