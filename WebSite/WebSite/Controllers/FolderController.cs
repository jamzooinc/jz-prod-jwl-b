﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.IO;

using JamZoo.Project.WebSite.Library.Azure;

using JamZoo.Project.WebSite.Models;
using JamZoo.Project.WebSite.Service;
using JamZoo.Project.WebSite.ViewModels;
using JamZoo.Project.WebSite.Enums;
using JamZoo.Project.WebSite.Library.Principal;

namespace JamZoo.Project.WebSite.Controllers
{
    public class FolderController : BaseController
    {
        BookcaseService bookSer = new BookcaseService();
        FolderService Service = new FolderService();
        CategoryService cateSer = new CategoryService();
        DocumentTypeService docTypSer = new DocumentTypeService();
        FolderDocumentTypeService fdtSer = new FolderDocumentTypeService();        
        public FolderController()
        {
            
        }
        
        #region CRUD
        //GET: /Account/List
        [Authorize]
        public ActionResult List(FolderListModel criteria)
        {
            try
            {
                BookcaseModel bookcaseMod = new BookcaseModel();

                if (this.ControllerContext.HttpContext.Request.Cookies.AllKeys.Contains("bookcaseMod"))
                {
                    System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                    HttpCookie bookCookie = Request.Cookies.Get("bookcaseMod");
                    bookcaseMod = serializer.Deserialize<BookcaseModel>(bookCookie.Value.ToString());
                }

                FolderListModel model = Service.GetList(UserManager.User.Domain, bookcaseMod.Id, criteria);

                return View(model);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("message", ex.Message);
            }

            return View(criteria);
        }

        
        [Authorize]
        public ActionResult Add(int page = 1)
        {
            BookcaseModel bookcaseMod = new BookcaseModel();            

            FolderDocTypeModel model = new FolderDocTypeModel();            
            model.DocTypeList = docTypSer.GetList(UserManager.User.Domain);
            ViewData["DocTypeList_Data"] = model.DocTypeList.Select(o => new SelectListItem { Text = o.Name, Value = o.Id.ToString() });

            FolderDocumentTypeService fdtSer = new FolderDocumentTypeService();            
            ViewData["FolderDocType_Data"] = model.FolderDocType.Select(o => new SelectListItem { Text = o.DocumentTypeName, Value = o.DocumentTypeId.ToString() });
            model.DocTypeIdList = string.Join(",", model.FolderDocType.Select(r => r.DocumentTypeId.ToString()));

            if (this.ControllerContext.HttpContext.Request.Cookies.AllKeys.Contains("bookcaseMod"))
            {
                System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                HttpCookie bookCookie = Request.Cookies.Get("bookcaseMod");
                bookcaseMod = serializer.Deserialize<BookcaseModel>(bookCookie.Value.ToString());
            }                        

            FolderModel entity = Service.NewInstance();
            entity.BookcaseSelectItem = bookSer.GetList(UserManager.User.Domain, new Guid());
            entity.BookcaseId = bookcaseMod.Id;
            entity.page = page;
            entity.Mode = EditPageMode.Create;
            return View(entity);
        }
       
        [Authorize]
        [HttpPost]
        public ActionResult Create(FolderModel model, HttpPostedFileBase file)
        {
            model.CreateAccountId = UserManager.User.Id;
            model.Domain = UserManager.User.Domain;

            if (ModelState.IsValid)
            {
                try
                {
                    string ErrMsgs = string.Empty;

                    string azurePaht = UserManager.User.Domain.ToLower();
                    string FileName = string.Empty;

                    model.ThumbnailUrl = string.Empty;

                    #region 處理圖片
                    if (file != null)
                    {
                        FileInfo fi = new FileInfo(file.FileName);

                        FileName = string.Format("{0}{1}", Guid.NewGuid(), fi.Extension);

                        BlobHandler bh = new BlobHandler(azurePaht);
                        bh.Upload(file, FileName);

                        model.ThumbnailUrl = string.Format("{0}{1}/{2}", WLBlob, azurePaht, FileName);
                    }
                    #endregion                    

                    if (Service.Create(model, out ErrMsgs))
                    {
                        if (!string.IsNullOrEmpty(model.DocTypeIdList))
                        {
                            var DocTypeIds = model.DocTypeIdList.TrimEnd(',').Split(',');

                            if (!fdtSer.DelAllByFolder(model.Id, out ErrMsgs))
                                throw new Exception(ErrMsgs);

                            foreach (var DocId in DocTypeIds)
                            {
                                if (!fdtSer.Create(new FolderDocumentTypeModel { FolderId = model.Id, DocumentTypeId = new Guid(DocId) }, out ErrMsgs))
                                    throw new Exception(ErrMsgs);
                            }
                        }
                        else
                        {
                            if (!fdtSer.DelAllByFolder(model.Id, out ErrMsgs))
                                throw new Exception(ErrMsgs);
                        }

                        return RedirectToAction("List", new { page = model.page });
                    }
                    else
                    {
                        BlobHandler bh = new BlobHandler(azurePaht.ToLower());
                        bh.DeleteFile(FileName);
                    }

                    ModelState.AddModelError("message", ErrMsgs);

                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("message", ex.Message);
                }
            }

            model.Mode = EditPageMode.Create;
            model.BookcaseSelectItem = bookSer.GetList(UserManager.User.Domain, model.BookcaseId);
            return View("Add", model);
        }
        
        [Authorize]
        public ActionResult Edit(Guid id, int page = 1)
        {
            FolderModel model = Service.Get(id);

            model.Id = id;
            model.DocTypeList = docTypSer.GetList(UserManager.User.Domain);
            ViewData["DocTypeList_Data"] = model.DocTypeList.Select(o => new SelectListItem { Text = o.Name, Value = o.Id.ToString() });

            FolderDocumentTypeService fdtSer = new FolderDocumentTypeService();
            model.FolderDocType = fdtSer.GetList(id);
            ViewData["FolderDocType_Data"] = model.FolderDocType.Select(o => new SelectListItem { Text = o.DocumentTypeName, Value = o.DocumentTypeId.ToString() });
            model.DocTypeIdList = string.Join(",", model.FolderDocType.Select(r => r.DocumentTypeId.ToString()));

            model.BookcaseSelectItem = bookSer.GetList(UserManager.User.Domain, id);
            model.Mode = EditPageMode.Update;            
            model.page = page;
            return View("Add", model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Update(FolderModel model, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    string ErrMsgs = string.Empty;

                    string azurePaht = UserManager.User.Domain.ToLower();
                    string FileName = string.Empty;

                    #region 處理圖片
                    if (file != null)
                    {
                        FileInfo fi = new FileInfo(file.FileName);

                        FileName = model.ThumbnailUrl == null ?
                            string.Format("{0}{1}", Guid.NewGuid(), fi.Extension) :
                            model.ThumbnailUrl.Replace(WLBlob, "").Replace(azurePaht + "/", "");                                           

                        BlobHandler bh = new BlobHandler(azurePaht.ToLower());
                        bh.DeleteFile(FileName);                        

                        FileName = string.Format("{0}{1}", Guid.NewGuid(), fi.Extension);

                        bh.Upload(file, FileName);

                        model.ThumbnailUrl = string.Format("{0}{1}/{2}", WLBlob, azurePaht, FileName);
                    }
                    #endregion

                    if (Service.Update(model, out ErrMsgs))
                    {
                        if (!string.IsNullOrEmpty(model.DocTypeIdList))
                        {
                            var DocTypeIds = model.DocTypeIdList.TrimEnd(',').Split(',');

                            if (!fdtSer.DelAllByFolder(model.Id, out ErrMsgs))
                                throw new Exception(ErrMsgs);

                            foreach (var DocId in DocTypeIds)
                            {
                                if (!fdtSer.Create(new FolderDocumentTypeModel { FolderId = model.Id, DocumentTypeId = new Guid(DocId) }, out ErrMsgs))
                                    throw new Exception(ErrMsgs);
                            }
                        }
                        else
                        {
                            if (!fdtSer.DelAllByFolder(model.Id, out ErrMsgs))
                                throw new Exception(ErrMsgs);
                        }

                        return RedirectToAction("List", new { page = model.page });
                    }
                    else
                    {
                        ModelState.AddModelError("message", "修改失敗:" + ErrMsgs);
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("message", ex.Message);
                }
            }

            model.Mode = EditPageMode.Update;
            model.BookcaseSelectItem = bookSer.GetList(UserManager.User.Domain, model.BookcaseId);

            return View("Add", model);
        }
        
        [Authorize]
        public ActionResult Delete(Guid id, int page = 1)
        {
            try
            {
                string errorMsg = string.Empty;

                var delobj = Service.Get(id);

                string azurePaht = UserManager.User.Domain.ToLower();
                string FileName = FileName = delobj.ThumbnailUrl.Replace(WLBlob, "").Replace(azurePaht + "/", "");
                
                if (Service.Delete(id, out errorMsg))
                {
                    BlobHandler bh = new BlobHandler(azurePaht.ToLower());
                    bh.DeleteFile(FileName);

                    TempData["delete"] = true;
                }
                return RedirectToAction("List", new { page = page });
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("message", ex.Message);
            }
            return RedirectToAction("List");
        }

        #endregion

        #region DocType
        public ActionResult FolderDocTypeCom(FolderDocTypeModel criteria, Guid folderID)
        {
            DocumentTypeService docTypSer = new DocumentTypeService();
            try
            {                
                FolderDocTypeModel model = new FolderDocTypeModel();
                model.ID = folderID;
                model.DocTypeList = docTypSer.GetList(UserManager.User.Domain);
                ViewData["DocTypeList_Data"] = model.DocTypeList.Select(o => new SelectListItem { Text = o.Name, Value = o.Id.ToString() });

                FolderDocumentTypeService fdtSer = new FolderDocumentTypeService();
                model.FolderDocType = fdtSer.GetList(folderID);
                ViewData["FolderDocType_Data"] = model.FolderDocType.Select(o => new SelectListItem { Text = o.DocumentTypeName, Value = o.DocumentTypeId.ToString() });
                model.DocTypeIdList = string.Join(",", model.FolderDocType.Select(r => r.DocumentTypeId.ToString()));

                return View(model);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("message", ex.Message);
            }

            return View(criteria);
        }

        public ActionResult UpdateFolderDocType(FolderDocTypeModel model)
        {           
            string errMsgs = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(model.DocTypeIdList))
                {
                    var DocTypeIds = model.DocTypeIdList.TrimEnd(',').Split(',');

                    if (!fdtSer.DelAllByFolder(model.ID, out errMsgs))
                        throw new Exception(errMsgs);

                    foreach (var DocId in DocTypeIds)
                    {
                        if (!fdtSer.Create(new FolderDocumentTypeModel { FolderId = model.ID, DocumentTypeId = new Guid(DocId) }, out errMsgs))
                            throw new Exception(errMsgs);                        
                    }                    
                }
                else
                {
                    if (!fdtSer.DelAllByFolder(model.ID, out errMsgs))
                        throw new Exception(errMsgs);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("message", ex.Message);
            }

            
            model.DocTypeList = docTypSer.GetList(UserManager.User.Domain);
            ViewData["DocTypeList_Data"] = model.DocTypeList.Select(o => new SelectListItem { Text = o.Name, Value = o.Id.ToString() });
            
            model.FolderDocType = fdtSer.GetList(model.ID);
            ViewData["FolderDocType_Data"] = model.FolderDocType.Select(o => new SelectListItem { Text = o.DocumentTypeName, Value = o.DocumentTypeId.ToString() });

            return View("FolderDocTypeCom", model);
        }

        #endregion
    }
}
