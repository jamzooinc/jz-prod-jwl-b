﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

using JamOA.sdk;

using JamZoo.Project.WebSite.Models;
using JamZoo.Project.WebSite.Service;
using JamZoo.Project.WebSite.ViewModels;
using JamZoo.Project.WebSite.Enums;
using JamZoo.Project.WebSite.Library.Principal;

namespace JamZoo.Project.WebSite.Controllers
{
    public class AccountController : Controller
    {
        AccountService Service = new AccountService();

        public AccountController()
        {
            
        }

        

        #region CRUD
        //GET: /Account/List
        [Authorize]
        public ActionResult List(AccountListModel criteria)
        {
            try
            {
                AccountListModel model = Service.GetList(User.Identity.Name, criteria);

                return View(model);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("message", ex.Message);
            }

            return View(criteria);
        }

        [Authorize]
        public ActionResult Add(int page = 1)
        {
            AccountModel entity = Service.NewInstance();
            entity.RoleSelectList = Service.GetRoleList("");
            entity.page = page;
            entity.Mode = EditPageMode.Create;
            return View(entity);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Create(AccountModel model)
        {
            model.Id = model.Email;

            if (ModelState.IsValid)
            {
                try
                {
                    string ErrMsgs = string.Empty;

                    if (Service.Create(
                        User.Identity.Name, 
                        model, out ErrMsgs))
                    {
                        return RedirectToAction("List", new { page = model.page });
                    }

                    ModelState.AddModelError("message", ErrMsgs);

                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("message", ex.Message);
                }
            }

            model.Mode = EditPageMode.Create;
            return View("Add", model);
        }


        [Authorize]
        public ActionResult Edit(string id, int page = 1)
        {
            AccountModel model = Service.Get(User.Identity.Name, id);
            model.Mode = EditPageMode.Update;
            model.RoleSelectList = Service.GetRoleList(model.Role);
            model.page = page;
            return View("Add", model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Update(AccountModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    string ErrMsgs = string.Empty;

                    if (Service.Update(User.Identity.Name, model, out ErrMsgs))
                    {
                        return RedirectToAction("List", new { page = model.page });
                    }
                    else
                    {
                        ModelState.AddModelError("message", "修改失敗:" + ErrMsgs);
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("message", ex.Message);
                }
            }

            model.Mode = EditPageMode.Update;
            return View("Add", model);
        }

        [Authorize]
        public ActionResult Delete(string id, int page = 1)
        {
            try
            {
                string errorMsg = string.Empty;

                if (Service.Delete(User.Identity.Name, id, out errorMsg))
                {
                    TempData["delete"] = true;
                }
                return RedirectToAction("List", new { page = page });
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("message", ex.Message);
            }
            return RedirectToAction("List");
        }
        #endregion


        public ActionResult Info()
        {
            return View();
        }

        #region 認證模組

        // 登入頁
        // GET: /Account/Login
        public ActionResult Login()
        {
            LoginPage Model = new LoginPage();

            return View("Index", Model);
        }

        // 登入驗證
        // POST: /Account/Login
        [HttpPost]
        public ActionResult Login(LoginPage Model, string returnUrl)
        {
            string errorMsg = string.Empty;
            if (ModelState.IsValid)
            {
                AccountModel user = null;
                bool isSuccess = Service.Authentication(
                    Model.Account, 
                    Model.Password,
                    out user,
                    out errorMsg);

                var GroupObj = JamOAClient.MeOrgans(Model.Account);
                GroupObj = GroupObj.Where(p => p.ParentName.ToUpper() == "JAMZOO.SERVICE");                

                if (true == isSuccess && GroupObj != null)
                {                                        
                
                    //要存在 cookie 的 user data
                    WebSiteUser userData = new WebSiteUser()
                    {
                        Id = user.Id,
                        Account = user.Id,
                        Domain = GroupObj.FirstOrDefault().Id,
                        Roles = user.RoleList.Select(p => p.ToString()).ToArray()
                    };                                        
                    
                    BookcaseModel bookcaseMod = new BookcaseModel();                 
                    BookcaseService bookSer = new BookcaseService();
                    bookcaseMod = bookSer.GetDefault(userData.Domain, null);

                    bool isCookiePersistent = false;
                    System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                    bookcaseMod.Name = HttpUtility.UrlEncode(bookcaseMod.Name).Replace("+", "%20");
                    string jsonbookcaseModel = serializer.Serialize(bookcaseMod);

                    if (!this.ControllerContext.HttpContext.Request.Cookies.AllKeys.Contains("bookcaseMod"))                    
                    {
                        HttpCookie cookie = new HttpCookie("bookcaseMod", jsonbookcaseModel);
                        this.ControllerContext.HttpContext.Response.Cookies.Add(cookie);
                    }
                                        
                    string jsonAccountModel = serializer.Serialize(userData);

                    FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(1,
                              user.Id, DateTime.Now, DateTime.Now.AddMinutes(60), isCookiePersistent, jsonAccountModel);

                    string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
                    HttpCookie authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);

                    if (true == isCookiePersistent)
                        authCookie.Expires = authTicket.Expiration;
                    Response.Cookies.Add(authCookie);
                    //Response.Redirect(FormsAuthentication.GetRedirectUrl(Model.Account, false));
                    Response.Redirect("~/Document/List");
                }

            }

            ModelState.AddModelError("message", errorMsg);

            return View("index", Model);
        }


        // 登出
        // GET: /Account/Signout
        public ActionResult Signout()
        {
            FormsAuthentication.SignOut();
            return Redirect("~/");
        }

        #endregion
        

    }
}
