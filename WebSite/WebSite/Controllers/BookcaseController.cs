﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.IO;

using JamZoo.Project.WebSite.Library.Azure;

using JamZoo.Project.WebSite.Models;
using JamZoo.Project.WebSite.Service;
using JamZoo.Project.WebSite.ViewModels;
using JamZoo.Project.WebSite.Enums;
using JamZoo.Project.WebSite.Library.Principal;

namespace JamZoo.Project.WebSite.Controllers
{
    public class BookcaseController : BaseController
    {
        BookcaseService Service = new BookcaseService();
        CategoryService cateSer = new CategoryService();

        public BookcaseController()
        {
            
        }
        
        #region CRUD
        //GET: /Account/List
        [Authorize]
        public ActionResult List(BookcaseListModel criteria)
        {
            try
            {                
                BookcaseListModel model = Service.GetList(UserManager.User.Domain, criteria);

                return View(model);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("message", ex.Message);
            }

            return View(criteria);
        }

        

        [Authorize]
        public ActionResult Add(int page = 1)
        {
            BookcaseModel entity = Service.NewInstance();
            entity.CategorSelectList1 = cateSer.GetItemList(UserManager.User.Domain, "");
            entity.CategorSelectList2 = cateSer.GetItemList(UserManager.User.Domain, "");
            entity.CategorSelectList3 = cateSer.GetItemList(UserManager.User.Domain, "");
            entity.page = page;
            entity.Mode = EditPageMode.Create;
            return View(entity);
        }
        
        [Authorize]
        [HttpPost]
        public ActionResult Create(BookcaseModel model, HttpPostedFileBase file)
        {
            model.CreateAccountId = UserManager.User.Id;
            model.Domain = UserManager.User.Domain;    

            if (ModelState.IsValid)
            {
                try
                {
                    string ErrMsgs = string.Empty;

                    string azurePaht = UserManager.User.Domain.ToLower();
                    string FileName = string.Empty;

                    model.ThumbnailUrl = string.Empty;

                    #region 處理圖片
                    if (file != null)
                    {
                        FileInfo fi = new FileInfo(file.FileName);

                        FileName = string.Format("{0}{1}", Guid.NewGuid(), fi.Extension);

                        BlobHandler bh = new BlobHandler(azurePaht);
                        bh.Upload(file, FileName);

                        model.ThumbnailUrl = string.Format("{0}{1}/{2}", WLBlob, azurePaht, FileName);
                    }
                    #endregion
                    

                    if (Service.Create(model, out ErrMsgs))                        
                    {                                 
                        return RedirectToAction("List", new { page = model.page });
                    }
                    else
                    {
                        BlobHandler bh = new BlobHandler(azurePaht.ToLower());
                        bh.DeleteFile(FileName);
                    }

                    ModelState.AddModelError("message", ErrMsgs);

                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("message", ex.Message);
                }
            }

            model.CategorSelectList1 = cateSer.GetItemList(UserManager.User.Domain, model.CategoryId1.ToString());
            model.CategorSelectList2 = cateSer.GetItemList(UserManager.User.Domain, model.CategoryId2.ToString());
            model.CategorSelectList3 = cateSer.GetItemList(UserManager.User.Domain, model.CategoryId3.ToString());

            model.Mode = EditPageMode.Create;
            return View("Add", model);
        }
        
        [Authorize]
        public ActionResult Edit(Guid id, int page = 1)
        {
            BookcaseModel model = Service.Get(id);

            model.CategorSelectList1 = cateSer.GetItemList(UserManager.User.Domain, model.CategoryId1.ToString());
            model.CategorSelectList2 = cateSer.GetItemList(UserManager.User.Domain, model.CategoryId2.ToString());
            model.CategorSelectList3 = cateSer.GetItemList(UserManager.User.Domain, model.CategoryId3.ToString());

            model.Mode = EditPageMode.Update;            
            model.page = page;
            return View("Add", model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Update(BookcaseModel model, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    string ErrMsgs = string.Empty;

                    string azurePaht = UserManager.User.Domain.ToLower();
                    string FileName = string.Empty;

                    #region 處理圖片
                    if (file != null)
                    {
                        FileInfo fi = new FileInfo(file.FileName);

                        FileName = model.ThumbnailUrl == null ?
                            string.Format("{0}{1}", Guid.NewGuid(), fi.Extension) :
                            model.ThumbnailUrl.Replace(WLBlob, "").Replace(azurePaht + "/", "");                        

                        BlobHandler bh = new BlobHandler(azurePaht.ToLower());
                        bh.DeleteFile(FileName);
                                           
                        FileName = string.Format("{0}{1}", Guid.NewGuid(), fi.Extension);
                        
                        bh.Upload(file, FileName);

                        model.ThumbnailUrl = string.Format("{0}{1}/{2}", WLBlob, azurePaht, FileName);
                    }
                    #endregion

                    if (Service.Update(model, out ErrMsgs))
                    {
                        BookcaseModel bookcaseMod = new BookcaseModel();
                        System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();

                        if (this.ControllerContext.HttpContext.Request.Cookies.AllKeys.Contains("bookcaseMod"))
                        {                            
                            HttpCookie bookCookie = Request.Cookies.Get("bookcaseMod");
                            bookcaseMod = serializer.Deserialize<BookcaseModel>(bookCookie.Value.ToString());
                        }  

                        if(bookcaseMod.Id == model.Id)
                        {
                            bookcaseMod.Name = HttpUtility.UrlEncode(bookcaseMod.Name).Replace("+", "%20");
                            string jsonbookcaseModel = serializer.Serialize(bookcaseMod);                            
                            HttpCookie cookie = new HttpCookie("bookcaseMod", jsonbookcaseModel);
                            this.ControllerContext.HttpContext.Response.Cookies.Add(cookie);
                        }

                        return RedirectToAction("CtrlList", "Document", new { bookCaseId = model.Id });
                    }
                    else
                    {
                        ModelState.AddModelError("message", "修改失敗:" + ErrMsgs);
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("message", ex.Message);
                }
            }

            model.Mode = EditPageMode.Update;

            model.CategorSelectList1 = cateSer.GetItemList(UserManager.User.Domain, model.CategoryId1.ToString());
            model.CategorSelectList2 = cateSer.GetItemList(UserManager.User.Domain, model.CategoryId2.ToString());
            model.CategorSelectList3 = cateSer.GetItemList(UserManager.User.Domain, model.CategoryId3.ToString());

            return View("Add", model);
        }
        
        [Authorize]
        public ActionResult Delete(Guid id, int page = 1)
        {
            try
            {
                string errorMsg = string.Empty;

                var delobj = Service.Get(id);

                string azurePaht = UserManager.User.Domain.ToLower();
                string FileName = FileName = delobj.ThumbnailUrl.Replace(WLBlob, "").Replace(azurePaht + "/", "");

                if (Service.Delete(id, out errorMsg))
                {
                    BlobHandler bh = new BlobHandler(azurePaht.ToLower());
                    bh.DeleteFile(FileName);

                    TempData["delete"] = true;
                }
                return RedirectToAction("List", new { page = page });
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("message", ex.Message);
            }
            return RedirectToAction("List");
        }

        #endregion
        

    }
}
