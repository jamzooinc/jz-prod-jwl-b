﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.IO;

using JamZoo.Project.WebSite.Library.Azure;

using JamZoo.Project.WebSite.Models;
using JamZoo.Project.WebSite.Service;
using JamZoo.Project.WebSite.ViewModels;
using JamZoo.Project.WebSite.Enums;
using JamZoo.Project.WebSite.Library.Principal;
using JamZoo.Project.WebSite.Library.DocConvert;

namespace JamZoo.Project.WebSite.Controllers
{
    public class DocumentController : BaseController
    {
        PageImageService pageImgSer = new PageImageService();
        BookcaseService bookSer = new BookcaseService();
        DocumentTypeService docTypeSer = new DocumentTypeService();
        DocumentService Service = new DocumentService();
        CategoryItemService cateItemSer = new CategoryItemService();
        DocumentConvertService docConvSer = new DocumentConvertService();

        public DocumentController()
        {
            
        }
        
        #region CRUD       
        [Authorize]
        public ActionResult List(DocumentListModel criteria, Guid? foldId, string ErrMsg)
        {
            try
            {
                BookcaseModel bookcaseMod = new BookcaseModel();

                if (this.ControllerContext.HttpContext.Request.Cookies.AllKeys.Contains("bookcaseMod"))
                {
                    System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                    HttpCookie bookCookie = Request.Cookies.Get("bookcaseMod");
                    bookcaseMod = serializer.Deserialize<BookcaseModel>(bookCookie.Value.ToString());
                }

                DocumentListModel model = Service.GetList(UserManager.User.Domain, criteria, bookcaseMod.Id, foldId, false);

                if(!string.IsNullOrEmpty(ErrMsg))
                    ModelState.AddModelError("message", ErrMsg);

                return View(model);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("message", ex.Message);
            }

            return View(criteria);
        }

        public ActionResult CtrlList(Guid bookCaseId)
        {
            DocumentListModel criteria = new DocumentListModel();
            try
            {
                BookcaseModel bookcaseMod = new BookcaseModel();

                DocumentListModel model = Service.GetList(UserManager.User.Domain, criteria, bookCaseId, out bookcaseMod);

                System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                bookcaseMod.Name = HttpUtility.UrlEncode(bookcaseMod.Name).Replace("+", "%20");
                string jsonbookcaseModel = serializer.Serialize(bookcaseMod);

                this.ControllerContext.HttpContext.Response.Cookies.Remove("bookcaseMod");
                HttpCookie cookie = new HttpCookie("bookcaseMod", jsonbookcaseModel);
                this.ControllerContext.HttpContext.Response.Cookies.Add(cookie);

                return View("List", criteria);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("message", ex.Message);
            }

            return View("List", criteria);
        }

        public ActionResult ApproveList(DocumentListModel criteria)
        {
            try
            {
                DocumentListModel model = Service.GetList(UserManager.User.Domain, criteria, null, null, true);

                return View(model);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("message", ex.Message);
            }

            return View(criteria);
        }

        [Authorize]
        public ActionResult Add(int page = 1)
        {
            BookcaseModel bookcaseMod = new BookcaseModel();            

            if (this.ControllerContext.HttpContext.Request.Cookies.AllKeys.Contains("bookcaseMod"))
            {
                System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                HttpCookie bookCookie = Request.Cookies.Get("bookcaseMod");
                bookcaseMod = serializer.Deserialize<BookcaseModel>(bookCookie.Value.ToString());                
            }  

            DocumentModel entity = Service.NewInstance();
            entity.BookcaseId = bookcaseMod.Id;
            entity.DocumentTypeSelectList = docTypeSer.GetList(UserManager.User.Domain, new Guid());

            if (bookcaseMod.CategoryId1 != null)
                entity.CategorItemSelectList1 = cateItemSer.GetList(new Guid(bookcaseMod.CategoryId1.ToString()), "");

            if (bookcaseMod.CategoryId2 != null)
                entity.CategorItemSelectList2 = cateItemSer.GetList(new Guid(bookcaseMod.CategoryId2.ToString()), "");

            if (bookcaseMod.CategoryId3 != null)
                entity.CategorItemSelectList3 = cateItemSer.GetList(new Guid(bookcaseMod.CategoryId3.ToString()), "");
           
            entity.page = page;
            entity.Mode = EditPageMode.Create;
            return View(entity);
        }
        
        [Authorize]
        [HttpPost]
        public ActionResult Create(DocumentModel model, HttpPostedFileBase file, HttpPostedFileBase Coverfile, Guid? foldId)
        {
            BookcaseModel bookcaseMod = new BookcaseModel();
            DocumentTypeModel docTypeMod = new DocumentTypeModel();
            DocumentConvertModel docConvMod = new DocumentConvertModel();

            bookcaseMod = bookSer.Get(model.BookcaseId);


            model.CreateAccountId = UserManager.User.Id;
            model.Domain = UserManager.User.Domain;

            if (ModelState.IsValid)
            {
                try
                {
                    string ErrMsgs = string.Empty;

                    string webFilePath = System.Web.Hosting.HostingEnvironment.MapPath("~/DocConvertFolder");

                    BlobHandler bh = new BlobHandler(UserManager.User.Domain.ToLower());

                    FileInfo fi = new FileInfo(file.FileName);

                    string FileName = string.Format("{0}{1}", model.DocVerMod.Id.ToString(), fi.Extension);

                    if(bookcaseMod.IsVersionControlEnabled)
                    {
                        model.DocVerMod.Version = new decimal(0.1);
                        model.DocVerMod.State = DocVerState.CheckOut;
                        model.DocVerMod.DisplayVersion = "無";
                    }                        
                    else
                    {
                        model.DocVerMod.Version = 1;
                        model.DocVerMod.State = DocVerState.Release;
                        model.DocVerMod.DisplayVersion = "1";
                    }                        
                    
                    
                    model.DocVerMod.ModifyTime = DateTime.Now;
                    model.DocVerMod.ModifyAccountId = UserManager.User.Id;

                    #region 處理圖片
                    if (Coverfile != null)
                    {
                        FileInfo cfi = new FileInfo(Coverfile.FileName);

                        string cFileName = string.Format("{0}{1}", Guid.NewGuid(), cfi.Extension);

                        bh.Upload(Coverfile, cFileName);

                        model.DocVerMod.CoverThumbnailUrl = string.Format("{0}{1}/{2}", WLBlob, UserManager.User.Domain.ToLower(), cFileName);
                    }
                    #endregion

                    #region 處理文件

                    if (file != null)
                    {
                        docTypeMod = docTypeSer.Get(model.DocumentTypeId);

                        if (!docTypeMod.SubFileName.Replace(".", "").Trim().ToLower().Contains(fi.Extension.Replace(".", "")))
                            throw new Exception("檔案格式不符");

                        model.DocVerMod.FileUploadTime = DateTime.Now;

                        model.DocVerMod.FileName = fi.Name;

                        model.DocVerMod.FileSize = file.ContentLength;

                        string SavePath = string.Format("{0}/{1}", webFilePath, FileName);
                        string FileTempFolder = string.Format("{0}/{1}", webFilePath, model.DocVerMod.Id);
                        file.SaveAs(SavePath);

                        bh.Upload(file, FileName);

                        model.DocVerMod.FileUrl = string.Format("{0}{1}/{2}", WLBlob, UserManager.User.Domain.ToLower(), FileName);

                        
                        #region 寫入排程轉檔
                        if (docTypeMod.SubFileName.Replace(".", "").Trim().ToLower() != "mp4" && docTypeMod.SubFileName.Replace(".", "").Trim().ToLower() != "jpg")
                        {
                            docConvMod.Id = Guid.NewGuid();
                            docConvMod.VersionId = model.DocVerMod.Id;
                            docConvMod.ConvertStatus = 0;
                            docConvMod.CreateTime = DateTime.Now;
                            docConvMod.ModifyTime = DateTime.Now;
                            docConvSer.Create(docConvMod, out ErrMsgs);
                            if (!string.IsNullOrEmpty(ErrMsgs))
                                throw new Exception(ErrMsgs);
                        }
                        #endregion

                        fi = new FileInfo(SavePath);
                        fi.Delete();


                    }
                    else
                        throw new Exception("請選擇上傳文件");

                    #endregion


                    if (Service.Create(model, foldId, out ErrMsgs))
                    {
                        return RedirectToAction("List", new { page = model.page, foldId = foldId });
                    }

                    ModelState.AddModelError("message", ErrMsgs);

                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("message", ex.Message);
                }
            }
            model.DocVerMod.State = DocVerState.CheckOut;

            model.DocumentTypeSelectList = docTypeSer.GetList(UserManager.User.Domain, new Guid());
            if (bookcaseMod.CategoryId1 != null)
                model.CategorItemSelectList1 = cateItemSer.GetList(new Guid(bookcaseMod.CategoryId1.ToString()), "");

            if (bookcaseMod.CategoryId2 != null)
                model.CategorItemSelectList2 = cateItemSer.GetList(new Guid(bookcaseMod.CategoryId2.ToString()), "");

            if (bookcaseMod.CategoryId3 != null)
                model.CategorItemSelectList3 = cateItemSer.GetList(new Guid(bookcaseMod.CategoryId3.ToString()), "");
            model.Mode = EditPageMode.Create;
            return View("Add", model);
        }

        [Authorize]
        public ActionResult Edit(Guid id, HttpPostedFileBase file, int page = 1)
        {
            BookcaseModel bookcaseMod = new BookcaseModel();

            if (this.ControllerContext.HttpContext.Request.Cookies.AllKeys.Contains("bookcaseMod"))
            {
                System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                HttpCookie bookCookie = Request.Cookies.Get("bookcaseMod");
                bookcaseMod = serializer.Deserialize<BookcaseModel>(bookCookie.Value.ToString());
            }  

            DocumentModel model = Service.Get(id);
            model.DocumentTypeSelectList = docTypeSer.GetList(UserManager.User.Domain, model.Id);

            if (bookcaseMod.CategoryId1 != null)
                model.CategorItemSelectList1 = cateItemSer.GetList(new Guid(bookcaseMod.CategoryId1.ToString()), model.CategoryItemId1.ToString());

            if (bookcaseMod.CategoryId2 != null)
                model.CategorItemSelectList2 = cateItemSer.GetList(new Guid(bookcaseMod.CategoryId2.ToString()), model.CategoryItemId2.ToString());

            if (bookcaseMod.CategoryId3 != null)
                model.CategorItemSelectList3 = cateItemSer.GetList(new Guid(bookcaseMod.CategoryId3.ToString()), model.CategoryItemId3.ToString());

            model.Mode = EditPageMode.Update;
            model.page = page;
            return View("Add", model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Update(DocumentModel model, HttpPostedFileBase file, HttpPostedFileBase Coverfile, Guid? foldId)
        {
            BookcaseModel bookcaseMod = new BookcaseModel();
            DocumentTypeModel docTypeMod = new DocumentTypeModel();
            DocumentConvertModel docConvMod = new DocumentConvertModel();

            bookcaseMod = bookSer.Get(model.BookcaseId);

            if (ModelState.IsValid)
            {
                try
                {

                    BlobHandler bh = new BlobHandler(UserManager.User.Domain.ToLower());

                    string ErrMsgs = string.Empty;

                    string webFilePath = System.Web.Hosting.HostingEnvironment.MapPath("~/DocConvertFolder");                    
                                                            
                    model.DocVerMod.ModifyTime = DateTime.Now;
                    model.DocVerMod.ModifyAccountId = UserManager.User.Id;

                    #region 處理圖片
                    if (Coverfile != null)
                    {
                        
                        FileInfo cfi = new FileInfo(Coverfile.FileName);

                        string cFileName = string.Format("{0}{1}", Guid.NewGuid(), cfi.Extension);

                        if(model.DocVerMod.CoverThumbnailUrl != null)                        
                            cFileName = model.DocVerMod.CoverThumbnailUrl.Replace(WLBlob, "").Replace(UserManager.User.Domain.ToLower(), "").Replace("/", "");                                                                                

                        bh.Upload(Coverfile, cFileName);

                        model.DocVerMod.CoverThumbnailUrl = string.Format("{0}{1}/{2}", WLBlob, UserManager.User.Domain.ToLower(), cFileName);
                    }
                    #endregion

                    #region 處理文件

                    if (file != null)
                    {
                        if (!bookcaseMod.IsVersionControlEnabled)
                        {
                            model.DocVerMod.Version += 1;
                            model.DocVerMod.State = DocVerState.Release;
                        }                        

                        FileInfo fi = new FileInfo(file.FileName);

                        string FileName = model.DocVerMod.FileUrl == null ?
                            string.Format("{0}{1}", model.DocVerMod.Id.ToString(), fi.Extension) :
                            string.Format("{0}", model.DocVerMod.FileUrl.Replace(WLBlob, "").Replace(UserManager.User.Domain.ToLower(), "").Replace("/", ""));

                        docTypeMod = docTypeSer.Get(model.DocumentTypeId);

                        if (docTypeMod.SubFileName.Replace(".", "").Trim().ToLower() != fi.Extension.Replace(".", ""))
                            throw new Exception("檔案格式不符");

                        model.DocVerMod.FileUploadTime = DateTime.Now;

                        model.DocVerMod.FileName = fi.Name;

                        model.DocVerMod.FileSize = file.ContentLength;

                        string SavePath = string.Format("{0}/{1}", webFilePath, FileName);
                        string FileTempFolder = string.Format("{0}/{1}", webFilePath, model.DocVerMod.Id);
                        file.SaveAs(SavePath);


                        bh.DeleteFile(FileName);

                        bh.Upload(file, FileName);

                        model.DocVerMod.FileUrl = string.Format("{0}{1}/{2}", WLBlob, UserManager.User.Domain.ToLower(), FileName);

                        #region 寫入排程轉檔
                        if (docTypeMod.SubFileName.Replace(".", "").Trim().ToLower() != "mp4" && docTypeMod.SubFileName.Replace(".", "").Trim().ToLower() != "jpg")
                        {
                            docConvMod.Id = Guid.NewGuid();
                            docConvMod.VersionId = model.DocVerMod.Id;
                            docConvMod.ConvertStatus = 0;
                            docConvMod.CreateTime = DateTime.Now;
                            docConvMod.ModifyTime = DateTime.Now;
                            docConvSer.Create(docConvMod, out ErrMsgs);
                            if (!string.IsNullOrEmpty(ErrMsgs))
                                throw new Exception(ErrMsgs);
                        }
                        #endregion

                        fi = new FileInfo(SavePath);
                        fi.Delete();
                    }

                    #endregion


                    if (Service.Update(model, out ErrMsgs))
                    {
                        return RedirectToAction("List", new { page = model.page, foldId = foldId });
                    }

                    ModelState.AddModelError("message", ErrMsgs);

                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("message", ex.Message);
                }
            }

            model.Mode = EditPageMode.Update;

            model.DocumentTypeSelectList = docTypeSer.GetList(UserManager.User.Domain, new Guid());

            if (bookcaseMod.CategoryId1 != null)
                model.CategorItemSelectList1 = cateItemSer.GetList(new Guid(bookcaseMod.CategoryId1.ToString()), model.CategoryItemId1.ToString());

            if (bookcaseMod.CategoryId2 != null)
                model.CategorItemSelectList2 = cateItemSer.GetList(new Guid(bookcaseMod.CategoryId2.ToString()), model.CategoryItemId2.ToString());

            if (bookcaseMod.CategoryId3 != null)
                model.CategorItemSelectList3 = cateItemSer.GetList(new Guid(bookcaseMod.CategoryId3.ToString()), model.CategoryItemId3.ToString());

            return View("Add", model);
        }

        [Authorize]
        public ActionResult CheckOut(Guid id, int page = 1)
        {
            try
            {
                string errorMsg = string.Empty;

                if (!Service.ChangeStatus(id, DocVerState.CheckOut, "", out errorMsg))
                    throw new Exception(errorMsg);

                return RedirectToAction("List", new { page = page });
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("message", ex.Message);
            }
            return RedirectToAction("List");
        }

        [Authorize]
        public ActionResult CheckIn(Guid id, int page = 1)
        {
            string errorMsg = string.Empty;

            try
            {                
                if (!Service.ChangeStatus(id, DocVerState.CheckIn, "", out errorMsg))
                    throw new Exception(errorMsg);

                return RedirectToAction("List", new { page = page });
            }
            catch (Exception ex)
            {
                errorMsg = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                ModelState.AddModelError("message", ex.Message);
            }
            return RedirectToAction("List", new { page = page, ErrMsg = errorMsg });
        }

        [Authorize]
        public ActionResult Approve(Guid id, int page = 1)
        {                     
            try
            {
                string errorMsg = string.Empty;

                if (!Service.ChangeStatus(id, DocVerState.Release, UserManager.User.Id, out errorMsg))
                    throw new Exception(errorMsg);

                return RedirectToAction("ApproveList", new { page = page });
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("message", ex.Message);
            }
            return RedirectToAction("ApproveList");
        }

        [Authorize]
        public ActionResult ReverToVersion(Guid TagVerId, int page = 1)
        {
            try
            {
                string errorMsg = string.Empty;

                if (!Service.ReverToVersion(TagVerId, UserManager.User.Id, out errorMsg))
                    throw new Exception(errorMsg);

                return RedirectToAction("ApproveList", new { page = page });
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("message", ex.Message);
            }
            return RedirectToAction("ApproveList");
        }

        [Authorize]
        public ActionResult Delete(Guid id, int page = 1)
        {
            try
            {
                BlobHandler bh = new BlobHandler(UserManager.User.Domain.ToLower());

                string errorMsg = string.Empty;

                var delobj = Service.Get(id);

                string azurePaht = UserManager.User.Domain.ToLower();

                List<string> bolbFileList = new List<string>();

                if (Service.Delete(id, WLBlob, UserManager.User.Domain, out bolbFileList, out errorMsg))
                {
                    foreach (string str in bolbFileList)
                        bh.DeleteFile(str);

                    TempData["delete"] = true;
                }
                return RedirectToAction("List", new { page = page });
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("message", ex.Message);
            }
            return RedirectToAction("List");
        }

        #endregion

        #region Folder
        public ActionResult FolderDocumentCom(FolderDocModel criteria, Guid docId, Guid bookcaseId)
        {            
            FolderService fodSer = new FolderService();
            FolderDocumentService fdSer = new FolderDocumentService();
            try
            {
                criteria.bookcaseId = bookcaseId;
                FolderDocModel model = new FolderDocModel();
                model.ID = docId;
                model.FolderList = fodSer.GetList(UserManager.User.Domain, bookcaseId);
                ViewData["FolderList_Data"] = model.FolderList.Select(o => new SelectListItem { Text = o.Name, Value = o.Id.ToString() });


                model.FolderDoc = fdSer.GetList(docId);
                ViewData["FolderDoc_Data"] = model.FolderDoc.Select(o => new SelectListItem { Text = o.FolderName, Value = o.FolderId.ToString() });
                model.FolderIds = string.Join(",", model.FolderDoc.Select(r => r.FolderId.ToString()));

                return View(model);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("message", ex.Message);
            }

            return View(criteria);
        }

        public ActionResult UpdateFolderDoc(FolderDocModel model)
        {
            FolderService fodSer = new FolderService();
            FolderDocumentService fdSer = new FolderDocumentService();

            string errMsgs = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(model.FolderIds))
                {
                    var FolderIds = model.FolderIds.TrimEnd(',').Split(',');

                    if (!fdSer.DelAllByDocument(model.ID, out errMsgs))
                        throw new Exception(errMsgs);

                    foreach (var folderId in FolderIds)
                    {
                        if (!fdSer.Create(new FolderDocumentModel { DocumentId = model.ID, FolderId = new Guid(folderId) }, out errMsgs))
                            throw new Exception(errMsgs);
                    }
                }
                else
                {
                    if (!fdSer.DelAllByDocument(model.ID, out errMsgs))
                        throw new Exception(errMsgs);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("message", ex.Message);
            }

            model.FolderList = fodSer.GetList(UserManager.User.Domain, model.bookcaseId);
            ViewData["FolderList_Data"] = model.FolderList.Select(o => new SelectListItem { Text = o.Name, Value = o.Id.ToString() });

            model.FolderDoc = fdSer.GetList(model.ID);
            ViewData["FolderDoc_Data"] = model.FolderDoc.Select(o => new SelectListItem { Text = o.FolderName, Value = o.FolderId.ToString() });
            model.FolderIds = string.Join(",", model.FolderDoc.Select(r => r.FolderId.ToString()));

            return View("FolderDocumentCom", model);
        }

        #endregion
    }
}
