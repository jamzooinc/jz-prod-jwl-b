﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;


using JamZoo.Project.WebSite.Models;
using JamZoo.Project.WebSite.Service;
using JamZoo.Project.WebSite.ViewModels;
using JamZoo.Project.WebSite.Enums;
using JamZoo.Project.WebSite.Library.Principal;

namespace JamZoo.Project.WebSite.Controllers
{
    public class CategoryController : Controller
    {
        CategoryService Service = new CategoryService();

        public CategoryController()
        {
            
        }

        

        #region CRUD
        //GET: /Account/List
        [Authorize]
        public ActionResult List(CategoryListModel criteria)
        {
            try
            {                
                CategoryListModel model = Service.GetList(UserManager.User.Domain, criteria);

                return View(model);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("message", ex.Message);
            }

            return View(criteria);
        }

        
        [Authorize]
        public ActionResult Add(int page = 1)
        {
            CategoryModel entity = Service.NewInstance();            
            entity.page = page;
            entity.Mode = EditPageMode.Create;
            return View(entity);
        }

        
        [Authorize]
        [HttpPost]
        public ActionResult Create(CategoryModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    string ErrMsgs = string.Empty;

                    model.CreateAccountId = UserManager.User.Id;
                    model.Domain = UserManager.User.Domain;

                    if (Service.Create(model, out ErrMsgs))                        
                    {
                        return RedirectToAction("List", new { page = model.page });
                    }

                    ModelState.AddModelError("message", ErrMsgs);

                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("message", ex.Message);
                }
            }

            model.Mode = EditPageMode.Create;
            return View("Add", model);
        }

        
        [Authorize]
        public ActionResult Edit(Guid id, int page = 1)
        {
            CategoryModel model = Service.Get(id);
            model.Mode = EditPageMode.Update;            
            model.page = page;
            return View("Add", model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Update(CategoryModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    string ErrMsgs = string.Empty;

                    if (Service.Update(model, out ErrMsgs))
                    {
                        return RedirectToAction("List", new { page = model.page });
                    }
                    else
                    {
                        ModelState.AddModelError("message", "修改失敗:" + ErrMsgs);
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("message", ex.Message);
                }
            }

            model.Mode = EditPageMode.Update;
            return View("Add", model);
        }
        
        [Authorize]
        public ActionResult Delete(Guid id, int page = 1)
        {
            try
            {
                string errorMsg = string.Empty;

                if (Service.Delete(id, out errorMsg))
                {
                    TempData["delete"] = true;
                }
                return RedirectToAction("List", new { page = page });
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("message", ex.Message);
            }
            return RedirectToAction("List");
        }

        #endregion
        

    }
}
