﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;


using JamZoo.Project.WebSite.Models;
using JamZoo.Project.WebSite.Service;
using JamZoo.Project.WebSite.ViewModels;
using JamZoo.Project.WebSite.Enums;
using JamZoo.Project.WebSite.Library.Principal;

namespace JamZoo.Project.WebSite.Controllers
{
    public class CategoryItemController : Controller
    {
        CategoryItemService Service = new CategoryItemService();

        public CategoryItemController()
        {
            
        }

        

        #region CRUD
        //GET: /Account/List
        [Authorize]
        public ActionResult List(Guid CategoryId, CategoryItemListModel criteria)
        {
            try
            {
                CategoryItemListModel model = Service.GetList(UserManager.User.Domain, CategoryId, criteria);

                model.CategoryId = CategoryId;

                return View(model);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("message", ex.Message);
            }

            return View(criteria);
        }

        
        [Authorize]
        public ActionResult Add(Guid CategoryId, int page = 1)
        {
            CategoryItemModel entity = Service.NewInstance();
            entity.CategoryId = CategoryId;
            entity.page = page;
            entity.Mode = EditPageMode.Create;
            return View(entity);
        }

        
        [Authorize]
        [HttpPost]
        public ActionResult Create(CategoryItemModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    string ErrMsgs = string.Empty;

                    model.CreateAccountId = UserManager.User.Id;
                    model.Domain = UserManager.User.Domain;

                    if (Service.Create(model, out ErrMsgs))                        
                    {
                        return RedirectToAction("List", new { page = model.page, CategoryId = model.CategoryId });
                    }

                    ModelState.AddModelError("message", ErrMsgs);

                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("message", ex.Message);
                }
            }

            model.Mode = EditPageMode.Create;
            return View("Add", model);
        }

        
        [Authorize]
        public ActionResult Edit(Guid id, int page = 1)
        {
            CategoryItemModel model = Service.Get(id);
            model.Mode = EditPageMode.Update;            
            model.page = page;
            return View("Add", model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Update(CategoryItemModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    string ErrMsgs = string.Empty;

                    if (Service.Update(model, out ErrMsgs))
                    {
                        return RedirectToAction("List", new { page = model.page, CategoryId = model.CategoryId });
                    }
                    else
                    {
                        ModelState.AddModelError("message", "修改失敗:" + ErrMsgs);
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("message", ex.Message);
                }
            }

            model.Mode = EditPageMode.Update;
            return View("Add", model);
        }
        
        [Authorize]
        public ActionResult Delete(Guid id, Guid categoryId, int page = 1)
        {
            try
            {
                string errorMsg = string.Empty;

                if (Service.Delete(id, out errorMsg))
                {
                    TempData["delete"] = true;
                }
                return RedirectToAction("List", new { page = page, CategoryId = categoryId });
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("message", ex.Message);
            }
            return RedirectToAction("List");
        }

        #endregion
        

    }
}
