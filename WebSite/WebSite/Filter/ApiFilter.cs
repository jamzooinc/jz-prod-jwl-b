﻿
using System.Web.Mvc;
using JamZoo.Project.WebSite.Service;

namespace JamZoo.Project.WebSite.Filter
{
    public class ApiFilter : ActionFilterAttribute
    {
        /// <summary>
        /// 此兩FUNCTION不使用(註冊or修改:RegisterOrMaintain/登入:Login)
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {            

            base.OnActionExecuting(filterContext);

            if (string.IsNullOrEmpty(filterContext.HttpContext.Request["Domain"]))
            {
                //沒有這個 Domain 要回應
                filterContext.Result = new JsonResult()
                {
                    Data = new { Code = 408, Msg = "用戶端參數不足[Domain]" },
                    ContentEncoding = System.Text.Encoding.UTF8,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            else
            {
                if (string.IsNullOrEmpty(filterContext.HttpContext.Request["AuthKey"]))
                {
                    //沒有這個 AuthKey 要回應
                    filterContext.Result = new JsonResult()
                    {
                        Data = new { Code = 408, Msg = "用戶端參數不足[AuthKey]" },
                        ContentEncoding = System.Text.Encoding.UTF8,
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                }
                else
                {


                    if (string.IsNullOrEmpty(filterContext.HttpContext.Request["xxxxx"]))
                    {

                    }
                    else
                    {

                    }
                }
            }
        }
    }
}