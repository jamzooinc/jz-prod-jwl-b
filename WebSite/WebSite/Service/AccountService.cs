﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Data;

using System.Web.Mvc;
using JamOA.sdk;

using JamZoo.Project.WebSite.Enums;
using JamZoo.Project.WebSite.Models;
using JamZoo.Project.WebSite.DbContext;
using JamZoo.Project.WebSite.Library;
using System.Data.Entity;


namespace JamZoo.Project.WebSite.Service
{
    public class AccountService : BaseService
    {
        /// <summary>
        /// 取得帳號列表
        /// </summary>
        /// <param name="CurrentUserName">目前使用者ID</param>
        /// <param name="Page">分頁</param>
        /// <returns></returns>
        public AccountListModel GetList(string CurrentUserName, AccountListModel Page)
        {
            try
            {
                var GroupObj = JamOAClient.MeOrgans(CurrentUserName);


                foreach (var UserItem in JamOAClient.OrganAccounts(GroupObj.FirstOrDefault().Id))
                {
                    AccountModel User = new AccountModel();

                    var UsrObj = JamOAClient.MeInfo(UserItem.Id);

                    if (UsrObj == null)
                        continue;

                    User.Id = UserItem.Id;
                    User.DisplayName = UserItem.DisplayName;
                    User.OfficalName = UserItem.OfficalName;
                    User.Email = UserItem.Email;
                    User.Status = UserItem.IsEnable;

                    var roleLtObj = JamOAClient.MeRoles(User.Id);

                    if (roleLtObj != null)
                    {
                        if (roleLtObj.Count() > 0)
                            User.Role = roleLtObj.FirstOrDefault().Id;

                        foreach (var obj in roleLtObj)
                            User.RoleList.Add(obj.Id);
                    }

                    Page.Data.Add(User);
                }

                if (!string.IsNullOrEmpty(Page.Search))
                {
                    Page.Data = Page.Data.Where(p => p.DisplayName.Contains(Page.Search)).ToList();
                }

                var query = Page.Data.OrderBy(p => p.DisplayName);


                Page.TotalRecords = Page.Data.Count();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Page;
        }

        public bool Authentication(string Account, string Password, out AccountModel User, out string errorMsg)
        {
            User = new AccountModel();

            errorMsg = string.Empty;
            string accountId = string.Empty;

            try
            {
                if (!JamOAClient.UserAuth("jamzoo.service", Account, Password))
                    throw new Exception(string.Format("{0}[{1}", "驗證失敗", JamOAClient.Message));

                var UsrObj = JamOAClient.MeInfo(Account);

                User.Id = UsrObj.Id;                
                User.DisplayName = UsrObj.DisplayName;
                User.OfficalName = UsrObj.OfficalName;
                User.Email = UsrObj.Email;

                var roleLtObj = JamOAClient.MeRoles(User.Id);

                if (roleLtObj != null)
                {
                    foreach (var obj in roleLtObj)
                        User.RoleList.Add(obj.Id);
                }

                return true;
            }
            catch (Exception ex)
            {
                errorMsg = ex.InnerException == null ? ex.Message : ex.InnerException.ToString();
            }

            return false;
        }       

        /// <summary>
        /// 建立一筆全新的實體，給予初始值
        /// </summary>
        /// <returns></returns>
        public AccountModel NewInstance()
        {
            AccountModel newOne = new AccountModel();            
            newOne.CreateDate = DateTime.Now;
            return newOne;
        }

        public List<SelectListItem> GetRoleList(string RoleId)
        {
            List<SelectListItem> ItemList = new List<SelectListItem>();

            var GroupObj = JamOAClient.MeOrgans(RoleId);

            IEnumerable<JamOA.sdk.Model.RoleModel> o_query = JamOAClient.ProjectRoles();

            foreach (var p in o_query)
            {
                ItemList.Add(new SelectListItem()
                {
                    Text = p.DisplayName,
                    Value = p.Id,
                    Selected = p.Id.Equals(RoleId)
                });
            }

            return ItemList.ToList();
        }

        public bool Create(string UserName, AccountModel model, out string ErrMsgs)
        {
            ErrMsgs = string.Empty;            

            try
            {
                var GroupObj = JamOAClient.MeOrgans(UserName);                

                if (JamOAClient.CreateAccount(model.Id, model.Password, model.Email, model.DisplayName, model.OfficalName, model.Title, 0, ""))
                {
                    if (!AddUserToRoles(model.Id, model.Role, out ErrMsgs))
                        throw new Exception(ErrMsgs);

                    if (!JamOAClient.OrganAddAccount(GroupObj.FirstOrDefault().Id, model.Id))
                    {
                        JamOAClient.DeleteAccount(model.Id);
                    }
                }
                else
                    throw new Exception(JamOAClient.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(string.IsNullOrEmpty(JamOAClient.Message) ? JamOAClient.Message : (ex.InnerException == null ? ex.Message : ex.InnerException.ToString()));
            }

            return ErrMsgs.Length == 0;
        }        

        public AccountModel Get(string userId, string id)
        {
            var o_entity = JamOAClient.MeInfo(id);

            if (o_entity != null)
            {
                var model = new AccountModel()
                    {
                        Id = o_entity.Id,                        
                        CreateDate = o_entity.CreateDateTime,                        
                        Status = o_entity.IsEnable,                        
                        DisplayName = o_entity.DisplayName,
                        OfficalName = o_entity.OfficalName,
                        Email = o_entity.Email,
                        Title = o_entity.Title,

                    };

                model.RoleList = GetRolesByAccountId(model.Id);
                if (model.RoleList.Count > 0)
                    model.Role = model.RoleList[0];
                return model;
            }
            return null;
        }

        public bool Update(string userName, AccountModel model, out string ErrMsgs)
        {
            ErrMsgs = string.Empty;

            try
            {                
                try
                {
                    if (!JamOAClient.UpdateAccount(model.Email, model.Password, model.Email, model.DisplayName, model.OfficalName, model.Title, 0, "", model.Status))
                        throw new Exception(JamOAClient.Message);
                }
                catch (Exception ex)
                {
                    ErrMsgs = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
                }
                

                ClearRoles(model.Id);

                //加入角色
                if (!AddUserToRoles(model.Id, model.Role, out ErrMsgs))
                    throw new Exception(ErrMsgs);
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    ErrMsgs = ex.InnerException.Message;
                }
                else
                {
                    ErrMsgs = ex.Message;
                }
            }

            return ErrMsgs.Length == 0;            
        }

        public bool Delete(string userName, string id, out string ErrorMsg)
        {
            ErrorMsg = string.Empty;

            //var o_delete = from p in basedb.T_Account
            //               where p.Id == id
            //               select p;

            //foreach (var row in o_delete)
            //{
            //    basedb.T_Account.Remove(row);
            //}

            //try
            //{
            //    basedb.SaveChanges();
            //}
            //catch (Exception ex)
            //{
            //    ErrorMsg = ex.Message;
            //}

            return ErrorMsg.Length == 0;
        }

        #region 其他函式

        /// <summary>
        /// 清除所有角色
        /// </summary>
        /// <param name="AccountId"></param>
        public void ClearRoles(string AccountId)
        {
            var RoleObjs = JamOA.sdk.JamOAClient.MeRoles(AccountId);
            foreach(var RoleObj in RoleObjs)
            {
                JamOA.sdk.JamOAClient.RoleRemoveAccount(RoleObj.Id, AccountId);
            }
        }

        public bool AddUserToRoles(string AccountId, string RoleId, out string ErrMsg)
        {
            ErrMsg = string.Empty;

            if (!JamOAClient.RoleAddAccount(RoleId, AccountId))
            {
                ErrMsg = JamOAClient.Message;
                return false;
            }

            return true;
        }


        /// <summary>
        /// 依 Account Id 取得角色
        /// </summary>
        /// <param name="AccountId"></param>
        /// <returns></returns>
        public List<String> GetRolesByAccountId(string AccountId)
        {
            var roleLtObj = JamOAClient.MeRoles(AccountId);

            return (roleLtObj == null) ? null : roleLtObj.Select(p => p.Id.ToString()).ToList();                  

        }
        #endregion
    }
}