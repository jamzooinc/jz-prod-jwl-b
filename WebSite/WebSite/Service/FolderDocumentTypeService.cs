﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using JamZoo.Project.WebSite.Models;
using JamZoo.Project.WebSite.DbContext;

namespace JamZoo.Project.WebSite.Service
{
    public class FolderDocumentTypeService : BaseService
    {
        public List<FolderDocumentTypeModel> GetList(Guid folderId)
        {
            List<FolderDocumentTypeModel> rztLt = new List<FolderDocumentTypeModel>();

            var o_query = from p in basedb.JWL_FolderDocumentType
                          join t in basedb.JWL_DocumentType on p.DocumentTypeId equals t.Id
                          where p.FolderId == folderId
                          select new { p.FolderId, p.DocumentTypeId, t.Name };           

            o_query = o_query.OrderBy(p => p.Name);

            try
            {
                rztLt = o_query.Select(o_entity =>
                    new FolderDocumentTypeModel()
                    {
                        FolderId = o_entity.FolderId,
                        DocumentTypeId = o_entity.DocumentTypeId,
                        DocumentTypeName = o_entity.Name              
                    }
                    ).ToList();                

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return rztLt;
        }

        public bool Create(FolderDocumentTypeModel model, out string ErrMsgs)
        {

            ErrMsgs = string.Empty;


            JWL_FolderDocumentType dbEntity = new JWL_FolderDocumentType();
            dbEntity.FolderId = model.FolderId;
            dbEntity.DocumentTypeId = model.DocumentTypeId;
            basedb.JWL_FolderDocumentType.Add(dbEntity);

            try
            {
                basedb.SaveChanges();
            }
            catch (Exception ex)
            {
                ErrMsgs = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
            }

            return ErrMsgs.Length == 0;
        }

        public bool DelAllByFolder(Guid folderId, out string ErrMsgs)
        {

            ErrMsgs = string.Empty;

            var o_delete = from p in basedb.JWL_FolderDocumentType
                           where p.FolderId == folderId
                           select p;



            foreach (var row in o_delete)
            {
                basedb.JWL_FolderDocumentType.Remove(row);
            }

            try
            {
                basedb.SaveChanges();
            }
            catch (Exception ex)
            {
                ErrMsgs = ex.Message;
            }

            return ErrMsgs.Length == 0;
        }
    }
}