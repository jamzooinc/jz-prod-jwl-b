﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using JamZoo.Project.WebSite.Models;
using JamZoo.Project.WebSite.DbContext;

namespace JamZoo.Project.WebSite.Service
{
    public class BookcaseService : BaseService
    {
        public BookcaseModel NewInstance()
        {
            BookcaseModel newOne = new BookcaseModel();            
            return newOne;
        }

        public BookcaseModel GetDefault(string domain, BookcaseModel bookcaseMod)
        {
            string strRzt = string.Empty;

            var bk_query = from p in basedb.JWL_Bookcase
                           where p.Domain == domain
                           select p;

            bk_query = bk_query.OrderBy(p => p.Name);

            if (bookcaseMod != null)
            {
                foreach (var item in bk_query)
                {
                    if (item.Id == bookcaseMod.Id)
                    {
                        bookcaseMod.Id = bookcaseMod.Id;
                        bookcaseMod.Name = item.Name;
                        bookcaseMod.CategoryId1 = item.CategoryId1;
                        bookcaseMod.CategoryId2 = item.CategoryId2;
                        bookcaseMod.CategoryId3 = item.CategoryId3;
                        bookcaseMod.ThumbnailUrl = item.ThumbnailUrl;
                        bookcaseMod.NoInherit = item.NoInherit;

                        //目前是第一個
                        if (bk_query.ToList().IndexOf(item) == 0)
                        {
                            if (bk_query.Count() > 2)
                            {
                                bookcaseMod.NextId = bk_query.ToList()[1].Id;
                                break;
                            }

                        }

                        //目前是最後一個
                        if (bk_query.ToList().IndexOf(item) == bk_query.Count() - 1)
                        {
                            bookcaseMod.NextId = bk_query.ToList()[bk_query.Count() - 2].Id;
                            break;
                        }

                        bookcaseMod.PreId = bk_query.ToList()[bk_query.ToList().IndexOf(item) - 1].Id;
                        bookcaseMod.NextId = bk_query.ToList()[bk_query.ToList().IndexOf(item) + 1].Id;
                        break;
                    }
                }
            }
            else
            {
                foreach (var item in bk_query)
                {
                    if (item.Id == bk_query.ToList()[0].Id)
                    {
                        bookcaseMod = new BookcaseModel();
                        bookcaseMod.Id = bk_query.ToList()[0].Id;
                        bookcaseMod.Name = item.Name;
                        bookcaseMod.CategoryId1 = item.CategoryId1;
                        bookcaseMod.CategoryId2 = item.CategoryId2;
                        bookcaseMod.CategoryId3 = item.CategoryId3;
                        bookcaseMod.ThumbnailUrl = item.ThumbnailUrl;
                        bookcaseMod.NoInherit = item.NoInherit;

                        //目前是第一個
                        if (bk_query.ToList().IndexOf(item) == 0)
                        {
                            if (bk_query.Count() > 2)
                            {
                                bookcaseMod.NextId = bk_query.ToList()[1].Id;
                                break;
                            }

                        }

                        //目前是最後一個
                        if (bk_query.ToList().IndexOf(item) == bk_query.Count() - 1)
                        {
                            bookcaseMod.NextId = bk_query.ToList()[bk_query.Count() - 2].Id;
                            break;
                        }

                        bookcaseMod.PreId = bk_query.ToList()[bk_query.ToList().IndexOf(item) - 1].Id;
                        bookcaseMod.NextId = bk_query.ToList()[bk_query.ToList().IndexOf(item) + 1].Id;
                        break;
                    }
                }
            }

            return bookcaseMod;
        }

        public BookcaseListModel GetList(string domain, BookcaseListModel Page)
        {
            var o_query = from p in basedb.JWL_Bookcase                          
                          where p.Domain == domain
                          select p;

            if (!string.IsNullOrEmpty(Page.Search))
            {
                o_query = o_query.Where(p => p.Name.Contains(Page.Search));
            }


            o_query = o_query.OrderBy(p => p.Name);

            try
            {
                Page.TotalRecords = o_query.Select(p => p.Id).Count();
                Page.Data = o_query.Skip(Page.Skip).Take(Page.Take).Select(o_entity =>
                    new BookcaseModel()
                    {
                        Id = o_entity.Id,
                        Name = o_entity.Name,
                        ThumbnailUrl = o_entity.ThumbnailUrl,
                        CreateTime = o_entity.CreateTime,                        
                        Domain = o_entity.Domain,
                        CreateAccountId = o_entity.CreateAccountId,
                        CategoryId1 = o_entity.CategoryId1,
                        CategoryId2 = o_entity.CategoryId2,
                        CategoryId3 = o_entity.CategoryId3,
                        FileMaxSize = o_entity.FileMaxSize,
                        IsApproveEnabled = o_entity.IsApproveEnabled,
                        IsVersionControlEnabled = o_entity.IsVersionControlEnabled,
                        IsDocumentExpireDelete = o_entity.IsDocumentExpireDelete,          
                        NoInherit = o_entity.NoInherit,      
                    }
                    ).ToList();

                foreach(var item in Page.Data)
                {
                    if (item.CategoryId1 != null)
                    {
                        var nameObj = (from p in basedb.JWL_Category where p.Id == item.CategoryId1 select p.Name).FirstOrDefault();
                        item.CategoryName1 = (nameObj != null) ? nameObj : "無";                            
                    }
                    else
                    {
                        item.CategoryName1 = "無";
                    }

                    if (item.CategoryId2 != null)
                    {
                        var nameObj = (from p in basedb.JWL_Category where p.Id == item.CategoryId2 select p.Name).FirstOrDefault();
                        item.CategoryName2 = (nameObj != null) ? nameObj : "無";   
                    }
                    else
                    {
                        item.CategoryName2 = "無";
                    }

                    if (item.CategoryId3 != null)
                    {
                        var nameObj = (from p in basedb.JWL_Category where p.Id == item.CategoryId3 select p.Name).FirstOrDefault();
                        item.CategoryName3 = (nameObj != null) ? nameObj : "無";                 
                    }
                    else
                    {
                        item.CategoryName3 = "無";
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Page;
        }

        public List<SelectListItem> GetList(string domain, Guid bookcaseId)
        {
            List<SelectListItem> rztObj = new List<SelectListItem>();

            var o_query = from p in basedb.JWL_Bookcase
                          where p.Domain == domain
                          select p;           

            o_query = o_query.OrderBy(p => p.Name);

            try
            {
                foreach (var item in o_query)
                    rztObj.Add(new SelectListItem { Text = item.Name, Value = item.Id.ToString(), Selected = (item.Id == bookcaseId) });

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return rztObj;
        }

        public List<BookcaseModel> GetList(string domain)
        {
            List<BookcaseModel> rztLt = new List<BookcaseModel>();

            var o_query = from p in basedb.JWL_Bookcase
                          where p.Domain == domain
                          select p;


            o_query = o_query.OrderBy(p => p.Name);

            try
            {
                rztLt = o_query.Select(o_entity =>
                    new BookcaseModel()
                    {
                        Id = o_entity.Id,
                        Name = o_entity.Name,
                        ThumbnailUrl = o_entity.ThumbnailUrl,
                        CreateTime = o_entity.CreateTime,
                        Domain = o_entity.Domain,
                        CreateAccountId = o_entity.CreateAccountId,
                        CategoryId1 = o_entity.CategoryId1,
                        CategoryId2 = o_entity.CategoryId2,
                        CategoryId3 = o_entity.CategoryId3,
                        FileMaxSize = o_entity.FileMaxSize,
                        IsApproveEnabled = o_entity.IsApproveEnabled,
                        IsVersionControlEnabled = o_entity.IsVersionControlEnabled,
                        IsDocumentExpireDelete = o_entity.IsDocumentExpireDelete,
                        NoInherit = o_entity.NoInherit,
                    }
                    ).ToList();              
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return rztLt;
        }

        public bool Create(BookcaseModel model, out string ErrMsgs)
        {

            ErrMsgs = string.Empty;

            JWL_Bookcase dbEntity = new JWL_Bookcase();
            dbEntity.Id = model.Id;
            dbEntity.Name = model.Name;
            dbEntity.ThumbnailUrl = model.ThumbnailUrl;
            dbEntity.CreateTime = model.CreateTime;
            dbEntity.Domain = model.Domain;
            dbEntity.CreateAccountId = model.CreateAccountId;
            dbEntity.CategoryId1 = model.CategoryId1;
            dbEntity.CategoryId2 = model.CategoryId2;
            dbEntity.CategoryId3 = model.CategoryId3;
            dbEntity.FileMaxSize = model.FileMaxSize;
            dbEntity.IsApproveEnabled = model.IsApproveEnabled;
            dbEntity.IsVersionControlEnabled = model.IsVersionControlEnabled;
            dbEntity.IsDocumentExpireDelete = model.IsDocumentExpireDelete;
            dbEntity.NoInherit = model.NoInherit;

            basedb.JWL_Bookcase.Add(dbEntity);

            try
            {
                basedb.SaveChanges();
            }
            catch (Exception ex)
            {
                ErrMsgs = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
            }

            return ErrMsgs.Length == 0;
        }

        public BookcaseModel Get(Guid id)
        {
            var o_entity = (from p in basedb.JWL_Bookcase
                            where p.Id == id
                            select p).FirstOrDefault();

            if (o_entity != null)
            {
                var model = new BookcaseModel()
                {
                    Id = o_entity.Id,
                    Name = o_entity.Name,
                    ThumbnailUrl = o_entity.ThumbnailUrl,
                    CreateTime = o_entity.CreateTime,
                    Domain = o_entity.Domain,
                    CreateAccountId = o_entity.CreateAccountId,
                    CategoryId1 = o_entity.CategoryId1,
                    CategoryId2 = o_entity.CategoryId2,
                    CategoryId3 = o_entity.CategoryId3,
                    FileMaxSize = o_entity.FileMaxSize,
                    IsApproveEnabled = o_entity.IsApproveEnabled,
                    IsVersionControlEnabled = o_entity.IsVersionControlEnabled,
                    IsDocumentExpireDelete = o_entity.IsDocumentExpireDelete,         
                    NoInherit = o_entity.NoInherit,
                };

                return model;
            }
            return null;
        }

        public bool Update(BookcaseModel model, out string ErrMsgs)
        {

            ErrMsgs = string.Empty;

            try
            {
                var dbDuty = basedb.JWL_Bookcase.Where(p => p.Id == model.Id).FirstOrDefault();
                if (dbDuty != null)
                {
                    dbDuty.Name = model.Name;
                    dbDuty.ThumbnailUrl = model.ThumbnailUrl;
                    dbDuty.CategoryId1 = model.CategoryId1;
                    dbDuty.CategoryId2 = model.CategoryId2;
                    dbDuty.CategoryId3 = model.CategoryId3;
                    dbDuty.FileMaxSize = model.FileMaxSize;
                    dbDuty.IsApproveEnabled = model.IsApproveEnabled;
                    dbDuty.IsVersionControlEnabled = model.IsVersionControlEnabled;
                    dbDuty.IsDocumentExpireDelete = model.IsDocumentExpireDelete;
                    dbDuty.NoInherit = model.NoInherit;
        
                    basedb.SaveChanges();
                }
                else
                {
                    ErrMsgs = "查無此物件";
                }

            }
            catch (Exception ex)
            {
                ErrMsgs = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
            }

            return ErrMsgs.Length == 0;
        }

        public bool Delete(Guid id, out string ErrorMsg)
        {
            ErrorMsg = string.Empty;

            var o_delete = from p in basedb.JWL_Bookcase
                           where p.Id == id
                           select p;



            foreach (var row in o_delete)
            {
                basedb.JWL_Bookcase.Remove(row);
            }

            try
            {
                basedb.SaveChanges();
            }
            catch (Exception ex)
            {
                ErrorMsg = ex.Message;
            }

            return ErrorMsg.Length == 0;
        }
    }
}