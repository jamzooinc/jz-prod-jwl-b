﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using JamZoo.Project.WebSite.Models;
using JamZoo.Project.WebSite.DbContext;

namespace JamZoo.Project.WebSite.Service
{
    public class FolderDocumentService : BaseService
    {
        public List<FolderDocumentModel> GetList(Guid docId)
        {
            List<FolderDocumentModel> rztLt = new List<FolderDocumentModel>();

            var o_query = from p in basedb.JWL_FolderDocument
                          join t in basedb.JWL_Folder on p.FolderId equals t.Id
                          where p.DocumentId == docId
                          select new { p.FolderId, p.DocumentId, t.Name };           

            o_query = o_query.OrderBy(p => p.Name);

            try
            {
                rztLt = o_query.Select(o_entity =>
                    new FolderDocumentModel()
                    {                        
                        DocumentId = o_entity.DocumentId,
                        FolderId = o_entity.FolderId,
                        FolderName = o_entity.Name              
                    }
                    ).ToList();                

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return rztLt;
        }

        public bool Create(FolderDocumentModel model, out string ErrMsgs)
        {

            ErrMsgs = string.Empty;


            JWL_FolderDocument dbEntity = new JWL_FolderDocument();
            dbEntity.FolderId = model.FolderId;
            dbEntity.DocumentId = model.DocumentId;
            basedb.JWL_FolderDocument.Add(dbEntity);

            try
            {
                basedb.SaveChanges();
            }
            catch (Exception ex)
            {
                ErrMsgs = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
            }

            return ErrMsgs.Length == 0;
        }

        public bool DelAllByDocument(Guid docId, out string ErrMsgs)
        {

            ErrMsgs = string.Empty;

            var o_delete = from p in basedb.JWL_FolderDocument
                           where p.DocumentId == docId
                           select p;



            foreach (var row in o_delete)
            {
                basedb.JWL_FolderDocument.Remove(row);
            }

            try
            {
                basedb.SaveChanges();
            }
            catch (Exception ex)
            {
                ErrMsgs = ex.Message;
            }

            return ErrMsgs.Length == 0;
        }
    }
}