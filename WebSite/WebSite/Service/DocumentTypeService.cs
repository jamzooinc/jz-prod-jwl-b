﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using JamZoo.Project.WebSite.Models;
using JamZoo.Project.WebSite.DbContext;

namespace JamZoo.Project.WebSite.Service
{
    public class DocumentTypeService : BaseService
    {
        public DocumentTypeModel NewInstance()
        {
            DocumentTypeModel newOne = new DocumentTypeModel();            
            return newOne;
        }

        public DocumentTypeListModel GetList(string domain, DocumentTypeListModel Page)
        {
            var o_query = from p in basedb.JWL_DocumentType
                          where p.Domain == domain
                          select p;

            if (!string.IsNullOrEmpty(Page.Search))
            {
                o_query = o_query.Where(p => p.Name.ToUpper().Contains(Page.Search.ToUpper()) || p.FilterKeyword.ToUpper().Contains(Page.Search.ToUpper()));
            }


            o_query = o_query.OrderBy(p => p.Name);

            try
            {
                Page.TotalRecords = o_query.Select(p => p.Id).Count();
                Page.Data = o_query.Skip(Page.Skip).Take(Page.Take).Select(o_entity =>
                    new DocumentTypeModel()
                    {
                        Id = o_entity.Id,
                        Name = o_entity.Name,
                        Domain = o_entity.Domain,
                        SubFileName = o_entity.SubFileName,
                        FilterKeyword = o_entity.FilterKeyword,
                        ConvertPageImage = o_entity.ConvertPageImage                        
                    }
                    ).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Page;
        }

        public List<DocumentTypeModel> GetList(string domain)
        {
            List<DocumentTypeModel> rztLt = new List<DocumentTypeModel>();

            var o_query = from p in basedb.JWL_DocumentType
                          where p.Domain == domain
                          select p;         

            o_query = o_query.OrderBy(p => p.Name);

            try
            {
                rztLt = o_query.Select(o_entity =>
                    new DocumentTypeModel()
                    {
                        Id = o_entity.Id,
                        Name = o_entity.Name,
                        Domain = o_entity.Domain,
                        SubFileName = o_entity.SubFileName,
                        FilterKeyword = o_entity.FilterKeyword,
                        ConvertPageImage = o_entity.ConvertPageImage
                    }
                    ).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return rztLt;
        }

        public List<SelectListItem> GetList(string domain, Guid docTypeId)
        {

            List<SelectListItem> rztObj = new List<SelectListItem>();

            var o_query = from p in basedb.JWL_DocumentType
                          where p.Domain == domain
                          select p;

            o_query = o_query.OrderBy(p => p.Name);

            try
            {
                foreach (var item in o_query)
                    rztObj.Add(new SelectListItem { Text = item.Name, Value = item.Id.ToString(), Selected = (item.Id == docTypeId) });
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return rztObj;
        }

        public bool Create(DocumentTypeModel model, out string ErrMsgs)
        {

            ErrMsgs = string.Empty;

            
            JWL_DocumentType dbEntity = new JWL_DocumentType();
            dbEntity.Id = model.Id;
            dbEntity.Name = model.Name;
            dbEntity.Domain = model.Domain;
            dbEntity.SubFileName = model.SubFileName;
            dbEntity.FilterKeyword = model.FilterKeyword;
            dbEntity.ConvertPageImage = model.ConvertPageImage;
            basedb.JWL_DocumentType.Add(dbEntity);

            try
            {
                basedb.SaveChanges();
            }
            catch (Exception ex)
            {
                ErrMsgs = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
            }

            return ErrMsgs.Length == 0;
        }

        public DocumentTypeModel Get(Guid id)
        {
            var o_entity = (from p in basedb.JWL_DocumentType
                            where p.Id == id
                            select p).FirstOrDefault();

            if (o_entity != null)
            {
                var model = new DocumentTypeModel()
                {
                    Id = o_entity.Id,
                    Name = o_entity.Name,
                    SubFileName = o_entity.SubFileName,
                    Domain = o_entity.Domain,
                    FilterKeyword = o_entity.FilterKeyword,
                    ConvertPageImage = o_entity.ConvertPageImage                  
                };

                return model;
            }
            return null;
        }

        public bool Update(DocumentTypeModel model, out string ErrMsgs)
        {

            ErrMsgs = string.Empty;

            try
            {
                var dbDuty = basedb.JWL_DocumentType.Where(p => p.Id == model.Id).FirstOrDefault();
                if (dbDuty != null)
                {                    
                    dbDuty.Name = model.Name;
                    dbDuty.SubFileName = model.SubFileName;
                    dbDuty.FilterKeyword = model.FilterKeyword;
                    dbDuty.ConvertPageImage = model.ConvertPageImage;   
                    basedb.SaveChanges();
                }
                else
                {
                    ErrMsgs = "查無此物件";
                }

            }
            catch (Exception ex)
            {
                ErrMsgs = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
            }

            return ErrMsgs.Length == 0;
        }

        public bool Delete(Guid id, out string ErrorMsg)
        {
            ErrorMsg = string.Empty;

            var o_delete = from p in basedb.JWL_DocumentType
                           where p.Id == id
                           select p;



            foreach (var row in o_delete)
            {
                basedb.JWL_DocumentType.Remove(row);
            }

            try
            {
                basedb.SaveChanges();
            }
            catch (Exception ex)
            {
                ErrorMsg = ex.Message;
            }

            return ErrorMsg.Length == 0;
        }
    }
}