﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using JamZoo.Project.WebSite.Models;
using JamZoo.Project.WebSite.DbContext;

namespace JamZoo.Project.WebSite.Service
{
    public class DocumentVersionService : BaseService
    {
        public DocumentVersionModel NewInstance()
        {
            DocumentVersionModel newOne = new DocumentVersionModel();
            return newOne;
        }

        //public DocumentVersionListModel GetList(Guid docVerId, DocumentVersionListModel Page)
        //{
        //    string strRzt = string.Empty;

        //    Page.Data = new List<DocumentVersionModel>();

        //    var o_query = from p in basedb.JWL_DocumentVersion
        //                  where p.Id == docVerId
        //                  select new { p };

        //    //if (!string.IsNullOrEmpty(Page.Search))
        //    //{
        //    //    o_query = o_query.Where(p => p.Id.Contains(Page.Search));
        //    //}


        //    o_query = o_query.OrderBy(p => p.p.Id);

        //    try
        //    {
        //        Page.TotalRecords = o_query.Select(p => p.p.Id).Count();
        //        foreach( var o_entity in o_query.Skip(Page.Skip).Take(Page.Take).ToList())
        //        {
        //            var docVerObj = (from p in basedb.JWL_Document
        //                             where p.Id == o_entity.p.DocumentId
        //                          select p).FirstOrDefault();

        //            if(docVerObj == null)
        //                continue;

        //            DocumentVersionModel docMod = new DocumentVersionModel();
        //            docMod.Id = o_entity.p.Id;
        //            docMod.CreateTime = o_entity.p.CreateTime;
        //            docMod.Domain = o_entity.p.Domain;
        //            docMod.CreateAccountId = o_entity.p.CreateAccountId;                    
        //            docMod.DocumentVersionTypeId = o_entity.p.DocumentVersionTypeId;
        //            docMod.DocumentVersionTypeName = o_entity.Name;
        //            docMod.CategoryItemId1 = o_entity.p.CategoryItemId1;
        //            docMod.CategoryItemId2 = o_entity.p.CategoryItemId2;
        //            docMod.CategoryItemId3 = o_entity.p.CategoryItemId3;

        //            docMod.DocumentVersionDisplayName = docVerObj.DisplayName;
        //            docMod.Version = docVerObj.Version;
        //            docMod.DisplayVersion = docVerObj.DisplayVersion;
        //            docMod.State = docVerObj.State;

        //            Page.Data.Add(docMod);
                                                                            
        //        }                                            
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //    return Page;
        //}        

        //public DocumentVersionModel Get(Guid id)
        //{
        //    var o_entity = (from p in basedb.JWL_DocumentVersion
        //                    where p.Id == id
        //                    select p).FirstOrDefault();

        //    if (o_entity != null)
        //    {
        //        var model = new DocumentVersionModel()
        //        {
        //            Id = o_entity.Id,
        //            Name = o_entity.Name,
        //            ThumbnailUrl = o_entity.ThumbnailUrl,
        //            CreateTime = o_entity.CreateTime,
        //            Domain = o_entity.Domain,
        //            CreateAccountId = o_entity.CreateAccountId,
        //            CategoryId1 = o_entity.CategoryId1,
        //            CategoryId2 = o_entity.CategoryId2,
        //            CategoryId3 = o_entity.CategoryId3,
        //            FileMaxSize = o_entity.FileMaxSize,
        //            IsApproveEnabled = o_entity.IsApproveEnabled,
        //            IsVersionControlEnabled = o_entity.IsVersionControlEnabled,
        //            IsDocumentVersionExpireDelete = o_entity.IsDocumentVersionExpireDelete,
        //        };

        //        return model;
        //    }
        //    return null;
        //}

        //public bool Create(DocumentVersionModel model, out string ErrMsgs)
        //{

        //    ErrMsgs = string.Empty;

        //    JWL_DocumentVersion dbEntity = new JWL_DocumentVersion();
        //    dbEntity.Id = model.Id;            
        //    dbEntity.CreateTime = model.CreateTime;
        //    dbEntity.Domain = model.Domain;
        //    dbEntity.CreateAccountId = model.CreateAccountId;
        //    dbEntity.DocumentVersionTypeId = model.DocumentVersionTypeId;
        //    dbEntity.CategoryItemId1 = model.CategoryItemId1;
        //    dbEntity.CategoryItemId2 = model.CategoryItemId2;
        //    dbEntity.CategoryItemId3 = model.CategoryItemId3;            

        //    basedb.JWL_DocumentVersion.Add(dbEntity);

        //    try
        //    {
        //        basedb.SaveChanges();
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrMsgs = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
        //    }

        //    return ErrMsgs.Length == 0;
        //}        

        //public bool Update(DocumentVersionModel model, out string ErrMsgs)
        //{

        //    ErrMsgs = string.Empty;

        //    try
        //    {
        //        var dbDuty = basedb.JWL_DocumentVersion.Where(p => p.Id == model.Id).FirstOrDefault();
        //        if (dbDuty != null)
        //        {
        //            dbDuty.Name = model.Name;
        //            dbDuty.ThumbnailUrl = model.ThumbnailUrl;
        //            dbDuty.CategoryId1 = model.CategoryId1;
        //            dbDuty.CategoryId2 = model.CategoryId2;
        //            dbDuty.CategoryId3 = model.CategoryId3;
        //            dbDuty.FileMaxSize = model.FileMaxSize;
        //            dbDuty.IsApproveEnabled = model.IsApproveEnabled;
        //            dbDuty.IsVersionControlEnabled = model.IsVersionControlEnabled;
        //            dbDuty.IsDocumentVersionExpireDelete = model.IsDocumentVersionExpireDelete;

        //            basedb.SaveChanges();
        //        }
        //        else
        //        {
        //            ErrMsgs = "查無此物件";
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        ErrMsgs = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
        //    }

        //    return ErrMsgs.Length == 0;
        //}

        //public bool Delete(Guid id, out string ErrorMsg)
        //{
        //    ErrorMsg = string.Empty;

        //    var o_delete = from p in basedb.JWL_DocumentVersion
        //                   where p.Id == id
        //                   select p;



        //    foreach (var row in o_delete)
        //    {
        //        basedb.JWL_DocumentVersion.Remove(row);
        //    }

        //    try
        //    {
        //        basedb.SaveChanges();
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorMsg = ex.Message;
        //    }

        //    return ErrorMsg.Length == 0;
        //}
    }
}