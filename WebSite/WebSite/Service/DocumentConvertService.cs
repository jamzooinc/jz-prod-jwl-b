﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using JamZoo.Project.WebSite.Models;
using JamZoo.Project.WebSite.DbContext;

namespace JamZoo.Project.WebSite.Service
{
    public class DocumentConvertService : BaseService
    {
        public DocumentConvertModel NewInstance()
        {
            DocumentConvertModel newOne = new DocumentConvertModel();
            return newOne;
        }

        public DocumentConvertListModel GetList(DocumentConvertListModel Page)
        {
            string strRzt = string.Empty;

            Page.Data = new List<DocumentConvertModel>();

            var o_query = from p in basedb.JWL_DocumentConvert
                          where p.ConvertStatus == 0 || p.ConvertStatus == 1
                          select p;


            o_query = o_query.OrderBy(p => p.ConvertStartTime);

            try
            {
                Page.TotalRecords = o_query.Select(p => p.Id).Count();

                try
                {                    
                    Page.Data = o_query.Select(o_entity =>
                        new DocumentConvertModel()
                        {
                            Id = o_entity.Id,
                            VersionId = o_entity.VersionId,
                            ConvertStatus = o_entity.ConvertStatus,                            
                            ConvertStartTime = o_entity.ConvertStartTime,
                            ConvertEndTime = o_entity.ConvertEndTime,
                            ConvertMessage = o_entity.ConvertMessage,
                            CreateTime = o_entity.CreateTime,
                            ModifyTime = o_entity.ModifyTime,               
                        }
                        ).ToList();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Page;
        }        

        public DocumentConvertModel Get(Guid id)
        {
            var o_entity = (from p in basedb.JWL_DocumentConvert
                            where p.Id == id
                            select p).FirstOrDefault();

            if (o_entity != null)
            {
                var model = new DocumentConvertModel()
                {
                    Id = o_entity.Id,
                    VersionId = o_entity.VersionId,
                    ConvertStatus = o_entity.ConvertStatus,
                    ConvertStartTime = o_entity.ConvertStartTime,
                    ConvertEndTime = o_entity.ConvertEndTime,
                    ConvertMessage = o_entity.ConvertMessage,
                    CreateTime = o_entity.CreateTime,
                    ModifyTime = o_entity.ModifyTime, 
                };

                return model;
            }
            return null;
        }

        public bool Create(DocumentConvertModel model, out string ErrMsgs)
        {

            ErrMsgs = string.Empty;


            JWL_DocumentConvert dbEntity = new JWL_DocumentConvert();
            dbEntity.Id = model.Id;
            dbEntity.VersionId = model.VersionId;
            dbEntity.ConvertStatus = model.ConvertStatus;
            dbEntity.CreateTime = model.CreateTime;
            dbEntity.ModifyTime = model.ModifyTime;

            basedb.JWL_DocumentConvert.Add(dbEntity);

            try
            {
                basedb.SaveChanges();
            }
            catch (Exception ex)
            {
                ErrMsgs = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
            }

            return ErrMsgs.Length == 0;
        }       

        public bool Update(DocumentConvertModel model, out string ErrMsgs)
        {

            ErrMsgs = string.Empty;

            try
            {
                var dbDuty = basedb.JWL_DocumentConvert.Where(p => p.Id == model.Id).FirstOrDefault();
                if (dbDuty != null)
                {
                    dbDuty.ConvertStatus = model.ConvertStatus;
                    dbDuty.ConvertStartTime = model.ConvertStartTime;
                    dbDuty.ConvertEndTime = model.ConvertEndTime;
                    dbDuty.ConvertMessage = model.ConvertMessage;
                    dbDuty.ModifyTime = model.ModifyTime;
                    basedb.SaveChanges();
                }
                else
                {
                    ErrMsgs = "查無此物件";
                }

            }
            catch (Exception ex)
            {
                ErrMsgs = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
            }

            return ErrMsgs.Length == 0;
        }
    }
}