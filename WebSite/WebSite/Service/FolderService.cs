﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using JamZoo.Project.WebSite.Models;
using JamZoo.Project.WebSite.DbContext;

namespace JamZoo.Project.WebSite.Service
{
    public class FolderService : BaseService
    {
        public FolderModel NewInstance()
        {
            FolderModel newOne = new FolderModel();            
            return newOne;
        }

        public FolderListModel GetList(string domain, Guid BookcaseId, FolderListModel Page)
        {
            var o_query = from p in basedb.JWL_Folder
                          join b in basedb.JWL_Bookcase on p.BookcaseId equals b.Id
                          where p.Domain == domain && p.BookcaseId == BookcaseId
                          select new { p, b.Name };

            var bkObj = (from b in basedb.JWL_Bookcase
                         where b.Id == BookcaseId
                         select b).FirstOrDefault();

            if (!string.IsNullOrEmpty(Page.Search))
            {
                o_query = o_query.Where(p => p.Name.Contains(Page.Search));
            }


            o_query = o_query.OrderBy(p => p.Name);

            try
            {
                Page.TotalRecords = o_query.Select(p => p.p.Id).Count();
                Page.BookCaseIsNoInherit = bkObj.NoInherit;
                Page.Data = o_query.Skip(Page.Skip).Take(Page.Take).Select(o_entity =>
                    new FolderModel()
                    {
                        Id = o_entity.p.Id,
                        BookcaseId = o_entity.p.BookcaseId,
                        BookcaseName = o_entity.Name,
                        Name = o_entity.p.Name,
                        TypeName = o_entity.p.TypeName,
                        ThumbnailUrl = o_entity.p.ThumbnailUrl,
                        CreateTime = o_entity.p.CreateTime,
                        Domain = o_entity.p.Domain,
                        CreateAccountId = o_entity.p.CreateAccountId,                                       
                    }
                    ).ToList();                

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Page;
        }

        public List<FolderModel> GetList(string domain, Guid bookcaseId)
        {
            List<FolderModel> rztObj = new List<FolderModel>();

            var o_query = from p in basedb.JWL_Folder
                          join b in basedb.JWL_Bookcase on p.BookcaseId equals b.Id
                          where p.Domain == domain && p.BookcaseId == bookcaseId
                          select new { p, b.Name };

            o_query = o_query.OrderBy(p => p.Name);

            try
            {
                rztObj = o_query.Select(o_entity =>
                    new FolderModel()
                    {
                        Id = o_entity.p.Id,
                        Name = o_entity.p.Name,
                        TypeName = o_entity.p.TypeName,
                        ThumbnailUrl = o_entity.p.ThumbnailUrl,
                        CreateTime = o_entity.p.CreateTime,
                        Domain = o_entity.p.Domain,
                        CreateAccountId = o_entity.p.CreateAccountId,
                    }
                    ).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return rztObj;
        }

        public bool Create(FolderModel model, out string ErrMsgs)
        {

            ErrMsgs = string.Empty;

            JWL_Folder dbEntity = new JWL_Folder();
            dbEntity.Id = model.Id;
            dbEntity.BookcaseId = model.BookcaseId;
            dbEntity.Name = model.Name;
            dbEntity.TypeName = model.TypeName;
            dbEntity.ThumbnailUrl = model.ThumbnailUrl;
            dbEntity.CreateTime = model.CreateTime;
            dbEntity.Domain = model.Domain;
            dbEntity.CreateAccountId = model.CreateAccountId;
            dbEntity.NoInherit = model.NoInherit;     
    
            basedb.JWL_Folder.Add(dbEntity);

            try
            {
                basedb.SaveChanges();
            }
            catch (Exception ex)
            {
                ErrMsgs = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
            }

            return ErrMsgs.Length == 0;
        }

        public FolderModel Get(Guid id)
        {
            var o_entity = (from p in basedb.JWL_Folder
                            where p.Id == id
                            select p).FirstOrDefault();

            if (o_entity != null)
            {
                var model = new FolderModel()
                {
                    Id = o_entity.Id,
                    BookcaseId = o_entity.BookcaseId,
                    Name = o_entity.Name,
                    TypeName = o_entity.TypeName,
                    ThumbnailUrl = o_entity.ThumbnailUrl,
                    CreateTime = o_entity.CreateTime,
                    Domain = o_entity.Domain,
                    CreateAccountId = o_entity.CreateAccountId,
                    NoInherit = o_entity.NoInherit,                             
                };

                return model;
            }
            return null;
        }

        public bool Update(FolderModel model, out string ErrMsgs)
        {

            ErrMsgs = string.Empty;

            try
            {
                var dbObj = basedb.JWL_Folder.Where(p => p.Id == model.Id).FirstOrDefault();
                if (dbObj != null)
                {
                    dbObj.Name = model.Name;
                    dbObj.BookcaseId = model.BookcaseId;
                    dbObj.TypeName = model.TypeName;
                    dbObj.NoInherit = model.NoInherit;
                    dbObj.ThumbnailUrl = model.ThumbnailUrl == null ? "" : model.ThumbnailUrl;
                                                 
                    basedb.SaveChanges();
                }
                else
                {
                    ErrMsgs = "查無此物件";
                }

            }
            catch (Exception ex)
            {
                ErrMsgs = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
            }

            return ErrMsgs.Length == 0;
        }

        public bool Delete(Guid id, out string ErrorMsg)
        {
            ErrorMsg = string.Empty;

            var o_delete = from p in basedb.JWL_Folder
                           where p.Id == id
                           select p;



            foreach (var row in o_delete)
            {
                basedb.JWL_Folder.Remove(row);
            }

            try
            {
                basedb.SaveChanges();
            }
            catch (Exception ex)
            {
                ErrorMsg = ex.Message;
            }

            return ErrorMsg.Length == 0;
        }
    }
}