﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using JamZoo.Project.WebSite.Models;
using JamZoo.Project.WebSite.DbContext;

namespace JamZoo.Project.WebSite.Service
{
    public class CategoryItemService : BaseService
    {
        public CategoryItemModel NewInstance()
        {
            CategoryItemModel newOne = new CategoryItemModel();            
            return newOne;
        }

        public CategoryItemListModel GetList(string domain, Guid CategoryId, CategoryItemListModel Page)
        {
            var o_query = from p in basedb.JWL_CategoryItem
                          where p.Domain == domain && p.CategoryId == CategoryId
                          select p;

            if (!string.IsNullOrEmpty(Page.Search))
            {
                o_query = o_query.Where(p => p.Name.Contains(Page.Search));
            }


            o_query = o_query.OrderBy(p => p.Name);

            try
            {
                Page.TotalRecords = o_query.Select(p => p.Id).Count();
                Page.Data = o_query.Skip(Page.Skip).Take(Page.Take).Select(o_entity =>
                    new CategoryItemModel()
                    {
                        Id = o_entity.Id,
                        Name = o_entity.Name,
                        CategoryId = o_entity.CategoryId,
                        CreateAccountId = o_entity.CreateAccountId                        
                    }
                    ).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Page;
        }

        public List<SelectListItem> GetList(Guid CategoryId, string selectedItemName)
        {
            List<SelectListItem> ItemList = new List<SelectListItem>();

            var o_query = from p in basedb.JWL_CategoryItem
                          where p.CategoryId == CategoryId
                          select p;

            o_query = o_query.OrderBy(p => p.Name);

            try
            {                
                foreach (var p in o_query)
                {
                    ItemList.Add(new SelectListItem()
                    {
                        Text = p.Name,
                        Value = p.Id.ToString(),
                        Selected = p.Id.Equals(selectedItemName)
                    });
                }

                if (ItemList.Count > 0)
                {
                    ItemList.Insert(0,new SelectListItem()
                    {
                        Text = "請選擇",
                        Value = "",
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ItemList.Count > 0 ? ItemList : null;
        }

        public bool Create(CategoryItemModel model, out string ErrMsgs)
        {

            ErrMsgs = string.Empty;

            
            JWL_CategoryItem dbEntity = new JWL_CategoryItem();
            dbEntity.Id = model.Id;
            dbEntity.Name = model.Name;
            dbEntity.Domain = model.Domain;
            dbEntity.CategoryId = model.CategoryId;
            dbEntity.CreateAccountId = model.CreateAccountId;
            basedb.JWL_CategoryItem.Add(dbEntity);

            try
            {
                basedb.SaveChanges();
            }
            catch (Exception ex)
            {
                ErrMsgs = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
            }

            return ErrMsgs.Length == 0;
        }

        public CategoryItemModel Get(Guid id)
        {
            var o_entity = (from p in basedb.JWL_CategoryItem
                            where p.Id == id
                            select p).FirstOrDefault();

            if (o_entity != null)
            {
                var model = new CategoryItemModel()
                {
                    Id = o_entity.Id,
                    Name = o_entity.Name,
                    CategoryId = o_entity.CategoryId,
                    Domain = o_entity.Domain,
                    CreateAccountId = o_entity.CreateAccountId                  
                };

                return model;
            }
            return null;
        }

        public bool Update(CategoryItemModel model, out string ErrMsgs)
        {

            ErrMsgs = string.Empty;

            try
            {
                var dbDuty = basedb.JWL_CategoryItem.Where(p => p.Id == model.Id).FirstOrDefault();
                if (dbDuty != null)
                {                   
                    dbDuty.Name = model.Name;
                    dbDuty.CategoryId = model.CategoryId;                                        
                    basedb.SaveChanges();
                }
                else
                {
                    ErrMsgs = "查無此物件";
                }

            }
            catch (Exception ex)
            {
                ErrMsgs = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
            }

            return ErrMsgs.Length == 0;
        }

        public bool Delete(Guid id, out string ErrorMsg)
        {
            ErrorMsg = string.Empty;

            var o_delete = from p in basedb.JWL_CategoryItem
                           where p.Id == id
                           select p;



            foreach (var row in o_delete)
            {
                basedb.JWL_CategoryItem.Remove(row);
            }

            try
            {
                basedb.SaveChanges();
            }
            catch (Exception ex)
            {
                ErrorMsg = ex.Message;
            }

            return ErrorMsg.Length == 0;
        }
    }
}