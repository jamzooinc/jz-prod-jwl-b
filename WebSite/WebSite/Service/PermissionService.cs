﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using JamOA.sdk;
using JamOA.sdk.Model;
using JamZoo.Project.WebSite.DbContext;
using JamZoo.Project.WebSite.Enums;
using JamZoo.Project.WebSite.Library;
using JamZoo.Project.WebSite.Library.Principal;
using JamZoo.Project.WebSite.Models;

namespace JamZoo.Project.WebSite.Service
{
    public class PermissionService : BaseService
    {
        public static IEnumerable<RoleModel> Roles { get; set; }

        static PermissionService()
        {
            if(Roles == null)
                Roles = JamOAClient.ProjectRoles();
        }

        public PermissionModel NewInstance()
        {
            var newOne = new PermissionModel();
            return newOne;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="domain"></param>
        /// <param name="contentType">0:, 1:, 2:</param>
        /// <param name="contentId"></param>
        /// <param name="Page"></param>
        /// <returns></returns>
        public PermissionListModel GetList(string domain, PermissionContentType contentType, Guid contentId, PermissionListModel Page)
        {
            var o_query = basedb.JWL_Permission.Where(p => p.Domain == domain);
            switch (contentType)
            {
                case PermissionContentType.Bookcase:
                    o_query = o_query.Where(p => p.BookcaseId == contentId);
                    break;
                case PermissionContentType.Folder:
                    o_query = o_query.Where(p => p.FolderId == contentId);
                    break;
                case PermissionContentType.Document:
                    o_query = o_query.Where(p => p.DocumentId == contentId);
                    break;
            }
            o_query = o_query.OrderBy(p => p.RoleType);

            try
            {
                Page.TotalRecords = o_query.Select(p => p.Id).Count();
                var permissions = o_query.Skip(Page.Skip).Take(Page.Take);
                foreach (var permission in permissions)
                {
                    var permissionModel = permission.Inject<JWL_Permission, PermissionModel>();
                    switch (permission.RoleType)
                    {
                        case 0:
                            permissionModel.RoleTypeName = "人員";
                            permissionModel.UnitId = permission.AccountId;
                            if(!string.IsNullOrEmpty(permission.AccountId))
                                permissionModel.UnitName = JamOAClient.MeInfo(permission.AccountId).DisplayName;
                            break;
                        case 1:
                            permissionModel.RoleTypeName = "部門";
                            permissionModel.UnitId = permission.OrganId;
                            permissionModel.UnitName = JamOAClient.OrganInfo(permission.OrganId).DisplayName;
                            break;
                        //case 2:
                        //    permissionModel.RoleTypeName = "群組";
                        //    break;
                    }
                    //var roleContent = JamOAClient.GetRoleContents(permission.RoleId).FirstOrDefault();
                    //if (roleContent != null)
                    //{
                    //    permissionModel.RoleUnitName = roleContent.RefName;
                    //}

                    permissionModel.RoleName = Roles.First(r => r.Id == permission.RoleId).DisplayName;
                    Page.Data.Add(permissionModel);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Page;
        }

        public bool Create(PermissionModel model, out string ErrMsgs)
        {
            ErrMsgs = String.Empty;
            try
            {
                var entity = model.Inject<PermissionModel, JWL_Permission>();
                entity.Id = Guid.NewGuid();
                entity.Domain = UserManager.User.Domain;
                if (model.RoleType == 0)
                {
                    entity.AccountId = model.UnitId;
                }
                else
                {
                    entity.OrganId = model.UnitId;
                }
                entity.CreateAccountId = UserManager.User.Id;
                basedb.JWL_Permission.Add(entity);
                basedb.SaveChanges();
            }
            catch (Exception ex)
            {
                ErrMsgs = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
            }
            return ErrMsgs.Length == 0;
        }

        public PermissionModel Get(Guid id)
        {
            var permission = (from p in basedb.JWL_Permission
                            where p.Id == id
                            select p).FirstOrDefault();

            if (permission != null)
            {
                var permissionModel = permission.Inject<JWL_Permission, PermissionModel>();
                //var roleContent = JamOAClient.GetRoleContents(model.RoleId).FirstOrDefault();
                //if (roleContent != null)
                //    model.RoleUnitName = roleContent.RefName;
                switch (permission.RoleType)
                {
                    case 0:
                        permissionModel.RoleTypeName = "人員";
                        permissionModel.UnitId = permission.AccountId;
                        permissionModel.UnitName = JamOAClient.MeInfo(permission.AccountId).DisplayName;
                        break;
                    case 1:
                        permissionModel.RoleTypeName = "部門";
                        permissionModel.UnitId = permission.OrganId;
                        permissionModel.UnitName = JamOAClient.OrganInfo(permission.OrganId).DisplayName;
                        break;
                        //case 2:
                        //    permissionModel.RoleTypeName = "群組";
                        //    break;
                }
                //var roleContent = JamOAClient.GetRoleContents(permission.RoleId).FirstOrDefault();
                //if (roleContent != null)
                //{
                //    permissionModel.RoleUnitName = roleContent.RefName;
                //}

                permissionModel.RoleName = Roles.First(r => r.Id == permission.RoleId).DisplayName;
                return permissionModel;
            }
            return null;
        }

        public bool Update(PermissionModel model, out string ErrMsgs)
        {

            ErrMsgs = string.Empty;

            try
            {
                var o_entity = (from p in basedb.JWL_Permission
                                where p.Id == model.Id
                                select p).FirstOrDefault();

                model.Inject<PermissionModel, JWL_Permission>(o_entity);
                if (model.RoleType == 0)
                {
                    o_entity.AccountId = model.UnitId;
                }
                else
                {
                    o_entity.OrganId = model.UnitId;
                }
                basedb.SaveChanges();
            }
            catch (Exception ex)
            {
                ErrMsgs = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
            }

            return ErrMsgs.Length == 0;
        }

        public bool Delete(Guid id, out string ErrorMsg)
        {
            ErrorMsg = string.Empty;

            var o_delete = basedb.JWL_Permission.FirstOrDefault(p => p.Id == id);

            if(o_delete != null)
            {
                basedb.JWL_Permission.Remove(o_delete);
            }

            try
            {
                basedb.SaveChanges();
            }
            catch (Exception ex)
            {
                ErrorMsg = ex.Message;
            }

            return ErrorMsg.Length == 0;
        }
    }

}