﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using JamZoo.Project.WebSite.Enums;
using JamZoo.Project.WebSite.Models;
using JamZoo.Project.WebSite.DbContext;

namespace JamZoo.Project.WebSite.Service
{
    public class DocumentService : BaseService
    {
        public DocumentModel NewInstance()
        {
            DocumentModel newOne = new DocumentModel();
            return newOne;
        }

        public DocumentListModel GetList(string domain, DocumentListModel Page, Guid bookCaseeId, out BookcaseModel bookcaseMod)
        {
            bookcaseMod = new BookcaseModel();

            string strRzt = string.Empty;

            var bk_query = from p in basedb.JWL_Bookcase
                           where p.Domain == domain
                           select p;

            bk_query = bk_query.OrderBy(p => p.Name);

            foreach (var item in bk_query)
            {
                if (item.Id == bookCaseeId)
                {
                    bookcaseMod.Id = bookCaseeId;
                    bookcaseMod.Name = item.Name;
                    bookcaseMod.CategoryId1 = item.CategoryId1;
                    bookcaseMod.CategoryId2 = item.CategoryId2;
                    bookcaseMod.CategoryId3 = item.CategoryId3;
                    bookcaseMod.ThumbnailUrl = item.ThumbnailUrl;

                    //目前是第一個
                    if (bk_query.ToList().IndexOf(item) == 0)
                    {
                        if (bk_query.Count() > 2)
                        {
                            bookcaseMod.NextId = bk_query.ToList()[1].Id;
                            break;
                        }

                    }

                    //目前是最後一個
                    if (bk_query.ToList().IndexOf(item) == bk_query.Count() - 1)
                    {
                        bookcaseMod.PreId = bk_query.ToList()[bk_query.Count() - 2].Id;
                        break;
                    }

                    bookcaseMod.PreId = bk_query.ToList()[bk_query.ToList().IndexOf(item) - 1].Id;
                    bookcaseMod.NextId = bk_query.ToList()[bk_query.ToList().IndexOf(item) + 1].Id;
                    break;
                }
            }

            Page.Data = new List<DocumentModel>();

            var o_query = from p in basedb.JWL_Document
                          join t in basedb.JWL_DocumentType on p.DocumentTypeId equals t.Id
                          where p.Domain == domain && p.BookcaseId == bookCaseeId
                          select new { p, t.Name };           

            o_query = o_query.OrderBy(p => p.p.Id);

            try
            {
                Page.TotalRecords = o_query.Select(p => p.p.Id).Count();
                foreach (var o_entity in o_query)
                {
                    var docVerObj = (from p in basedb.JWL_DocumentVersion
                                     where p.DocumentId == o_entity.p.Id
                                     select p).OrderByDescending(p => p.ModifyTime).FirstOrDefault();

                    var convObj = from p in basedb.JWL_DocumentConvert
                                  join v in basedb.JWL_DocumentVersion on p.VersionId equals v.Id
                                  where (p.ConvertStatus == 0 || p.ConvertStatus == 1) && v.DocumentId == o_entity.p.Id
                                  select p;

                    if (docVerObj == null)
                        continue;                   

                    DocumentModel docMod = new DocumentModel();
                    docMod.Id = o_entity.p.Id;
                    docMod.CreateTime = o_entity.p.CreateTime;
                    docMod.Domain = o_entity.p.Domain;
                    docMod.CreateAccountId = o_entity.p.CreateAccountId;
                    docMod.DocumentTypeId = o_entity.p.DocumentTypeId;
                    docMod.DocumentTypeName = o_entity.Name;
                    docMod.CategoryItemId1 = o_entity.p.CategoryItemId1;
                    docMod.CategoryItemId2 = o_entity.p.CategoryItemId2;
                    docMod.CategoryItemId3 = o_entity.p.CategoryItemId3;
                    docMod.BookcaseId = bookcaseMod.Id;
                    docMod.BookcaseName = bookcaseMod.Name;
                    docMod.ConvertStatus = (docVerObj.FileUrl == null || string.IsNullOrEmpty(docVerObj.FileUrl)) ? "未上傳" : (convObj.Count() > 0 ? "未完成" : "完成");

                    docMod.DocVerMod.FileUrl = docVerObj.FileUrl;
                    docMod.DocVerMod.FileName = docVerObj.FileName;
                    docMod.DocVerMod.CoverThumbnailUrl = docVerObj.CoverThumbnailUrl;
                    docMod.DocVerMod.DisplayName = docVerObj.DisplayName;
                    docMod.DocVerMod.Version = docVerObj.Version;
                    docMod.DocVerMod.DisplayVersion = docVerObj.DisplayVersion;
                    docMod.DocVerMod.State = (DocVerState)docVerObj.State;

                    Page.Data.Add(docMod);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (!string.IsNullOrEmpty(Page.Search))
            {
                Page.Data = Page.Data.Where(p => p.DocVerMod.DisplayName.Contains(Page.Search)).ToList();
            }

            Page.Data = Page.Data.Skip(Page.Skip).Take(Page.Take).ToList();

            return Page;
        }

        public DocumentListModel GetList(string domain, DocumentListModel Page, Guid? bookCaseeId, Guid? foldId, bool IsApproveOnly)
        {
            string strRzt = string.Empty;

            Page.Data = new List<DocumentModel>();

            var o_query = foldId == null ?
                          (from p in basedb.JWL_Document
                          join t in basedb.JWL_DocumentType on p.DocumentTypeId equals t.Id
                          where p.Domain == domain
                          && (bookCaseeId == null ? true : p.BookcaseId == bookCaseeId)
                          select new { p, t.Name, t.FilterKeyword }) 
                          :
                          (from p in basedb.JWL_Document
                           join f in basedb.JWL_FolderDocument on p.Id equals f.DocumentId
                           join t in basedb.JWL_DocumentType on p.DocumentTypeId equals t.Id
                           where p.Domain == domain && f.FolderId == foldId
                           && (bookCaseeId == null ? true : p.BookcaseId == bookCaseeId)
                           select new { p, t.Name, t.FilterKeyword });

            var IsFdNoInherit = foldId == null ? false : (from f in basedb.JWL_Folder
                                                        where f.Id == foldId
                                                        select f).FirstOrDefault().NoInherit;

            try
            {
                Page.TotalRecords = o_query.Select(p => p.p.Id).Count();
                foreach (var o_entity in o_query)
                {
                    var bk_query = (from p in basedb.JWL_Bookcase
                                    where p.Id == o_entity.p.BookcaseId
                                    select p).FirstOrDefault();  

                    var docVerObj = (from p in basedb.JWL_DocumentVersion
                                     where p.DocumentId == o_entity.p.Id
                                     select p).OrderByDescending(p => p.ModifyTime).FirstOrDefault();

                    var convObj = from p in basedb.JWL_DocumentConvert
                                  join v in basedb.JWL_DocumentVersion on p.VersionId equals v.Id
                                  where (p.ConvertStatus == 0 || p.ConvertStatus == 1) && v.DocumentId == o_entity.p.Id
                                  select p;

                    if (docVerObj == null)
                        continue;

                    if (IsApproveOnly && docVerObj.State != 1)
                        continue;

                    //var fdsObj = from fd in basedb.JWL_FolderDocument
                    //             join f in basedb.JWL_Folder on fd.FolderId equals f.Id
                    //             where fd.DocumentId == o_entity.p.Id
                    //             select f;                    

                    DocumentModel docMod = new DocumentModel();
                    docMod.Id = o_entity.p.Id;
                    docMod.CreateTime = o_entity.p.CreateTime;
                    docMod.Domain = o_entity.p.Domain;
                    docMod.CreateAccountId = o_entity.p.CreateAccountId;
                    docMod.DocumentTypeId = o_entity.p.DocumentTypeId;
                    docMod.DocumentTypeName = o_entity.Name;
                    docMod.DocumentKeyWord = o_entity.FilterKeyword;
                    docMod.CategoryItemId1 = o_entity.p.CategoryItemId1;
                    docMod.CategoryItemId2 = o_entity.p.CategoryItemId2;
                    docMod.CategoryItemId3 = o_entity.p.CategoryItemId3;
                    docMod.BookcaseId = bk_query.Id;
                    docMod.BookcaseName = bk_query.Name;
                    docMod.ConvertStatus = (docVerObj.FileUrl == null || string.IsNullOrEmpty(docVerObj.FileUrl)) ? "未上傳" : (convObj.Count() > 0 ? "未完成" : "完成");

                    docMod.DocVerMod.FileUrl = docVerObj.FileUrl;
                    docMod.DocVerMod.FileName = docVerObj.FileName;
                    docMod.DocVerMod.CoverThumbnailUrl = docVerObj.CoverThumbnailUrl;
                    docMod.DocVerMod.DisplayName = docVerObj.DisplayName;
                    docMod.DocVerMod.Version = docVerObj.Version;
                    docMod.DocVerMod.DisplayVersion = docVerObj.DisplayVersion;
                    docMod.DocVerMod.State = (DocVerState)docVerObj.State;

                    //foreach (var fObj in fdsObj)
                    //{
                    //    docMod.FolderString += string.Format("[{0}]", fObj.Name);
                    //}           

                    if (bookCaseeId != null)
                        Page.FolderIsNoInherit = IsFdNoInherit;                                            

                    Page.Data.Add(docMod);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (!string.IsNullOrEmpty(Page.Search))
            {
                List<DocumentModel> docModLt = new List<DocumentModel>();
                foreach (var docObj in Page.Data)
                {
                    if (docObj.DocVerMod.DisplayName.ToUpper().Contains(Page.Search.ToUpper()))
                        docModLt.Add(docObj);

                    if (docObj.DocumentKeyWord.ToUpper().Contains(Page.Search.ToUpper()))
                        docModLt.Add(docObj);

                    Page.Data = docModLt;
                }                
            }

            Page.Data = Page.Data.Skip(Page.Skip).Take(Page.Take).ToList();

            Page.Data = IsApproveOnly ? 
                Page.Data.OrderBy(p => p.BookcaseName).ToList() 
                : 
                Page.Data.OrderByDescending(p => p.DocVerMod.ModifyTime).ToList();

            return Page;
        }        

        public DocumentModel Get(Guid id)
        {
            string strRzt = string.Empty;            

            var o_query = (from p in basedb.JWL_Document
                          join t in basedb.JWL_DocumentType on p.DocumentTypeId equals t.Id
                          where p.Id == id
                          select new { p, t.Name }).FirstOrDefault();           

            try
            {
                if (o_query != null)
                {
                    var docVerObj = (from p in basedb.JWL_DocumentVersion
                                     where p.DocumentId == o_query.p.Id
                                     select p).OrderByDescending(p=>p.ModifyTime).FirstOrDefault();

                    if (docVerObj != null)
                    {
                        DocumentModel docMod = new DocumentModel();
                        docMod.Id = o_query.p.Id;
                        docMod.BookcaseId = o_query.p.BookcaseId;
                        docMod.DocumentTypeId = docMod.DocumentTypeId;
                        docMod.CreateTime = o_query.p.CreateTime;
                        docMod.Domain = o_query.p.Domain;
                        docMod.CreateAccountId = o_query.p.CreateAccountId;
                        docMod.DocumentTypeId = o_query.p.DocumentTypeId;
                        docMod.DocumentTypeName = o_query.Name;
                        docMod.CategoryItemId1 = o_query.p.CategoryItemId1;
                        docMod.CategoryItemId2 = o_query.p.CategoryItemId2;
                        docMod.CategoryItemId3 = o_query.p.CategoryItemId3;
                        docMod.NoInherit = o_query.p.NoInherit;

                        docMod.DocVerMod.Id = docVerObj.Id;
                        docMod.DocVerMod.Version = docVerObj.Version;
                        docMod.DocVerMod.DocumentId = docVerObj.DocumentId;
                        docMod.DocVerMod.State = (DocVerState)docVerObj.State;
                        docMod.DocVerMod.DisplayName = docVerObj.DisplayName;
                        docMod.DocVerMod.DisplayVersion = docVerObj.DisplayVersion;
                        docMod.DocVerMod.ModifyTime = docVerObj.ModifyTime;
                        docMod.DocVerMod.ModifyAccountId = docVerObj.ModifyAccountId;
                        docMod.DocVerMod.ApproveAccountId = docVerObj.ApproveAccountId;
                        docMod.DocVerMod.FilterKeyword = docVerObj.FilterKeyword;
                        docMod.DocVerMod.Expire = docVerObj.Expire;
                        docMod.DocVerMod.FileUrl = docVerObj.FileUrl;
                        docMod.DocVerMod.FileUploadTime = docVerObj.FileUploadTime;
                        docMod.DocVerMod.CoverThumbnailUrl = docVerObj.CoverThumbnailUrl;
                        docMod.DocVerMod.FileName = docVerObj.FileName;
                        if (docVerObj.FileSize != null)
                            docMod.DocVerMod.FileSize = Convert.ToInt64(docVerObj.FileSize);
                        docMod.DocVerMod.IsPublic = docVerObj.IsPublic;                       

                        docMod.DocVerMod.FileUrl = docVerObj.FileUrl;
                        return docMod;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return null;
        }

        public bool Create(DocumentModel model, Guid? foldId, out string ErrMsgs)
        {

            ErrMsgs = string.Empty;

            JWL_Document dbEntity = new JWL_Document();
            dbEntity.BookcaseId = model.BookcaseId;
            dbEntity.Id = model.Id;            
            dbEntity.CreateTime = model.CreateTime;
            dbEntity.Domain = model.Domain;
            dbEntity.CreateAccountId = model.CreateAccountId;
            dbEntity.DocumentTypeId = model.DocumentTypeId;
            dbEntity.CategoryItemId1 = model.CategoryItemId1;
            dbEntity.CategoryItemId2 = model.CategoryItemId2;
            dbEntity.CategoryItemId3 = model.CategoryItemId3;
            dbEntity.NoInherit = model.NoInherit;

            basedb.JWL_Document.Add(dbEntity);

            JWL_DocumentVersion VerdbEntity = new JWL_DocumentVersion();
            VerdbEntity.Id = model.DocVerMod.Id; 
            VerdbEntity.Version = model.DocVerMod.Version;
            VerdbEntity.DocumentId = model.Id;
            VerdbEntity.State = Convert.ToByte(model.DocVerMod.State);
            VerdbEntity.DisplayName = model.DocVerMod.DisplayName;
            VerdbEntity.DisplayVersion = model.DocVerMod.DisplayVersion;
            VerdbEntity.ModifyTime = model.DocVerMod.ModifyTime;
            VerdbEntity.ModifyAccountId = model.DocVerMod.ModifyAccountId;

            if (model.DocVerMod.FilterKeyword != null)
                VerdbEntity.FilterKeyword = model.DocVerMod.FilterKeyword;

            if (model.DocVerMod.Expire != null)
                VerdbEntity.Expire = model.DocVerMod.Expire;

            VerdbEntity.FileUploadTime = model.DocVerMod.FileUploadTime;

            if (model.DocVerMod.CoverThumbnailUrl != null)
                VerdbEntity.CoverThumbnailUrl = model.DocVerMod.CoverThumbnailUrl;

            VerdbEntity.FileName = model.DocVerMod.FileName;
            VerdbEntity.FileSize = model.DocVerMod.FileSize;
            VerdbEntity.FileUrl = model.DocVerMod.FileUrl;
            VerdbEntity.IsPublic = model.DocVerMod.IsPublic;

            basedb.JWL_DocumentVersion.Add(VerdbEntity);

            foreach(var pageObj in model.pagImgLtMod.Data)
            {
                JWL_PageImage PageImagedbEntity = new JWL_PageImage();
                PageImagedbEntity.Id = pageObj.Id;
                PageImagedbEntity.Seq = pageObj.Seq;
                PageImagedbEntity.ImageUrl = pageObj.ImageUrl;
                PageImagedbEntity.DocumentVersionId = pageObj.DocumentVersionId;
                basedb.JWL_PageImage.Add(PageImagedbEntity);
            }

            if (foldId != null)
            {
                JWL_FolderDocument FddbEntity = new JWL_FolderDocument();
                FddbEntity.FolderId = new Guid(foldId.ToString());
                FddbEntity.DocumentId = model.Id;
                basedb.JWL_FolderDocument.Add(FddbEntity);
            }

            try
            {
                basedb.SaveChanges();
            }
            catch (Exception ex)
            {
                ErrMsgs = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
            }

            return ErrMsgs.Length == 0;
        }

        public bool Update(DocumentModel model, out string ErrMsgs)
        {

            ErrMsgs = string.Empty;

            try
            {
                var dbDoc = basedb.JWL_Document.Where(p => p.Id == model.Id).FirstOrDefault();
                if (dbDoc != null)
                {
                    dbDoc.CategoryItemId1 = model.CategoryItemId1;
                    dbDoc.CategoryItemId2 = model.CategoryItemId2;
                    dbDoc.CategoryItemId3 = model.CategoryItemId3;
                    dbDoc.NoInherit = model.NoInherit;

                    var dbDocVer = basedb.JWL_DocumentVersion.Where(p => p.Id == model.DocVerMod.Id).FirstOrDefault();

                    dbDocVer.State = Convert.ToByte(model.DocVerMod.State);
                    dbDocVer.DisplayName = model.DocVerMod.DisplayName;
                    dbDocVer.DisplayVersion = model.DocVerMod.DisplayVersion;
                    dbDocVer.ModifyTime = model.DocVerMod.ModifyTime;
                    dbDocVer.ModifyAccountId = model.DocVerMod.ModifyAccountId;

                    if (model.DocVerMod.FilterKeyword != null)
                        dbDocVer.FilterKeyword = model.DocVerMod.FilterKeyword;

                    if (model.DocVerMod.Expire != null)
                        dbDocVer.Expire = model.DocVerMod.Expire;                    

                    if (model.DocVerMod.CoverThumbnailUrl != null)
                        dbDocVer.CoverThumbnailUrl = model.DocVerMod.CoverThumbnailUrl;
                   
                    dbDocVer.IsPublic = model.DocVerMod.IsPublic;

                    if(model.DocVerMod.FileName != null)
                    {
                        dbDocVer.FileUploadTime = model.DocVerMod.FileUploadTime;
                        dbDocVer.FileName = model.DocVerMod.FileName;
                        dbDocVer.FileSize = model.DocVerMod.FileSize;
                        dbDocVer.FileUrl = model.DocVerMod.FileUrl;

                        var dbPages = basedb.JWL_PageImage.Where(p => p.DocumentVersionId == model.DocVerMod.Id).ToList();

                        foreach (var pageObj in dbPages)
                        {
                            basedb.JWL_PageImage.Remove(pageObj);
                        }

                        foreach (var pageObj in model.pagImgLtMod.Data)
                        {
                            JWL_PageImage PageImagedbEntity = new JWL_PageImage();
                            PageImagedbEntity.Id = pageObj.Id;
                            PageImagedbEntity.Seq = pageObj.Seq;
                            PageImagedbEntity.ImageUrl = pageObj.ImageUrl;
                            PageImagedbEntity.DocumentVersionId = pageObj.DocumentVersionId;
                            basedb.JWL_PageImage.Add(PageImagedbEntity);
                        }

                    }
                    
                    basedb.SaveChanges();
                }
                else
                {
                    ErrMsgs = "查無此物件";
                }

            }
            catch (Exception ex)
            {
                ErrMsgs = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
            }

            return ErrMsgs.Length == 0;
        }

        public bool ChangeStatus(Guid Id, DocVerState Status, string UserId, out string ErrMsgs)
        {

            ErrMsgs = string.Empty;

            var docVerObj = (from p in basedb.JWL_DocumentVersion
                             where p.DocumentId == Id
                             select p).OrderByDescending(p=>p.ModifyTime).FirstOrDefault();


            JWL_DocumentVersion VerdbEntity = new JWL_DocumentVersion();
            VerdbEntity.Id = Guid.NewGuid();            

            switch(Status)
            {
                case DocVerState.CheckIn:
                    VerdbEntity.Version = docVerObj.Version;
                    VerdbEntity.DisplayVersion = (VerdbEntity.Version < 1) ? "無" : docVerObj.DisplayVersion;
                    if (docVerObj.FileUrl == null)
                        throw new Exception("請先上傳文件");
                    break;
                case DocVerState.CheckOut:
                    VerdbEntity.Version = docVerObj.Version + new decimal(0.1);
                    VerdbEntity.DisplayVersion = (VerdbEntity.Version < 1) ? "無" : docVerObj.DisplayVersion;
                    break;
                case DocVerState.Release:
                    VerdbEntity.Version = decimal.Floor(docVerObj.Version) + 1;
                    VerdbEntity.DisplayVersion = String.Format("{0:N1}", VerdbEntity.Version);
                    VerdbEntity.ApproveAccountId = UserId;
                    break;
            }    
        
            VerdbEntity.DocumentId = docVerObj.DocumentId;
            VerdbEntity.State = (byte)Status;
            VerdbEntity.DisplayName = docVerObj.DisplayName;            
            VerdbEntity.ModifyTime = DateTime.Now;
            VerdbEntity.ModifyAccountId = docVerObj.ModifyAccountId;

            if (docVerObj.FilterKeyword != null)
                VerdbEntity.FilterKeyword = docVerObj.FilterKeyword;

            if (docVerObj.Expire != null)
                VerdbEntity.Expire = docVerObj.Expire;            

            if (docVerObj.CoverThumbnailUrl != null)
                VerdbEntity.CoverThumbnailUrl = docVerObj.CoverThumbnailUrl;

            if (Status != DocVerState.CheckOut || docVerObj.State != 2)
            {
                VerdbEntity.FileUploadTime = docVerObj.FileUploadTime;
                VerdbEntity.FileName = docVerObj.FileName;
                VerdbEntity.FileSize = docVerObj.FileSize;
                VerdbEntity.FileUrl = docVerObj.FileUrl;
            }

            VerdbEntity.IsPublic = docVerObj.IsPublic;

            basedb.JWL_DocumentVersion.Add(VerdbEntity);

            if (VerdbEntity.FileName != null)
            {
                var o_delete = from p in basedb.JWL_PageImage
                               where p.DocumentVersionId == docVerObj.Id
                               select p;

                foreach (var row in o_delete)
                {
                    row.DocumentVersionId = VerdbEntity.Id;
                }
            }

            try
            {
                basedb.SaveChanges();
            }
            catch (Exception ex)
            {
                ErrMsgs = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
            }

            return ErrMsgs.Length == 0;
        }

        public bool ReverToVersion(Guid TagVerId, string UserId, out string ErrMsgs)
        {

            ErrMsgs = string.Empty;            

            var docVerObj = (from p in basedb.JWL_DocumentVersion
                             where p.Id == TagVerId
                             select p).OrderByDescending(p => p.ModifyTime).FirstOrDefault();

            var oldDocVerObj = (from p in basedb.JWL_DocumentVersion
                                where p.DocumentId == docVerObj.DocumentId
                             select p).OrderByDescending(p => p.ModifyTime).FirstOrDefault();

            JWL_DocumentVersion VerdbEntity = new JWL_DocumentVersion();
            VerdbEntity.Id = Guid.NewGuid();

            VerdbEntity.Version = oldDocVerObj.Version + new decimal(0.1);
            VerdbEntity.DisplayVersion = (oldDocVerObj.Version < 1) ? "無" : oldDocVerObj.DisplayVersion;            

            VerdbEntity.DocumentId = docVerObj.DocumentId;
            VerdbEntity.State = (byte)DocVerState.CheckIn;
            VerdbEntity.DisplayName = docVerObj.DisplayName;
            VerdbEntity.ModifyTime = DateTime.Now;
            VerdbEntity.ModifyAccountId = docVerObj.ModifyAccountId;

            if (docVerObj.FilterKeyword != null)
                VerdbEntity.FilterKeyword = docVerObj.FilterKeyword;

            if (docVerObj.Expire != null)
                VerdbEntity.Expire = docVerObj.Expire;

            if (docVerObj.CoverThumbnailUrl != null)
                VerdbEntity.CoverThumbnailUrl = docVerObj.CoverThumbnailUrl;

            VerdbEntity.FileUploadTime = docVerObj.FileUploadTime;
            VerdbEntity.FileName = docVerObj.FileName;
            VerdbEntity.FileSize = docVerObj.FileSize;
            VerdbEntity.FileUrl = docVerObj.FileUrl;

            VerdbEntity.IsPublic = docVerObj.IsPublic;

            basedb.JWL_DocumentVersion.Add(VerdbEntity);

            var o_delete = from p in basedb.JWL_PageImage
                           where p.DocumentVersionId == docVerObj.Id
                           select p;

            foreach (var row in o_delete)
            {
                JWL_PageImage dbImage = new JWL_PageImage();
                dbImage.Id = Guid.NewGuid();
                dbImage.Seq = row.Seq;
                dbImage.ImageUrl = row.ImageUrl;
                dbImage.DocumentVersionId = VerdbEntity.Id;

                basedb.JWL_PageImage.Add(dbImage);
            }

            try
            {
                basedb.SaveChanges();
            }
            catch (Exception ex)
            {
                ErrMsgs = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
            }

            return ErrMsgs.Length == 0;
        }

        public bool Delete(Guid id, string WLBlob, string Domain, out List<string> BlobFileList, out string ErrorMsg)
        {
            ErrorMsg = string.Empty;
            BlobFileList = new List<string>();

            var delDoc = from p in basedb.JWL_Document
                         where p.Id == id
                         select p;



            foreach (var docObj in delDoc)
            {
                var delDocVers = from p in basedb.JWL_DocumentVersion
                                 where p.DocumentId == docObj.Id
                                 select p;

                foreach (var docVerObj in delDocVers)
                {
                    var delImages = from p in basedb.JWL_PageImage
                                    where p.DocumentVersionId == docVerObj.Id
                                    select p;

                    foreach (var imageObj in delImages)
                    {
                        if (docVerObj.FileUrl != null)
                            BlobFileList.Add(imageObj.ImageUrl.Replace(WLBlob, "").Replace(Domain.ToLower(), "").Replace("/", ""));
                       
                        basedb.JWL_PageImage.Remove(imageObj);
                    }

                    if (docVerObj.FileUrl != null)
                        BlobFileList.Add(docVerObj.FileUrl.Replace(WLBlob, "").Replace(Domain.ToLower(), "").Replace("/", ""));

                    if (docVerObj.CoverThumbnailUrl != null)
                        BlobFileList.Add(docVerObj.CoverThumbnailUrl.Replace(WLBlob, "").Replace(Domain.ToLower(), "").Replace("/", ""));

                    basedb.JWL_DocumentVersion.Remove(docVerObj);
                }

                basedb.JWL_Document.Remove(docObj);
            }

            try
            {
                basedb.SaveChanges();
            }
            catch (Exception ex)
            {
                ErrorMsg = ex.Message;
            }

            return ErrorMsg.Length == 0;
        }
    }
}