﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using JamZoo.Project.WebSite.Models;
using JamZoo.Project.WebSite.DbContext;

namespace JamZoo.Project.WebSite.Service
{
    public class PageImageService : BaseService
    {
        public PageImageModel NewInstance()
        {
            PageImageModel newOne = new PageImageModel();            
            return newOne;
        }

        public PageImageListModel GetList(Guid docVerId, PageImageListModel Page)
        {
            var o_query = from p in basedb.JWL_PageImage
                          where p.DocumentVersionId == docVerId
                          select new { p };


            o_query = o_query.OrderBy(p => p.p.Id);

            try
            {
                Page.TotalRecords = o_query.Select(p => p.p.Id).Count();
                Page.Data = o_query.Skip(Page.Skip).Take(Page.Take).Select(o_entity =>
                    new PageImageModel()
                    {
                        Id = o_entity.p.Id,
                        Seq = o_entity.p.Seq,
                        ImageUrl = o_entity.p.ImageUrl,
                        DocumentVersionId = o_entity.p.DocumentVersionId,                        
                    }
                    ).ToList();                

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Page;
        }

        public PageImageListModel GetLastPublicDocImageList(Guid DocId, PageImageListModel Page)
        {
            var DocVer_query = from p in basedb.JWL_DocumentVersion
                               where p.DocumentId == DocId && p.State == 2
                               select p;

            DocVer_query = DocVer_query.OrderByDescending(p => p.ModifyTime);

            var o_query = from p in basedb.JWL_PageImage
                          where p.DocumentVersionId == DocVer_query.FirstOrDefault().Id
                          select new { p };

            o_query = o_query.OrderBy(p => p.p.Seq);

            try
            {
                Page.TotalRecords = o_query.Select(p => p.p.Id).Count();
                Page.Data = o_query.Select(o_entity =>
                    new PageImageModel()
                    {
                        Id = o_entity.p.Id,
                        Seq = o_entity.p.Seq,
                        ImageUrl = o_entity.p.ImageUrl,
                        DocumentVersionId = o_entity.p.DocumentVersionId,
                    }
                    ).ToList();

                //Page.Data = Page.Data.Skip(Page.Skip).Take(Page.Take).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Page;
        }  


        public bool Create(PageImageModel model, out string ErrMsgs)
        {

            ErrMsgs = string.Empty;

            JWL_PageImage dbEntity = new JWL_PageImage();
            dbEntity.Id = model.Id;
            dbEntity.Seq = model.Seq;
            dbEntity.ImageUrl = model.ImageUrl;
            dbEntity.DocumentVersionId = model.DocumentVersionId;           
    
            basedb.JWL_PageImage.Add(dbEntity);

            try
            {
                basedb.SaveChanges();
            }
            catch (Exception ex)
            {
                ErrMsgs = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
            }

            return ErrMsgs.Length == 0;
        }

        public PageImageModel Get(Guid id)
        {
            var o_entity = (from p in basedb.JWL_PageImage
                            where p.Id == id
                            select p).FirstOrDefault();

            if (o_entity != null)
            {
                var model = new PageImageModel()
                {
                    Id = o_entity.Id,
                    Seq = o_entity.Seq,
                    ImageUrl = o_entity.ImageUrl,
                    DocumentVersionId = o_entity.DocumentVersionId,                      
                };

                return model;
            }
            return null;
        }        

        public bool Delete(Guid id, out string ErrorMsg)
        {
            ErrorMsg = string.Empty;

            var o_delete = from p in basedb.JWL_PageImage
                           where p.Id == id
                           select p;



            foreach (var row in o_delete)
            {
                basedb.JWL_PageImage.Remove(row);
            }

            try
            {
                basedb.SaveChanges();
            }
            catch (Exception ex)
            {
                ErrorMsg = ex.Message;
            }

            return ErrorMsg.Length == 0;
        }

        public bool DeleteAllByDocVerId(Guid DocVerId, out string ErrorMsg)
        {
            ErrorMsg = string.Empty;

            var o_delete = from p in basedb.JWL_PageImage
                           where p.DocumentVersionId == DocVerId
                           select p;



            foreach (var row in o_delete)
            {
                basedb.JWL_PageImage.Remove(row);
            }

            try
            {
                basedb.SaveChanges();
            }
            catch (Exception ex)
            {
                ErrorMsg = ex.Message;
            }

            return ErrorMsg.Length == 0;
        }
    }
}