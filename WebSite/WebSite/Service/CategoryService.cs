﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using JamZoo.Project.WebSite.Models;
using JamZoo.Project.WebSite.DbContext;

namespace JamZoo.Project.WebSite.Service
{
    public class CategoryService : BaseService
    {
        public CategoryModel NewInstance()
        {
            CategoryModel newOne = new CategoryModel();            
            return newOne;
        }

        public CategoryListModel GetList(string domain, CategoryListModel Page)
        {
            var o_query = from p in basedb.JWL_Category
                          where p.Domain == domain
                          select p;

            if (!string.IsNullOrEmpty(Page.Search))
            {
                o_query = o_query.Where(p => p.Name.Contains(Page.Search));
            }


            o_query = o_query.OrderBy(p => p.Name);

            try
            {
                Page.TotalRecords = o_query.Select(p => p.Id).Count();
                Page.Data = o_query.Skip(Page.Skip).Take(Page.Take).Select(o_entity =>
                    new CategoryModel()
                    {
                        Id = o_entity.Id,
                        Name = o_entity.Name,
                        Domain = o_entity.Domain,
                        UiType = o_entity.UiType,
                        CreateAccountId = o_entity.CreateAccountId                        
                    }
                    ).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Page;
        }

        public List<SelectListItem> GetItemList(string domain, string selectedItemName)
        {
            List<SelectListItem> ItemList = new List<SelectListItem>();

            var o_query = from p in basedb.JWL_Category
                          where p.Domain == domain
                          select p;           


            o_query = o_query.OrderBy(p => p.Name);

            ItemList.Add(new SelectListItem()
            {
                Text = "請選擇",
                Value = "",
                Selected = !string.IsNullOrEmpty(selectedItemName)
            });

            try
            {

                foreach (var p in o_query)
                {
                    ItemList.Add(new SelectListItem()
                    {
                        Text = p.Name,
                        Value = p.Id.ToString(),
                        Selected = p.Id.Equals(selectedItemName)
                    });
                }             
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ItemList;
        }

        public bool Create(CategoryModel model, out string ErrMsgs)
        {

            ErrMsgs = string.Empty;

            
            JWL_Category dbEntity = new JWL_Category();
            dbEntity.Id = model.Id;
            dbEntity.Name = model.Name;
            dbEntity.Domain = model.Domain;
            dbEntity.UiType = model.UiType;
            dbEntity.CreateAccountId = model.CreateAccountId;
            basedb.JWL_Category.Add(dbEntity);

            try
            {
                basedb.SaveChanges();
            }
            catch (Exception ex)
            {
                ErrMsgs = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
            }

            return ErrMsgs.Length == 0;
        }

        public CategoryModel Get(Guid id)
        {
            var o_entity = (from p in basedb.JWL_Category
                            where p.Id == id
                            select p).FirstOrDefault();

            if (o_entity != null)
            {
                var model = new CategoryModel()
                {
                    Id = o_entity.Id,
                    Name = o_entity.Name,
                    UiType = o_entity.UiType,
                    Domain = o_entity.Domain,
                    CreateAccountId = o_entity.CreateAccountId                  
                };

                return model;
            }
            return null;
        }

        public bool Update(CategoryModel model, out string ErrMsgs)
        {

            ErrMsgs = string.Empty;

            try
            {
                var dbDuty = basedb.JWL_Category.Where(p => p.Id == model.Id).FirstOrDefault();
                if (dbDuty != null)
                {                   
                    dbDuty.Name = model.Name;
                    dbDuty.UiType = model.UiType;                                        
                    basedb.SaveChanges();
                }
                else
                {
                    ErrMsgs = "查無此物件";
                }

            }
            catch (Exception ex)
            {
                ErrMsgs = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
            }

            return ErrMsgs.Length == 0;
        }

        public bool Delete(Guid id, out string ErrorMsg)
        {
            ErrorMsg = string.Empty;

            var o_delete = from p in basedb.JWL_Category
                           where p.Id == id
                           select p;



            foreach (var row in o_delete)
            {
                basedb.JWL_Category.Remove(row);
            }

            try
            {
                basedb.SaveChanges();
            }
            catch (Exception ex)
            {
                ErrorMsg = ex.Message;
            }

            return ErrorMsg.Length == 0;
        }
    }
}