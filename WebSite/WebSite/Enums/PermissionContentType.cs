﻿namespace JamZoo.Project.WebSite.Enums
{
    public enum PermissionContentType : byte
    {
        Bookcase, Folder, Document
    }
}
