﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JamZoo.Project.WebSite.Enums
{
    /// <summary>
    /// 管理後台的角色
    /// </summary>
    public enum DocVerState
    {
        CheckOut,
        CheckIn,
        Release
    }
}