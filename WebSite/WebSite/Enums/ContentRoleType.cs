﻿using System;

namespace JamZoo.Project.WebSite.Enums
{
    [Flags]
    public enum ContentRoleType
    {
        Reader, Writer, Approve
    }
}
