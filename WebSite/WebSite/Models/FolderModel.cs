﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using JamZoo.Project.WebSite.Enums;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace JamZoo.Project.WebSite.Models
{
    /// <summary>
    /// 列表頁
    /// </summary>
    public class FolderListModel : PagerModel
    {
        public string Search { get; set; }

        public List<FolderModel> Data { get; set; }

        public bool BookCaseIsNoInherit { get; set; }

        public FolderListModel()
        {
            Data = new List<FolderModel>();
        }
    }
    /// <summary>
    /// 帳號
    /// </summary>
    public class FolderModel : EditModePage
    {
        [Required(ErrorMessage = "必填")]
        [Display(Name = "ID")]
        public Guid Id { get; set; }

        [Required(ErrorMessage = "必填")]
        [Display(Name = "書櫃ID")]
        public Guid BookcaseId { get; set; }

        [Display(Name = "所在書櫃")]
        public string BookcaseName { get; set; }

        public List<SelectListItem> BookcaseSelectItem { get; set; }   

        [StringLength(50)]
        [Required(ErrorMessage = "必填")]
        [Display(Name = "顯示名稱")]
        public string Name { get; set; }

        [StringLength(50)]
        [Required(ErrorMessage = "必填")]
        [Display(Name = "資料夾說明")]
        public string TypeName { get; set; }   

        [StringLength(200)]        
        [Display(Name = "縮圖URL")]
        public string ThumbnailUrl { get; set; }

        [Required(ErrorMessage = "必填")]
        [Display(Name = "建立時間")]
        public DateTime CreateTime { get; set; }

        [Display(Name = "客戶網域")]
        public string Domain { get; set; }

        [Display(Name = "建立帳號")]
        public string CreateAccountId { get; set; }

        [Required(ErrorMessage = "必填")]
        [Display(Name = "取消繼承")]
        public bool NoInherit { get; set; }

        public string DocTypeIdList { get; set; }

        [UIHint("MultiSelect")]
        [Display(Name = "可選擇")]
        public List<DocumentTypeModel> DocTypeList { get; set; }

        [UIHint("MultiSelect")]
        [Display(Name = "已選擇")]
        public List<FolderDocumentTypeModel> FolderDocType { get; set; }

        public FolderModel()
        {
            Id = Guid.NewGuid();
            CreateTime = DateTime.Now;
            DocTypeList = new List<DocumentTypeModel>();
            FolderDocType = new List<FolderDocumentTypeModel>();
        }

    }
    
}