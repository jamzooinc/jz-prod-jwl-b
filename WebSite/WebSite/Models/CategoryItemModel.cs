﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using JamZoo.Project.WebSite.Enums;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace JamZoo.Project.WebSite.Models
{
    /// <summary>
    /// 列表頁
    /// </summary>
    public class CategoryItemListModel : PagerModel
    {
        public string Search { get; set; }

        public Guid CategoryId { get; set; }

        public List<CategoryItemModel> Data { get; set; }

        public CategoryItemListModel()
        {
            Data = new List<CategoryItemModel>();
        }
    }
    /// <summary>
    /// 帳號
    /// </summary>
    public class CategoryItemModel : EditModePage
    {
        [Required(ErrorMessage = "必填")]
        [Display(Name = "ID")]
        public Guid Id { get; set; }

        [Required(ErrorMessage = "必填")]
        [Display(Name = "自訂分類ID")]
        public Guid CategoryId { get; set; }

        [Display(Name = "客戶網域")]
        public string Domain { get; set; }

        [StringLength(50)]
        [Required(ErrorMessage = "必填")]
        [Display(Name = "顯示名稱")]
        public string Name { get; set; }
        
        [Display(Name = "建立帳號")]
        public string CreateAccountId { get; set; }

        public CategoryItemModel()
        {
            Id = Guid.NewGuid();
        }

    }
}