﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JamZoo.Project.WebSite.Models
{
    public class PermissionListModel : PagerModel
    {
        public Nullable<System.Guid> BookcaseId { get; set; }
        public string Search { get; set; }

        public List<PermissionModel> Data { get; set; }

        public PermissionListModel()
        {
            Data = new List<PermissionModel>();
        }
    }
    public class PermissionModel : EditModePage
    {
        public Guid Id { get; set; }
        //public string Domain { get; set; }
        public string RoleId { get; set; }
        [DisplayName("角色")]
        public string RoleName { get; set; }
        public byte RoleType { get; set; }
        public string RoleTypeName { get; set; }
        [DisplayName("允許的帳號或組織")]
        public string UnitId { get; set; }
        [DisplayName("允許的帳號或組織")]
        public string UnitName { get; set; }
        public Nullable<System.Guid> BookcaseId { get; set; }
        public Nullable<System.Guid> FolderId { get; set; }
        public Nullable<System.Guid> DocumentId { get; set; }
    }
}
