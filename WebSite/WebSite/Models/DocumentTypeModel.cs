﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using JamZoo.Project.WebSite.Enums;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace JamZoo.Project.WebSite.Models
{
    /// <summary>
    /// 列表頁
    /// </summary>
    public class DocumentTypeListModel : PagerModel
    {
        public string Search { get; set; }

        public List<DocumentTypeModel> Data { get; set; }

        public DocumentTypeListModel()
        {
            Data = new List<DocumentTypeModel>();
        }
    }
    /// <summary>
    /// 帳號
    /// </summary>
    public class DocumentTypeModel : EditModePage
    {
        [Required(ErrorMessage = "必填")]
        [Display(Name = "ID")]
        public Guid Id { get; set; }
        
        [Display(Name = "客戶網域")]
        public string Domain { get; set; }

        [StringLength(50)]
        [Required(ErrorMessage = "必填")]
        [Display(Name = "顯示名稱")]
        public string Name { get; set; }

        [StringLength(10)]
        [Required(ErrorMessage = "必填")]
        [Display(Name = "副檔名")]
        public string SubFileName { get; set; }

        [StringLength(100)]
        [Required(ErrorMessage = "必填")]
        [Display(Name = "搜尋關鍵字")]
        public string FilterKeyword { get; set; }

        [Required(ErrorMessage = "必填")]
        [Display(Name = "轉換頁面內容為截圖")]
        public bool ConvertPageImage { get; set; }

        public DocumentTypeModel()
        {
            Id = Guid.NewGuid();
        }

    }
}