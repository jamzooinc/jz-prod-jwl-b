﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using JamZoo.Project.WebSite.Enums;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace JamZoo.Project.WebSite.Models
{
    /// <summary>
    /// 列表頁
    /// </summary>
    public class AccountListModel : PagerModel
    {
        public string Search { get; set; }

        public List<AccountModel> Data { get; set; }

        public AccountListModel()
        {
            Data = new List<AccountModel>();
        }
    }
    /// <summary>
    /// 帳號
    /// </summary>
    public class AccountModel : EditModePage
    {
        [Display(Name = "帳號")]
        public string Id { get; set; }

        [Required(ErrorMessage = "必填")]
        [Display(Name = "顯示名稱")]
        public string DisplayName { get; set; }

        [Required(ErrorMessage = "必填")]
        [Display(Name = "姓名")]
        public string OfficalName { get; set; }

        [Required(ErrorMessage="必填")]
        [Display(Name = "Email")]
        public string Email { get; set; }
        
        [Display(Name = "密碼")]
        public string Password { get; set; }
        
        [Display(Name = "職稱")]
        public string Title { get; set; }

        [DataType(DataType.Password)]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "與密碼不相符")]
        public string RePassword { get; set; }      

        [Display(Name = "建立時間")]
        public DateTime CreateDate { get; set; }

        [Display(Name = "最後登入時間")]
        public DateTime? LastLoginTime { get; set; }

        public bool Status { get; set; }
        /// <summary>
        /// 取得 Role
        /// </summary>
        /// <returns></returns>
        [Display(Name = "角色")]
        public string Role { get; set; }
       
        /// <summary>
        /// 取得 Role
        /// </summary>
        /// <returns></returns>
        [Display(Name = "角色")]
        public List<string> RoleList { get; set; }

        public List<SelectListItem> RoleSelectList { get; set; }

        /// <summary>
        /// 網站角色判斷
        /// </summary>
        /// <param name="Roles"></param>
        /// <returns></returns>
        public bool IsInRoles(List<string> _Roles)
        {
            if (_Roles == null) return false;
            if (_Roles.Count() == 0) return false;
            foreach (var Role in _Roles)
            {
                if (RoleList != null)
                {
                    if (RoleList.Contains(Role))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public AccountModel()
        {
            Id = string.Empty;
            RoleList = new List<string>();
        }

    }
}