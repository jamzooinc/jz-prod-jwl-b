﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using JamZoo.Project.WebSite.Enums;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace JamZoo.Project.WebSite.Models
{
    /// <summary>
    /// 列表頁
    /// </summary>
    public class BookcaseListModel : PagerModel
    {
        public string Search { get; set; }

        public List<BookcaseModel> Data { get; set; }

        public BookcaseListModel()
        {
            Data = new List<BookcaseModel>();
        }
    }
    /// <summary>
    /// 帳號
    /// </summary>
    public class BookcaseModel : EditModePage
    {                
        public Guid? PreId { get; set; }
                
        public Guid? NextId { get; set; }

        [Required(ErrorMessage = "必填")]
        [Display(Name = "ID")]
        public Guid Id { get; set; }

        [StringLength(50)]
        [Required(ErrorMessage = "必填")]
        [Display(Name = "顯示名稱")]
        public string Name { get; set; }        

        [StringLength(200)]        
        [Display(Name = "縮圖URL")]
        public string ThumbnailUrl { get; set; }

        [Required(ErrorMessage = "必填")]
        [Display(Name = "建立時間")]
        public DateTime CreateTime { get; set; }

        [Display(Name = "客戶網域")]
        public string Domain { get; set; }

        [Display(Name = "建立帳號")]
        public string CreateAccountId { get; set; }

        #region ID        
        [Display(Name = "自訂分類ID1")]
        public Guid? CategoryId1 { get; set; }               
        
        [Display(Name = "自訂分類ID2")]
        public Guid? CategoryId2 { get; set; }        
        
        [Display(Name = "自訂分類ID3")]
        public Guid? CategoryId3 { get; set; }
        #endregion

        #region Name
        [Display(Name = "自訂分類1")]
        public string CategoryName1 { get; set; }
        
        [Display(Name = "自訂分類2")]
        public string CategoryName2 { get; set; } 
        
        [Display(Name = "自訂分類3")]
        public string CategoryName3 { get; set; }
        #endregion

        #region SelectList
        public List<SelectListItem> CategorSelectList1 { get; set; }
        public List<SelectListItem> CategorSelectList2 { get; set; }
        public List<SelectListItem> CategorSelectList3 { get; set; }
        #endregion

        [Display(Name = "單檔上限(M)")]
        public long FileMaxSize { get; set; }

        [Required(ErrorMessage = "必填")]
        [Display(Name = "所屬文件須核准")]
        public bool IsApproveEnabled { get; set; }

        [Required(ErrorMessage = "必填")]
        [Display(Name = "所屬文件須版控")]
        public bool IsVersionControlEnabled { get; set; }

        [Required(ErrorMessage = "必填")]
        [Display(Name = "所屬文件到期是否刪除(App端)")]
        public bool IsDocumentExpireDelete { get; set; }

        [Required(ErrorMessage = "必填")]
        [Display(Name = "取消繼承")]
        public bool NoInherit { get; set; }

        public BookcaseModel()
        {
            Id = Guid.NewGuid();
            CreateTime = DateTime.Now;
        }

    }

}