﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using JamZoo.Project.WebSite.Enums;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace JamZoo.Project.WebSite.Models
{
    /// <summary>
    /// 列表頁
    /// </summary>
    public class PageImageListModel : PagerModel
    {
        public string Search { get; set; }

        public List<PageImageModel> Data { get; set; }

        public PageImageListModel()
        {
            Data = new List<PageImageModel>();
        }
    }
    /// <summary>
    /// 帳號
    /// </summary>
    public class PageImageModel : EditModePage
    {
        [Required(ErrorMessage = "必填")]
        [Display(Name = "ID")]
        public Guid Id { get; set; }
        
        [Display(Name = "頁數")]
        public short Seq { get; set; }

        [StringLength(200)]
        [Display(Name = "縮圖URL")]
        public string ImageUrl { get; set; }

        [Required(ErrorMessage = "必填")]
        [Display(Name = "文件版本ID")]
        public Guid DocumentVersionId { get; set; }


        public PageImageModel()
        {
            Id = Guid.NewGuid();            
        }

    }
    
}