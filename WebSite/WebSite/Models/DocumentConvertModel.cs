﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

using JamZoo.Project.WebSite.Enums;

namespace JamZoo.Project.WebSite.Models
{
    /// <summary>
    /// 列表頁
    /// </summary>
    public class DocumentConvertListModel : PagerModel
    {             

        public List<DocumentConvertModel> Data { get; set; }

        public DocumentConvertListModel()
        {            
            Data = new List<DocumentConvertModel>();
        }
    }
    /// <summary>
    /// 帳號
    /// </summary>
    public class DocumentConvertModel : EditModePage
    {
        [Required(ErrorMessage = "必填")]        
        public Guid Id { get; set; }
 
        [Required(ErrorMessage = "必填")]
        public Guid VersionId { get; set; }    

        [Required(ErrorMessage = "必填")]        
        public int ConvertStatus { get; set; }     
            
        public DateTime? ConvertStartTime { get; set; }
     
        public DateTime? ConvertEndTime { get; set; }           

        [StringLength(200)]
        public string ConvertMessage { get; set; }

        public DateTime CreateTime { get; set; }
        public DateTime ModifyTime { get; set; }           

        public DocumentConvertModel()
        {
            Id = Guid.NewGuid();            
        }
    }
}