﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using JamZoo.Project.WebSite.Enums;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace JamZoo.Project.WebSite.Models
{
    /// <summary>
    /// 列表頁
    /// </summary>
    public class CategoryListModel : PagerModel
    {
        public string Search { get; set; }

        public List<CategoryModel> Data { get; set; }

        public CategoryListModel()
        {
            Data = new List<CategoryModel>();
        }
    }
    /// <summary>
    /// 帳號
    /// </summary>
    public class CategoryModel : EditModePage
    {
        [Required(ErrorMessage = "必填")]
        [Display(Name = "ID")]
        public Guid Id { get; set; }
        
        [Display(Name = "客戶網域")]
        public string Domain { get; set; }

        [StringLength(50)]
        [Required(ErrorMessage = "必填")]
        [Display(Name = "顯示名稱")]
        public string Name { get; set; }

        [Required(ErrorMessage = "必填")]
        [Display(Name = "顯示方式")]
        public byte UiType { get; set; }
                
        [Display(Name = "建立帳號")]
        public string CreateAccountId { get; set; }

        public CategoryModel()
        {
            Id = Guid.NewGuid();
        }

    }
}