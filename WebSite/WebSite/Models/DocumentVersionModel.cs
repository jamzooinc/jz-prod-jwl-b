﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

using JamZoo.Project.WebSite.Enums;

namespace JamZoo.Project.WebSite.Models
{
    /// <summary>
    /// 列表頁
    /// </summary>
    public class DocumentVersionListModel : PagerModel
    {
        public string Search { get; set; }

        /// <summary>
        /// 對應的文件資料
        /// </summary>
        public DocumentModel DocMod { get; set; }

        public List<DocumentVersionModel> Data { get; set; }

        public DocumentVersionListModel()
        {
            DocMod = new DocumentModel();
            Data = new List<DocumentVersionModel>();
        }
    }
    /// <summary>
    /// 帳號
    /// </summary>
    public class DocumentVersionModel : EditModePage
    {
        [Required(ErrorMessage = "必填")]
        [Display(Name = "ID")]
        public Guid Id { get; set; }
 
        [Required(ErrorMessage = "必填")]
        [Display(Name = "版號")]
        public decimal Version { get; set; }  

        [Required(ErrorMessage = "必填")]
        [Display(Name = "文件ID")]
        public Guid DocumentId { get; set; }     
        
        [Display(Name = "文件狀態")]
        public DocVerState State { get; set; }               

        [Required(ErrorMessage = "必填")]
        [StringLength(50)]
        [Display(Name = "顯示名稱")]
        public string DisplayName { get; set; }
        
        [StringLength(50)]
        [Display(Name = "顯示版號")]
        public string DisplayVersion { get; set; }

        [Required(ErrorMessage = "必填")]
        [Display(Name = "修改時間")]
        public DateTime ModifyTime { get; set; }
        
        [Display(Name = "修改帳號")]
        public string ModifyAccountId { get; set; }

        [Display(Name = "審核帳號")]
        public string ApproveAccountId { get; set; }

        [Display(Name = "搜尋關鍵字")]
        public string FilterKeyword { get; set; }

        [Display(Name = "到期日")]
        public DateTime? Expire { get; set; }

        [Display(Name = "檔案上傳時間")]
        public DateTime? FileUploadTime { get; set; }

        [StringLength(200)]
        [Display(Name = "封面縮圖")]
        public string CoverThumbnailUrl { get; set; }

        [StringLength(50)]
        [Display(Name = "檔案名稱")]
        public string FileName { get; set; }
        
        [Display(Name = "檔案大小")]
        public long FileSize { get; set; }

        [StringLength(200)]
        [Display(Name = "檔案URL")]
        public string FileUrl { get; set; }

        [Display(Name = "是否為公開文件")]
        public bool IsPublic { get; set; }

        public DocumentVersionModel()
        {
            Id = Guid.NewGuid();            
        }
    }
}