﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using JamZoo.Project.WebSite.Enums;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace JamZoo.Project.WebSite.Models
{
    public class FolderDocumentListModel : PagerModel
    {        
        public List<FolderDocumentModel> Data { get; set; }

        public FolderDocumentListModel()
        {
            Data = new List<FolderDocumentModel>();
        }
    }

    public class FolderDocumentModel : EditModePage
    {
        public Guid DocumentId { get; set; }
        public Guid FolderId { get; set; }        
        public string FolderName { get; set; }
    }

    public class FolderDocModel
    {
        public Guid bookcaseId { get; set; }
        public Guid ID { get; set; }
        public string FolderIds { get; set; }

        [UIHint("MultiSelect")]
        [Display(Name = "可選擇")]
        public List<FolderModel> FolderList { get; set; }

        [UIHint("MultiSelect")]
        [Display(Name = "已選擇")]
        public List<FolderDocumentModel> FolderDoc { get; set; }

        public FolderDocModel()
        {
            FolderList = new List<FolderModel>();
            FolderDoc = new List<FolderDocumentModel>();
        }
    }
}