﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using JamZoo.Project.WebSite.Enums;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace JamZoo.Project.WebSite.Models
{
    /// <summary>
    /// 列表頁
    /// </summary>
    public class DocumentListModel : PagerModel
    {
        public string Search { get; set; }
       
        public List<DocumentModel> Data { get; set; }

        public bool FolderIsNoInherit { get; set; }

        public DocumentListModel()
        {
            Data = new List<DocumentModel>();
            FolderIsNoInherit = false;
        }
    }
    /// <summary>
    /// 帳號
    /// </summary>
    public class DocumentModel : EditModePage
    {
        [Required(ErrorMessage = "必填")]
        [Display(Name = "ID")]
        public Guid Id { get; set; }

        [Required(ErrorMessage = "必填")]
        [Display(Name = "書櫃ID")]
        public Guid BookcaseId { get; set; }
        
        [Display(Name = "所在書櫃")]
        public string BookcaseName { get; set; }        

        [Required(ErrorMessage = "必填")]
        [Display(Name = "建立時間")]
        [DataType(DataType.DateTime), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime CreateTime { get; set; }

        [Display(Name = "客戶網域")]
        public string Domain { get; set; }

        [Display(Name = "建立帳號")]
        public string CreateAccountId { get; set; }

        [Display(Name = "文件類型Id")]
        public Guid DocumentTypeId { get; set; }

        [Display(Name = "文件類型")]
        public string DocumentTypeName { get; set; }

        public string DocumentKeyWord { get; set; }

        [Required(ErrorMessage = "必填")]
        [Display(Name = "取消繼承")]
        public bool NoInherit { get; set; }

        public List<SelectListItem> DocumentTypeSelectList { get; set; }
        
        public string FolderString { get; set; } //所在書櫃

        #region ID        
        [Display(Name = "自訂分類項目ID1")]
        public Guid? CategoryItemId1 { get; set; }

        [Display(Name = "自訂分類項目ID2")]
        public Guid? CategoryItemId2 { get; set; }

        [Display(Name = "自訂分類項目ID3")]
        public Guid? CategoryItemId3 { get; set; }
        #endregion

        #region Name
        [Display(Name = "自訂分類項目1")]
        public string CategoryItemName1 { get; set; }

        [Display(Name = "自訂分類項目2")]
        public string CategoryItemName2 { get; set; } 
        
        [Display(Name = "自訂分類項目3")]
        public string CategoryItemName3 { get; set; }
        #endregion

        #region SelectList
        public List<SelectListItem> CategorItemSelectList1 { get; set; }
        public List<SelectListItem> CategorItemSelectList2 { get; set; }
        public List<SelectListItem> CategorItemSelectList3 { get; set; }
        #endregion    
        public string ConvertStatus { get; set; }
        public DocumentVersionModel DocVerMod { get; set; }
        public PageImageListModel pagImgLtMod { get; set; }
        public DocumentModel()
        {
            Id = Guid.NewGuid();
            DocVerMod = new DocumentVersionModel();
            pagImgLtMod = new PageImageListModel();
            CreateTime = DateTime.Now;
        }
    }
}