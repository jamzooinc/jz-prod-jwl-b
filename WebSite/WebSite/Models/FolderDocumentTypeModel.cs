﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using JamZoo.Project.WebSite.Enums;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace JamZoo.Project.WebSite.Models
{
    public class FolderDocumentTypeListModel : PagerModel
    {        
        public List<FolderDocumentTypeModel> Data { get; set; }

        public FolderDocumentTypeListModel()
        {
            Data = new List<FolderDocumentTypeModel>();
        }
    }

    public class FolderDocumentTypeModel : EditModePage
    {
        public Guid FolderId { get; set; }
        public Guid DocumentTypeId { get; set; }
        public string DocumentTypeName { get; set; }
    }

    public class FolderDocTypeModel
    {
        public Guid ID { get; set; }
        public string DocTypeIdList { get; set; }

        [UIHint("MultiSelect")]
        [Display(Name = "可選擇")]
        public List<DocumentTypeModel> DocTypeList { get; set; }

        [UIHint("MultiSelect")]
        [Display(Name = "已選擇")]
        public List<FolderDocumentTypeModel> FolderDocType { get; set; }

        public FolderDocTypeModel()
        {
            DocTypeList = new List<DocumentTypeModel>();
            FolderDocType = new List<FolderDocumentTypeModel>();
        }
    }
}