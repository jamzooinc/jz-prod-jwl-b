﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Drawing.Imaging;
using System.ServiceModel.Dispatcher;
using GhostscriptSharp;
using GhostscriptSharp.Settings;
using System.Web;
using JamZoo.Project.WebSite.Library.Azure;

using OFFICECORE = Microsoft.Office.Core;
using POWERPOINT = Microsoft.Office.Interop.PowerPoint;
using WORD = Microsoft.Office.Interop.Word;

using JamZoo.Project.WebSite.Library;
using JamZoo.Project.WebSite.Library.DocConvert;
using JamZoo.Project.WebSite.Models;
using JamZoo.Project.WebSite.Service;

namespace DocCnverterApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            //0 Domain
            //1 ConverId
            string errStr = string.Empty;
            string strdomain = args[0].ToString();
            string WLBlob = "https://walklib.blob.core.windows.net/";

            DocumentService docSer = new DocumentService();
            DocumentConvertService docConvSer = new DocumentConvertService();
            
            BlobHandler bh = new BlobHandler(strdomain.ToLower());
            if (args.Length == 2)
            if (true)
            {
                WriteOut("Start");

                WriteOut(string.Format("Domain:{0}", args[0].ToString()));                
                WriteOut(string.Format("ConvertId:{0}", args[1].ToString()));
                FileInfo[] fis = null;                

                DocumentConvertModel docConvObj = docConvSer.Get(new Guid(args[1].ToString()));
                DocumentVersionModel DocVerObj = docSer.GetDocVer(docConvObj.VersionId);
                
                WriteOut(string.Format("DocVersionId:{0}", docConvObj.VersionId.ToString()));         
                #region 處理文件                

                #region 下載檔案
                WebClient myWebClient = new WebClient();
                #endregion
                
                string FilePath = AppDomain.CurrentDomain.BaseDirectory;
                string FileName = DocVerObj.FileUrl.Replace(WLBlob, "").Replace(strdomain.ToLower() + "/", "");
                string SavePath = string.Format("{0}{1}", FilePath, FileName);
                
                WriteOut(string.Format("StartDownLoad:SavePath:[{0}]", SavePath));
                WriteOut(string.Format("SavePath:{0}", SavePath));
                FileInfo fi = new FileInfo(SavePath);                                
                myWebClient.DownloadFile(DocVerObj.FileUrl, SavePath);                

                string FileTempFolder = string.Format("{0}{1}", FilePath, fi.Name.Replace(fi.Extension, ""));

                if (!Directory.Exists(FileTempFolder))
                    Directory.CreateDirectory(FileTempFolder);

                DocConverter docConv = new DocConverter();
                
                WriteOut("StartConvert");
                switch (fi.Extension.ToUpper())
                {
                    case ".PPT":
                    case ".PPTX":
                        WriteOut("Convert To PowerPoint");
                        docConv.PptToImage(SavePath, FileTempFolder);
                        break;
                    case ".DOC":
                    case ".DOCX":
                        docConv.DocToImage(SavePath, FileTempFolder);
                        break;
                    case ".PDF":
                        docConv.PdfToImage(SavePath, FileTempFolder);
                        break;
                }

                DirectoryInfo di = new DirectoryInfo(FileTempFolder);

                fis = di.GetFiles();
                
                WriteOut("StartUploadFile");
                short Cnt = 1;
                foreach (var subfi in fis)
                {
                    
                    PageImageService imageSer = new PageImageService();
                    string pageFileName = string.Format("{0}_IMG{1}.JPG", fi.Name, Cnt.ToString());
                    PageImageModel imageMod = new PageImageModel { Id = Guid.NewGuid(), Seq = Cnt, ImageUrl = string.Format("{0}{1}/{2}", WLBlob, strdomain, pageFileName), DocumentVersionId = DocVerObj.Id };
                    imageSer.Create(imageMod, out errStr);                   
                    WriteOut(string.Format("UploadImage[{0}]", Cnt.ToString()));
                    bh.Upload(subfi.FullName, pageFileName);
                    Cnt++;
                }

                fi = new FileInfo(SavePath);
                fi.Delete();

                Directory.Delete(FileTempFolder, true);

                
                WriteOut("UpdateDbToFinish");
                docConvObj.ConvertEndTime = DateTime.Now;
                docConvObj.ConvertStatus = 2;
                docConvObj.ModifyTime = DateTime.Now; ;
                docConvSer.Update(docConvObj, out errStr);
                WriteOut("==========Finish==========");                
                #endregion
            }
        }
       
        private static void WriteOut(string Message)
        {
            Console.WriteLine(string.Format("[{0}]{1}", DateTime.Now.ToString("yyMMdd HH:mm:ss"), Message));
        }
    }    
}
